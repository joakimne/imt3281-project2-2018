package no.ntnu.imt3281.ludo.logic.piece;

import no.ntnu.imt3281.ludo.logic.Ludo;
import no.ntnu.imt3281.ludo.logic.dice.DiceEvent;

import java.util.EventObject;

/**
 * Custom event for a piece
 */
public class PieceEvent extends EventObject {
    int playerconst;
    int pieceid;
    int start;
    int end;

    public int getPlayerconst() {
        return playerconst;
    }

    public void setPlayerconst(int playerconst) {
        this.playerconst = playerconst;
    }

    public int getPieceid() {
        return pieceid;
    }

    public void setPieceid(int pieceid) {
        this.pieceid = pieceid;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    /**
     * Checks that the parameter object have the same contents as this one
     * @param obj the PieceEvent to compare the current object with
     * @return
     */
    @Override
    public boolean equals(Object obj) {
    	if(obj instanceof PieceEvent) {
    		PieceEvent eventparam = (PieceEvent) obj;
            return eventparam.getPlayerconst() == this.playerconst && eventparam.getPieceid() == this.pieceid && eventparam.getStart() == this.start && eventparam.getEnd() == this.end;
    	}
    	return false;
    }
    @Override
    public int hashCode() {
    	// TODO Auto-generated method stub
    	return super.hashCode();
    }

    /**
     * The constructor for a pieceEvent
     * @param ludo the ludo class
     * @param playerconst the player (ludo.RED)
     * @param pieceid the id of the piece
     * @param start the start position
     * @param end the end position
     */
    public PieceEvent(Ludo ludo, int playerconst, int pieceid, int start, int end){
        super(ludo);
        setPlayerconst(playerconst);
        setPieceid(pieceid);
        setStart(start);
        setEnd(end);
    }

    public Ludo getLudo() {
        return (Ludo) this.getSource();
    }

}
