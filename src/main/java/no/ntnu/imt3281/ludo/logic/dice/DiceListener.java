package no.ntnu.imt3281.ludo.logic.dice;

import java.util.EventListener;

/**
 * Interface for a diceListener
 * No implementation of functions in this class
 *
 */
public interface DiceListener { //extends EventListener {
    /**
     * @param diceevent custom event
     */
    void diceThrown(DiceEvent diceevent);
}
