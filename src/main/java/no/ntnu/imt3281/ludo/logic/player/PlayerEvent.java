package no.ntnu.imt3281.ludo.logic.player;

import no.ntnu.imt3281.ludo.logic.Ludo;

import java.util.EventObject;

/**
 * Custom event for a player
 */
public class PlayerEvent extends EventObject {
    //Status of a player
    public static final int WAITING = 0;
    public static final int PLAYING = 1;
    public static final int WON     = 2;
    public static final int LEFTGAME= 3;
    public static final int JOINED  = 4;
    int activeplayer;
    int state;

    public int getActiveplayer() {
        return activeplayer;
    }

    public void setActiveplayer(int activeplayer) {
        this.activeplayer = activeplayer;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }
    
    public Ludo getLudo() {
    	return (Ludo) this.getSource();
    }

    @Override
    public boolean equals(Object obj) {
    	if(obj instanceof PlayerEvent) {
    		 PlayerEvent eventparam = (PlayerEvent) obj;
    	        if(eventparam.getActiveplayer() == this.activeplayer && eventparam.getState() == this.state){
    	            return true;
    	        }else {
    	            return false;
    	        }
    	}
       return false;
    }
    @Override
    public int hashCode() {
    	// TODO Auto-generated method stub
    	return super.hashCode();
    }
    /**
     * Constructor for a playerevent
     * @param ludo the Ludo class
     * @param playerconst the player (LUDO.RED)
     * @param playerstatus the status of a player (WAITING, PLAYING, WON)
     */
    public PlayerEvent(Ludo ludo, int playerconst, int playerstatus){
        super(ludo);
        setActiveplayer(playerconst);
        setState(playerstatus);
    }
    
}
