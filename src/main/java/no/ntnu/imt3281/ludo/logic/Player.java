package no.ntnu.imt3281.ludo.logic;

/**
 * A token belongs to a player -  a player is a color.
 * Used to differentiate tokens to players/colors
 * @author joakimellestad
 *
 */
public enum Player {
	RED("#f44141",0),
	BLUE("#4286f4",1),
	YELLOW("#f4f441",2),
    GREEN("#41f476",3);

    private String colorName;
    private int playerNumber;
    
    /**
     * Constructor for Player object
     * @param colorName the color of the player, GREEN, RED, BLUE or YELLOW.
     */
    Player(String colorName,int playerNumber) {
        this.colorName = colorName;
        this.playerNumber = playerNumber;
    }
    
    
    /**
     * converts an int to a player object.
     * @param playerconst the int to be converted.
     * @return the object to be returned.
     */
    public static Player intToPlayer(int playerconst) {
    	Player player = null;
    	switch (playerconst) {
		case 0:
			player = Player.RED;	
			break;
		case 1:
			player = Player.BLUE;
			break;
		case 2:
			player = Player.YELLOW;
			break;
		case 3:
			player = Player.GREEN;
			break;
		default:
			break;
		}
    	return player;
    }
    
    
    public String getColorName() {
        return this.colorName;
    }
    public int getPlayerNumber(){
    	return this.playerNumber;
    }
}
