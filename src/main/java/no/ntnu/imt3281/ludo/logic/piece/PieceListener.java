package no.ntnu.imt3281.ludo.logic.piece;

import java.util.EventListener;

/**
 * Interface implemented in PieceListener
 * for definitoin of functions declared in PieceListener
 */
public interface PieceListener { //extends EventListener {

    /**
     * @param event custom PieceEvent
     */
    void pieceMoved(PieceEvent event);
}
