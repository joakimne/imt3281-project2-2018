package no.ntnu.imt3281.ludo.logic.player;


import java.util.EventListener;

/**
 * Listener for playerevents
 */
public interface PlayerListener {
    /**
     * Whenever a player state changes
     * @param event event describing the event that occurred
     */
    void playerStateChanged(PlayerEvent event);
}
