package no.ntnu.imt3281.ludo.logic;

/**
 * Supports the position of tokens within the yard
 * A token will then also hold the property either top,left,right,bottom
 * @author joakimellestad
 *
 */
public enum TokenPosition {
	TOP,
	LEFT,
	RIGHT,
	BOTTOM;
	private static TokenPosition[] positions = values();
	private TokenPosition() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Returns the next position
	 * 
	 * ordinal gives the current position the enum has. top,left,right,bottom.
	 * +1 gets the next
	 * modulo its length to avoid out of bounds on array
	 * @return
	 */
	public TokenPosition next() {
		return positions[(this.ordinal()+1) % positions.length];
	}
	
	/**
	 * Converts into TokenPosition object.
	 * @param piece the int to be converted.
	 * @return returns TokenPosition object.
	 */
	public static TokenPosition intToTokPos(int piece) {
		TokenPosition tokpos = null;
		switch (piece) {
		case 0:
			tokpos = TokenPosition.TOP;
			break;
		case 1:
			tokpos = TokenPosition.LEFT;
			break;
		case 2:
			tokpos = TokenPosition.RIGHT;
			break;
		case 3:
			tokpos = TokenPosition.BOTTOM;
			break;
		default:
			break;
		}
		return tokpos;
	}
}
