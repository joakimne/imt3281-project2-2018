package no.ntnu.imt3281.ludo.logic;

/**
 * for looking up coordinate to blocks
 */
public class LudoCoordinateToBlockLookup {

	/**
	 * 15x15 block board
	 * 
	 * 0 : yard position 
	 * 1 : first block
	 * 2 : second block
	 * ...
	 * 52 blokker rundt
	 * 53 første blokk på stige
	 * 54
	 * ...
	 * 58 i mål
	 * 
	 * Coordinate (1,6), blokk 1 og 52 gir likt coordinat
	 * 
	 */
	static private Coordinate[] lookupTableGreen = 
		{
				new Yard(
						new Coordinate(0,0),
						new Coordinate(2,1),
						new Coordinate(1,2),
						new Coordinate(3,2),
						new Coordinate(2,3)),
				new Coordinate(1,6),
				new Coordinate(2,6),
				new Coordinate(3,6),
				new Coordinate(4,6),
				new Coordinate(5,6),
				new Coordinate(6,5),
				new Coordinate(6,4),
				new Coordinate(6,3),
				new Coordinate(6,2),
				new Coordinate(6,1),
				new Coordinate(6,0),
				new Coordinate(7,0),
				new Coordinate(8,0),
				new Coordinate(8,1),
				new Coordinate(8,2),
				new Coordinate(8,3),
				new Coordinate(8,4),
				new Coordinate(8,5),
				new Coordinate(9,6),
				new Coordinate(10,6),
				new Coordinate(11,6),
				new Coordinate(12,6),
				new Coordinate(13,6),
				new Coordinate(14,6),
				new Coordinate(14,7),
				new Coordinate(14,8),
				new Coordinate(13,8),
				new Coordinate(12,8),
				new Coordinate(11,8),
				new Coordinate(10,8),
				new Coordinate(9,8),
				new Coordinate(8,9),
				new Coordinate(8,10),
				new Coordinate(8,11),
				new Coordinate(8,12),
				new Coordinate(8,13),
				new Coordinate(8,14),
				new Coordinate(7,14),
				new Coordinate(6,14),
				new Coordinate(6,13),
				new Coordinate(6,12),
				new Coordinate(6,11),
				new Coordinate(6,10),
				new Coordinate(6,9),
				new Coordinate(5,8),
				new Coordinate(4,8),
				new Coordinate(3,8),
				new Coordinate(2,8),
				new Coordinate(1,8),
				new Coordinate(0,8),
				new Coordinate(0,7),
				new Coordinate(0,6),
				new Coordinate(1,6),
				new Coordinate(1,7),
				new Coordinate(2,7),
				new Coordinate(3,7),
				new Coordinate(4,7),
				new Coordinate(5,7),
				new Coordinate(6,7),
		};
		
	static private Coordinate[] lookupTableRed = {
			new Yard(
					new Coordinate(9,0),
					new Coordinate(11,1),
					new Coordinate(10,2),
					new Coordinate(12,2),
					new Coordinate(11,3)),
			new Coordinate(8,1),
			new Coordinate(8,2),
			new Coordinate(8,3),
			new Coordinate(8,4),
			new Coordinate(8,5),
			new Coordinate(9,6),
			new Coordinate(10,6),
			new Coordinate(11,6),
			new Coordinate(12,6),
			new Coordinate(13,6),
			new Coordinate(14,6),
			new Coordinate(14,7),
			new Coordinate(14,8),
			new Coordinate(13,8),
			new Coordinate(12,8),
			new Coordinate(11,8),
			new Coordinate(10,8),
			new Coordinate(9,8),
			new Coordinate(8,9),
			new Coordinate(8,10),
			new Coordinate(8,11),
			new Coordinate(8,12),
			new Coordinate(8,13),
			new Coordinate(8,14),
			new Coordinate(7,14),
			new Coordinate(6,14),
			new Coordinate(6,13),
			new Coordinate(6,12),
			new Coordinate(6,11),
			new Coordinate(6,10),
			new Coordinate(6,9),
			new Coordinate(5,8),
			new Coordinate(4,8),
			new Coordinate(3,8),
			new Coordinate(2,8),
			new Coordinate(1,8),
			new Coordinate(0,8),
			new Coordinate(0,7),
			new Coordinate(0,6),
			new Coordinate(1,6),
			new Coordinate(2,6),
			new Coordinate(3,6),
			new Coordinate(4,6),
			new Coordinate(5,6),
			new Coordinate(6,5),
			new Coordinate(6,4),
			new Coordinate(6,3),
			new Coordinate(6,2),
			new Coordinate(6,1),
			new Coordinate(6,0),
			new Coordinate(7,0),
			new Coordinate(8,0),
			new Coordinate(8,1),
			new Coordinate(7,1),
			new Coordinate(7,2),
			new Coordinate(7,3),
			new Coordinate(7,4),
			new Coordinate(7,5),
			new Coordinate(7,6),
	};
	
	private static Coordinate [] lookupTableBlue = {
			new Yard(new Coordinate(9,9), new Coordinate(11,10),new Coordinate(10,11),new Coordinate(12,11),new Coordinate(11,12)),
			new Coordinate(13,8),
			new Coordinate(12,8),
			new Coordinate(11,8),
			new Coordinate(10,8),
			new Coordinate(9,8),
			new Coordinate(8,9),
			new Coordinate(8,10),
			new Coordinate(8,11),
			new Coordinate(8,12),
			new Coordinate(8,13),
			new Coordinate(8,14),
			new Coordinate(7,14),
			new Coordinate(6,14),
			new Coordinate(6,13),
			new Coordinate(6,12),
			new Coordinate(6,11),
			new Coordinate(6,10),
			new Coordinate(6,9),
			new Coordinate(5,8),
			new Coordinate(4,8),
			new Coordinate(3,8),
			new Coordinate(2,8),
			new Coordinate(1,8),
			new Coordinate(0,8),
			new Coordinate(0,7),
			new Coordinate(0,6),
			new Coordinate(1,6),
			new Coordinate(2,6),
			new Coordinate(3,6),
			new Coordinate(4,6),
			new Coordinate(5,6),
			new Coordinate(6,5),
			new Coordinate(6,4),
			new Coordinate(6,3),
			new Coordinate(6,2),
			new Coordinate(6,1),
			new Coordinate(6,0),
			new Coordinate(7,0),
			new Coordinate(8,0),
			new Coordinate(8,1),
			new Coordinate(8,2),
			new Coordinate(8,3),
			new Coordinate(8,4),
			new Coordinate(8,5),
			new Coordinate(9,6),
			new Coordinate(10,6),
			new Coordinate(11,6),
			new Coordinate(12,6),
			new Coordinate(13,6),
			new Coordinate(14,6),
			new Coordinate(14,7),
			new Coordinate(14,8),
			new Coordinate(13,8),
			new Coordinate(13,7),
			new Coordinate(12,7),
			new Coordinate(11,7),
			new Coordinate(10,7),
			new Coordinate(9,7),
			new Coordinate(8,7),
	};
	
	
	private static Coordinate [] lookupTableYellow = {
			new Yard(
					new Coordinate(0,9),
					new Coordinate(2,10),
					new Coordinate(1,11),
					new Coordinate(3,11),
					new Coordinate(2,12)),
			new Coordinate(6,13),
			new Coordinate(6,12),
			new Coordinate(6,11),
			new Coordinate(6,10),
			new Coordinate(6,9),
			new Coordinate(5,8),
			new Coordinate(4,8),
			new Coordinate(3,8),
			new Coordinate(2,8),
			new Coordinate(1,8),
			new Coordinate(0,8),
			new Coordinate(0,7),
			new Coordinate(0,6),
			new Coordinate(1,6),
			new Coordinate(2,6),
			new Coordinate(3,6),
			new Coordinate(4,6),
			new Coordinate(5,6),
			new Coordinate(6,5),
			new Coordinate(6,4),
			new Coordinate(6,3),
			new Coordinate(6,2),
			new Coordinate(6,1),
			new Coordinate(6,0),
			new Coordinate(7,0),
			new Coordinate(8,0),
			new Coordinate(8,1),
			new Coordinate(8,2),
			new Coordinate(8,3),
			new Coordinate(8,4),
			new Coordinate(8,5),
			new Coordinate(9,6),
			new Coordinate(10,6),
			new Coordinate(11,6),
			new Coordinate(12,6),
			new Coordinate(13,6),
			new Coordinate(14,6),
			new Coordinate(14,7),
			new Coordinate(14,8),
			new Coordinate(13,8),
			new Coordinate(12,8),
			new Coordinate(11,8),
			new Coordinate(10,8),
			new Coordinate(9,8),
			new Coordinate(8,9),
			new Coordinate(8,10),
			new Coordinate(8,11),
			new Coordinate(8,12),
			new Coordinate(8,13),
			new Coordinate(8,14),
			new Coordinate(7,14),
			new Coordinate(6,14),
			new Coordinate(6,13),
			new Coordinate(7,13),
			new Coordinate(7,12),
			new Coordinate(7,11),
			new Coordinate(7,10),
			new Coordinate(7,9),
			new Coordinate(7,8),
			
	};
	
	/**
	 * Get's a coordinate for e.g. gui controller 
	 * Gets the current block for a given color
	 * 
	 * TODO: Implent stream later
	 * @param co coordinate
	 * @param player the player
	 * @return block position
	 * @throws Exception if coordinate couldnt be found
	 */
	public static int getBlockPositionCoordinate(Coordinate co, Player player) throws Exception{
		int i = 0;
		switch (player) {
		case GREEN:
			for(Coordinate lookingAtCoordinate : lookupTableGreen) {
				if(lookingAtCoordinate.isWithin(co)) {
					return i;
				}
				i++;
			}
			break;
		case RED:
			for(Coordinate lookingAtCoordinate : lookupTableRed) {
				if(lookingAtCoordinate.isWithin(co)) {
					return i;
				}
				i++;
			}	
			break;
		case BLUE:
			for(Coordinate lookingAtCoordinate : lookupTableBlue) {
				if(lookingAtCoordinate.isWithin(co)) {
					return i;
				}
				i++;
			}	
			break;
		case YELLOW:
			for(Coordinate lookingAtCoordinate : lookupTableYellow) {
				if(lookingAtCoordinate.isWithin(co)) {
					return i;
				}
				i++;
			}	
			break;
		default:
			break;
		}
		throw new Exception("Coordinate is not found within " + player.name());
	}
	
	/**
	 * Takes a block number and passes coordinates in return
	 * Coordinate are centered
	 * @param forPlayer player
	 * @param blockNumber which block
	 * @return the cordinate
	 * @throws if block couldnt be found
	 */
	public static Coordinate getCoordinatesFromBlock(Player forPlayer, int blockNumber) throws Exception {

		switch (forPlayer) {
		case GREEN:
			if(blockNumber >= 0 && blockNumber < lookupTableGreen.length) {
				return lookupTableGreen[blockNumber];
			}
			break;
		case RED:
			if(blockNumber >= 0 && blockNumber < lookupTableRed.length) {
				return lookupTableRed[blockNumber];
			}
			break;
		case YELLOW:
			if(blockNumber >= 0 && blockNumber < lookupTableYellow.length) {
				return lookupTableYellow[blockNumber];
			}
			break;
		case BLUE:
			if(blockNumber >= 0 && blockNumber < lookupTableBlue.length) {
				return lookupTableBlue[blockNumber];
			}
			break;
		default:
			// Noplayer
			break;
		}

		throw new Exception("Coordinate/block is not found");

	}

	/**
	 * constructor
	 */
	private LudoCoordinateToBlockLookup() {}
	
}
