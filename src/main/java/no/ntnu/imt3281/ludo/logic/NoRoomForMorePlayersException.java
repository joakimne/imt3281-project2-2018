package no.ntnu.imt3281.ludo.logic;

/**
 * Exception to handle if there are more than four players added to a game
 */
public class NoRoomForMorePlayersException extends RuntimeException {

    /**
     * Creates an exception
     * @param message an optional message to pass to the exception
     */
    public NoRoomForMorePlayersException(String message)
    {
        super(message);
    }

    /**
     * Default constructor for the exception, if no message is passed
     * It sets a default one
     */
    public NoRoomForMorePlayersException()
    {
        super("No room for more than four players");
    }

    /**
     * Tostring method to return the message from the exception
     * @return the message...
     */
    @Override
    public String toString() {
        String s = getClass().getName();
        String message = getLocalizedMessage();
        return (message != null) ? (s + ": " + message) : s;
    }
}
