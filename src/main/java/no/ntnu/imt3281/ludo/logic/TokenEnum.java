package no.ntnu.imt3281.ludo.logic;

import no.ntnu.imt3281.ludo.gui.AvailableTokens;

/**
 * The possible types of token a user can choose from.
 * Possible merge with {@link AvailableTokens}
 * @author joakimellestad
 *
 */
public enum TokenEnum {
	BASKETBALL,
	CANNONNFILLED,
	DOUBLECHOCOLATECAKE,
	EQUESTRIANCHOCOLATE,
	GAMECONTROLLER,
	MEGAPHONE,
	PAWN,
	PIECEOFCAKE,
	SUBMACHINEGUN;
}
