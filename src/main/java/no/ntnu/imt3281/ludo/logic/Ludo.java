package no.ntnu.imt3281.ludo.logic;

import no.ntnu.imt3281.ludo.logic.dice.DiceEvent;
import no.ntnu.imt3281.ludo.logic.dice.DiceListener;
import no.ntnu.imt3281.ludo.logic.piece.PieceEvent;
import no.ntnu.imt3281.ludo.logic.piece.PieceListener;
import no.ntnu.imt3281.ludo.logic.player.PlayerEvent;
import no.ntnu.imt3281.ludo.logic.player.PlayerListener;
import org.json.JSONArray;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.UUID;

/**
 * Ludo logic class
 */
public class Ludo {

    //This is the playerconst
    static final int RED = 0;
    static final int BLUE = 1;
    static final int YELLOW = 2;
    static final int GREEN = 3;

    //Total number of players in a game
    private int numberofplayers = 0;

    //Number of pieces per player
    private static final int NUMBEROFPIECES = 4;

    /**
     * This array can be generated automatically..., but for now it is hardcoded
     * The format of the table below is:
     * {red, blue, yellow, green}
     * The value is -1 if it is illegal for that color
     * The value on the userboard is the row index of the array(0 --> 91)
     */
    private Integer[][] userToLudoBoard = new Integer[][]{
            {0, -1, -1, -1},
            {0, -1, -1, -1},
            {0, -1, -1, -1},
            {0, -1, -1, -1},
            {-1, 0, -1, -1},
            {-1, 0, -1, -1},
            {-1, 0, -1, -1},
            {-1, 0, -1, -1},
            {-1, -1, 0, -1},
            {-1, -1, 0, -1},
            {-1, -1, 0, -1},
            {-1, -1, 0, -1},
            {-1, -1, -1, 0},
            {-1, -1, -1, 0},
            {-1, -1, -1, 0},
            {-1, -1, -1, 0},
            {53, 40, 27, 14},
            {2, 41, 28, 15},
            {3, 42, 29, 16},
            {4, 43, 30, 17},
            {5, 44, 31, 18},
            {6, 45, 32, 19},
            {7, 46, 33, 20},
            {8, 47, 34, 21},
            {9, 48, 35, 22},
            {10, 49, 36, 23},
            {11, 50, 37, 24},
            {12, 51, 38, 25},
            {13, 52, 39, 26},
            {14, 53, 40, 27},
            {15, 2, 41, 28},
            {16, 3, 42, 29},
            {17, 4, 43, 30},
            {18, 5, 44, 31},
            {19, 6, 45, 32},
            {20, 7, 46, 33},
            {21, 8, 47, 34},
            {22, 9, 48, 35},
            {23, 10, 49, 36},
            {24, 11, 50, 37},
            {25, 12, 51, 38},
            {26, 13, 52, 39},
            {27, 14, 53, 40},
            {28, 15, 2, 41},
            {29, 16, 3, 42},
            {30, 17, 4, 43},
            {31, 18, 5, 44},
            {32, 19, 6, 45},
            {33, 20, 7, 46},
            {34, 21, 8, 47},
            {35, 22, 9, 48},
            {36, 23, 10, 49},
            {37, 24, 11, 50},
            {38, 25, 12, 51},
            {39, 26, 13, 52},
            {40, 27, 14, 53},
            {41, 28, 15, 2},
            {42, 29, 16, 3},
            {43, 30, 17, 4},
            {44, 31, 18, 5},
            {45, 32, 19, 6},
            {46, 33, 20, 7},
            {47, 34, 21, 8},
            {48, 35, 22, 9},
            {49, 36, 23, 10},
            {50, 37, 24, 11},
            {51, 38, 25, 12},
            {52, 39, 26, 13},
            {54, -1, -1, -1},
            {55, -1, -1, -1},
            {56, -1, -1, -1},
            {57, -1, -1, -1},
            {58, -1, -1, -1},
            {59, -1, -1, -1},
            {-1, 54, -1, -1},
            {-1, 55, -1, -1},
            {-1, 56, -1, -1},
            {-1, 57, -1, -1},
            {-1, 58, -1, -1},
            {-1, 59, -1, -1},
            {-1, -1, 54, -1},
            {-1, -1, 55, -1},
            {-1, -1, 56, -1},
            {-1, -1, 57, -1},
            {-1, -1, 58, -1},
            {-1, -1, 59, -1},
            {-1, -1, -1, 54},
            {-1, -1, -1, 55},
            {-1, -1, -1, 56},
            {-1, -1, -1, 57},
            {-1, -1, -1, 58},
            {-1, -1, -1, 59}
    };
    /**
     * Arraylist over all the players
     * 0-3   RED PIECES
     * 4-7   BLUE PIECES
     * 8-11  YELLOW PIECES
     * 12-15 GREEN PICES
     */
    private ArrayList<Integer> pieces = new ArrayList<>();
    //Which players turn it is
    private int activeplayer = 0;

    //A unique identifier for a game
    //This gameid have to be the key in the hashmap for the server and the client
    //and have to match for the client and the server
    private String gameid;

    //the current number of throws for the active player
    private int numberofthrowsfortheactiveplayer = 0;

    //A boolean to check if all number of throws are six so far
    private boolean allsix = true;

    //number on the dice from the last throw
    private int dice = -1;

    //List over the playernames currently in the game
    private ArrayList<String> gameusers = new ArrayList<>();

    public ArrayList<String> getGameUsers() {
        return gameusers;
    }

    //Arraylist over picelisteners
    private ArrayList<PieceListener> picelisteners = new ArrayList<>();
    //Arraylist with dicelisteners
    private ArrayList<DiceListener> dicelisteners = new ArrayList<>();
    //Arraylist with playerlisteners
    private ArrayList<PlayerListener> playerlisteners = new ArrayList<>();

    /**
     * Creates an empty game with no players in it
     * and initializes all of the players tokens
     */
    public Ludo() {
        initTokens();
    }

    /**
     * Creates a game with players passed as argument
     * if  more than one player: initialize board
     * else notEnoughPlayersException
     *
     * @param player1 name of player1 | null
     * @param player2 name of player2 | null
     * @param player3 name of player3 | null
     * @param player4 name of player4 | null
     */
    public Ludo(String player1, String player2, String player3, String player4) {
        //Need to have a function that adds the tokens to a 'list' or array structure together with the player

        //See if the players are not null and add them to the list of users
        //need to check all playernames, hence the 'nested' if
        if (player1 != null) {
            gameusers.add(player1);
        }
        if (player2 != null) {
            gameusers.add(player2);
        }
        if (player3 != null) {
            gameusers.add(player3);
        }
        if (player4 != null) {
            gameusers.add(player4);
        }
        //Figure out the number of players
        numberofplayers = gameusers.size();
        //If the size of the list is below 0: throw exception
        if (numberofplayers < 2) {
            throw new NotEnoughPlayersException("A game needs more than one player");
        } else {
            //fll the missing players with null to make the test work...
            int index = 4 - gameusers.size();
            for (int counter = 0; counter < index; counter++) {
                gameusers.add(null);
            }
        }
        //Initialize all the tokens to position zero
        initTokens();
    }

    /**
     * Get all the users in the game as a jsonarray
     * @return jsonarray with all the users in the game
     */
    public JSONArray getUsernames() {
        return new JSONArray(gameusers);
    }

    /**
     * Returns the dice that was thrown in the current round...
     * @return the current dicethrow
     */
    public int getDice() {
        return dice;
    }

    public boolean getAllSix() { return allsix; }

    /**
     * Initialize all of the players tokens at position 0
     */
    private void initTokens() {
        for (int addtoken = 0; addtoken < 16; addtoken++) {
            pieces.add(0);
        }
    }

    public int getNumberofthrowsfortheactiveplayer() {
        return numberofthrowsfortheactiveplayer;
    }


    /**
     * Returns number of players in game
     * @return nrplayers
     */
    public int nrOfPlayers() {
        return this.numberofplayers;
    }

    /**
     * Order of colors given to new players:
     * RED - BLUE - YELLOW - GREEN
     * if it is 2 players in the game:
     * player1 = RED,  player2 = BLUE
     *
     * @param playerconst the int of the player
     * @return            the name of the player with the color 'playerconst'
     */
    public String getPlayerName(int playerconst) {
        //return name if it's not null
        return gameusers.get(playerconst);
    }

    /**
     * gets the player constant using username
     * @param playername the username
     * @return the playerconst, -1 should never happen
     */
    public int getPlayerConst(String playername) {
        for (int i = 0; i < nrOfPlayers(); i++) {
            if (gameusers.get(i).equals(playername)) {
                return i;
            }
        }
        // should not happen
        return -1;
    }

    /**
     * Returns number of active players in game
     * @return nractive
     */
    int activePlayers() {
        //number of active players
        int numberofactiveplayers = 0;
        //Loop through all the players
        for (int i = 0; i < numberofplayers; i++) {
            //if the gameusers name is not containing "Inactive: " the player is active.
            if (!gameusers.get(i).contains("Inactive: ")) {
                numberofactiveplayers++;
            }
        }               //return the number of active users.
        return numberofactiveplayers;
    }

    /**
     * Getter for the unique game identifier
     * @return the gameid
     */
    public String getGameId() {
        return gameid;
    }

    /**
     * SERVER function:
     * generates a random game id for the current game
     * @return the gameid
     */
    public String setGameId() {
        this.gameid = UUID.randomUUID().toString();
        return this.gameid;
    }

    /**
     * CLIENT function:
     * Sets the gameid as the gameid from the parameter
     * @param gameid the game id passsed from the server
     */
    public void setGameId(String gameid) {
        this.gameid = gameid;
    }

    /**
     * Mark the player as inactive
     * "Inactive: Player B"
     * @param playername name of player to remove
     */
    public void removePlayer(String playername) {
        //Return the index in the list of player "playername"
        int index = gameusers.indexOf(playername);
        //Change the "index" players name to start with inactive
        gameusers.set(index, String.format("Inactive: %s", gameusers.get(index)));

        //Create a playerevent that indicates that the player left the game
        PlayerEvent tmpevent = new PlayerEvent(this, index, PlayerEvent.LEFTGAME);
        for(PlayerListener playerlistener : playerlisteners){
            playerlistener.playerStateChanged(tmpevent);
        }
        //Get the next active player
        nextPlayer();

    }

    /**
     * Returns which players turn it is
     * @return playerturn the const of the players turn
     */
    public int activePlayer() {
        return activeplayer;
    }

    /**
     * converts the usergridindex to a common ludoboardindex
     *
     * @param playerconst players name
     * @param position    players position
     * @return the users position on the ludoboardGrid (the row in the two dim. array ) | -1 if not found / invalid
     */
    int userGridToLudoBoardGrid(int playerconst, int position) {
        //Hardcoded the positions of the 1 index for each of the users
        //Because 56 and 1 is one the same field on the board.
        if (playerconst == Ludo.RED && position == 1) {
            return 16;
        } else if (playerconst == Ludo.BLUE && position == 1) {
            return 29;
        } else if (playerconst == Ludo.YELLOW && position == 1) {
            return 42;
        } else if (playerconst == Ludo.GREEN && position == 1) {
            return 55;
        } else {     //If the position isn't the first for each color:
                    //Loop all the rows for that column and find the right position and return the ludoposition
            for (int row = 0; row <= userToLudoBoard.length; row++) {
                if (userToLudoBoard[row][playerconst] == position) {
                    return row;
                }
            }
        }//This is never going to happen i think...
        return -1;
    }

    /**
     * SERVER function
     * Adds a player to the game and updates the number of current players
     * make sure that the number of players joining are allowed (max 4)
     * throw a NoRoomForMorePlayersException
     *
     * @param playername name of the added player
     */
    public void addPlayer(String playername) {
        //If the new player makes the total higher than four players: throw exception
        if (nrOfPlayers() >= 4) {
            throw new NoRoomForMorePlayersException("A game cannot contain more than four players");
        } else if (gameusers.contains(playername)) {
            throw new NoRoomForMorePlayersException("A game cannot contain multiple instances of same user");
        } else { //It is space for the new player to join the game
            //add the playername to the list
            gameusers.add(playername);
            //Set the new number of players in the game
            numberofplayers = gameusers.size();
            //Create a new playerevent for the joined player
            PlayerEvent tmpevent = new PlayerEvent(this, gameusers.size() -1, PlayerEvent.JOINED);
            for(PlayerListener playerlistener : playerlisteners){
                playerlistener.playerStateChanged(tmpevent);
            }
        }
    }

    /**
     * CLIENT function:
     * Ensures that a player is added with the same color as it is on the server
     * Adds a player to the game and updates the number of current players
     * make sure that the number of players joining are allowed (max 4)
     * throw a NoRoomForMorePlayersException
     * @param playername name of the added player
     * @param playercolor the color of the player, which equals the playerconst
     */
    public void addPlayer(String playername, int playercolor) {
        //If the new player makes the total higher than four players: throw exception
        if ((nrOfPlayers() + 1) > 4) {
            throw new NoRoomForMorePlayersException("A game cannot contain more than four players");
        } else if (gameusers.contains(playername)) {
            throw new NoRoomForMorePlayersException("A game cannot contain multiple instances of same user");
        } else { //It is space for the new player to join the game
            gameusers.add(playercolor,playername);
            //set the new number of players
            numberofplayers = gameusers.size();
            PlayerEvent tmpevent = new PlayerEvent(this, playercolor, PlayerEvent.JOINED);
            for(PlayerListener playerlistener : playerlisteners){
                playerlistener.playerStateChanged(tmpevent);
            }
        }
    }

    /**
     * Returns the position (local to that player)
     * @param playerconst Ludo.COLOR
     * @param piece       the index of the piece
     * @return position of the token
     */
    int getPosition(int playerconst, int piece) {
        int offsettoplayer = ((playerconst) * NUMBEROFPIECES);
        int index = offsettoplayer + piece;
        return pieces.get(index);
    }

    /**
     * Return the status of the game
     * "created" : no players
     * "Initiated" : with players
     * "started" : dice is thrown
     * "finished": we have a winner of the game
     * @return a string based on the current status of the game
     */
    public String getStatus() {
        if(getWinner() != -1){
            return "Finished";
        }else if(dice != -1){
            return "Started";
        }else if(numberofplayers != 0){
            return "Initiated";
        }else{
            return "Created";
        }
    }

    /**
     * SERVER function:
     * Throws a dice from 1 - 6 (random)
     * loops through all the registered 'dicelisteners' and generates and generates a new event
     * Chekcs for any
     * @return the dice throw value
     */
    public int throwDice(){
        //Throw the dice
        SecureRandom random = new SecureRandom();
        //Random number from 1 - 6
        this.dice = random.nextInt(6 - 1 + 1) + 1;
        //Set allsix to true if the current throw is a six or not
        if(dice != 6){ // works with !allhome()
            allsix = false;
        }
        //Throw the dice and move the pice afterwards
        DiceEvent tmpdiceEvent = new DiceEvent(this, activeplayer, dice);
        for(DiceListener dicelistener : dicelisteners) {
            dicelistener.diceThrown(tmpdiceEvent);
        }

        //increase the number of throws
        numberofthrowsfortheactiveplayer++;

        //next players turn if the player threw six three times in a row
        if(sixThreeTimesInARow()){
            nextPlayer();
            return this.dice;
        }

        //Next players turn if none of the players pieces can move with the current throw and not all pieces in the home position
        if(!canMove() && !allHome()){
            nextPlayer();
            return this.dice;
        }


        //next players turn if the player have all pieces in the home position and have thrown the dice three times without a six on the last one
        if(allHome() && numberofthrowsfortheactiveplayer == 3 && dice != 6){
            nextPlayer();
            return this.dice;
        }

        return this.dice;
    }

    /**
     * Switches the turn to the next player
     * by sending a playerevent with the state playing and nextplayer
     */
    private void nextPlayerEvent() {
        PlayerEvent tmpevent = new PlayerEvent(this, activeplayer , PlayerEvent.PLAYING);
        for(PlayerListener playerlistener : playerlisteners){
            playerlistener.playerStateChanged(tmpevent);
        }
    }

    /**
     * Set the current player to no longer be active
     */
    private void noLongerActiveEvent() {
        PlayerEvent tmpevent = new PlayerEvent(this, activeplayer, PlayerEvent.WAITING);
        for(PlayerListener playerlistener : playerlisteners){
            playerlistener.playerStateChanged(tmpevent);
        }
    }


    /**
     * Checks if the player have thrown six three times in a row.
     * @return true if the player have thrown a six three times in a row
     */
    private boolean sixThreeTimesInARow() {
        return allsix && numberofthrowsfortheactiveplayer == 3;
    }

    /**
     * CLIENT function:
     * used by the client when a dice is coming from the dispacher.
     * This function creates a diceEvent and sends it out to all of the dicelisteners registered
     * if this function is called: the dicethrow have passed all of the checks on the server
     * @param dice          number from the server
     * @return the value of the dice throw
     */
    public int throwDice(int dice) {
        //set the currrent throw
        this.dice = dice;
        //increase the number of throws
        numberofthrowsfortheactiveplayer++;
        if(dice != 6){ // works with !allhome()
            allsix = false;
        }
        //Throw the dice and move the pice afterwards
        DiceEvent tmpdiceEvent = new DiceEvent(this, activeplayer, dice);
        for(DiceListener dicelistener : dicelisteners) {
            dicelistener.diceThrown(tmpdiceEvent);
        }

        //If a player throws six three times in row: next players turn
        if(sixThreeTimesInARow()){
            nextPlayer();
            return this.dice;
        }

        //Next players turn if none of the players pieces can move with the current throw and not all pieces in the home position
        if(!canMove() && !allHome()){
            nextPlayer();
            return this.dice;
        }


        //next players turn if the player have all pieces in the home position and have thrown the dice three times without a six on the last one
        if(allHome() && numberofthrowsfortheactiveplayer == 3 && dice != 6){
            nextPlayer();
            return this.dice;
        }

        //the throw was successful and the player is able to move a piece with the current throw / dice result
        return this.dice;
    }

    /**
     * Checks if any of the players pieces can move
     * @return true if true if at least one piece is able to move
     */
    public boolean canMove() {
        //Find the index of the first piece of the active player
        int firstpiece = ((activeplayer) * NUMBEROFPIECES);
        //the value of the current piece
        int piecevalue;
        //Loop all the players pieces
        for (int index = firstpiece;index < (firstpiece+4); index++) {
            //get the value of the pieceindex
            piecevalue = pieces.get(index);
            //The piece is able to move
            //if the piece is outside the yard and can go to the goal or less and the oppopnent does not have a tower) || (inside with a six on the dice)
            if(piecevalue != 0 && checkGoalAllowed(piecevalue) && !opponentHaveStackedPiecesBetween(index,activeplayer) || (piecevalue == 0 && dice == 6)){
                return true;
            }
        }
        return false;
    }

    /**
     * Try to move player playerconst from start to end
     * Find out which pice to move based on the start position
     * @param playerconst the int of the player
     * @param start       the start of the move
     * @param end         the end of the move
     * @return true or false based on the move
     */
    public Boolean movePiece(int playerconst, int start, int end) {
        //Find the index of the piece at the start pos
        int pieceindex = findIndexToPieceOnPosition(start, playerconst);
        if(pieces.get(pieceindex) == 53 && end < 53){
            end = pieces.get(pieceindex)+dice;
            start = pieces.get(pieceindex);
        }

        //check if the piece belongs to the 'playerconst' player
        if(pieceindex == -1 || !checkDiceAndToFromPosition(start, end) //Check if the difference between start and end matches the dice thrown
                || (!checkGoalAllowed(start) && start+dice > 59)   //Check if the move is to the end position and the number of eyes is not exact
                || (opponentHaveStackedPiecesBetween(pieceindex, playerconst)) || (dice == 0)) { //Check if an opponent have two or more pieces stacked between start and end
            return false;
        }

        if (start == 0 && dice == 6) {
            end = 1;
        } else if (start == 0) {
            return false;
        }
        //Everything is AOK: move the piece:
        moveThePiece(playerconst,start,end,pieceindex);
        //Next players turn if the piece was moved to position 1 or the dice was not 6 in the last throw
        if(dice != 6 || end == 1){
            nextPlayer();
        }
        dice = 0;
        return true;
    }

    /**
     * Move the piece and create events
     * @param playerconst   the current player
     * @param start         Start of the move
     * @param end           The end of the move
     * @param pieceindex    The index of the piece in the pieces array
     */
    private void moveThePiece(int playerconst, int start, int end, int pieceindex) {
        //Find the ID of the piece
        int pieceid = (pieceindex%4);

        //Set the new position to the piece
        pieces.set(pieceindex,end);

        //move the piece event
        PieceEvent movepieceevent = new PieceEvent(this, playerconst, pieceid, start, end);
        for(PieceListener piecelistener : picelisteners){
            piecelistener.pieceMoved(movepieceevent);
        }

        //check if the piece is landing on top of an opponent
        int opponentPieceToBeRemoved = onTopOfOpponent(pieceindex, playerconst);

        //Generate and event if the player have won
        if(won(playerconst)){
            PlayerEvent tmpevent = new PlayerEvent(this, playerconst, PlayerEvent.WON);
            for(PlayerListener playerlistener : playerlisteners){
                playerlistener.playerStateChanged(tmpevent);
            }
         //Generate an event if the move leads to an oppoent piece is moved back to start
        }

        if(opponentPieceToBeRemoved != -1) {   //Ludo,   the player which have to move back to start   ,  the pieceid of the piece  ,   where to move the piece from                , and to
            PieceEvent tmpevent = new PieceEvent(this, getPlayerFromPieceIndex(opponentPieceToBeRemoved), (opponentPieceToBeRemoved % 4), pieces.get(opponentPieceToBeRemoved).intValue(), 0);
            for (PieceListener piecelistener : picelisteners) {
                piecelistener.pieceMoved(tmpevent);
            }
            //set the opponent piece back to start position
            pieces.set(opponentPieceToBeRemoved, 0);
        }
    }

    /**
     *
     *
     * @param piece       the id of the piece (0-3)
     * @param playerconst   The owner of the piece / performer of the move
     * @return  True if there are an opponent piece on where the player wants to move to
     */
    private int onTopOfOpponent(int piece, int playerconst) {
        //Get the end position in the global ludo bard grid
        int globalEndPosition = userGridToLudoBoardGrid(playerconst, pieces.get(piece));
        //Check if there are any pieces on that particular place in the global array which is not the current players pieces
        //loop all the intervals of the players pieces
        for (int player = 0; player < numberofplayers; player++) {
            //Not search own pieces
            if (player != playerconst) {
                //Loop through all the other players pieces
                for (int pieceindex = (player * NUMBEROFPIECES); pieceindex < ((player*NUMBEROFPIECES)+4); pieceindex++) {
                    //Convert all the pieces index to the "global one"
                    int globalOpponentPosition = userGridToLudoBoardGrid(player, pieces.get(pieceindex));
                    //If the end position is the same as where there is an opponent: return true
                    if(globalEndPosition == globalOpponentPosition){
                        return pieceindex;
                    }
                }
            }
        }//The field is clear, without an opponent piece
        return -1;
    }


    /**
     * Check if the new move makes the player win
     * @return true or false based on if the player have won or not
     */
    private boolean won(int playerconst) {
        //Find the first piece of the player
        int firstpiece = (playerconst*NUMBEROFPIECES);
        //check all the pieces of that player
        for (int piece = firstpiece; piece < (firstpiece+4); piece++) {
            //if one of the pieces is not in the final field
            if(pieces.get(piece) != 59){
                return false;
            }
        }
        //All pieces is in the middle of the board
        return true;
    }

    /**
     * @param playerconst  the current player that wants to do the move
     * @return true if the opponent have two or more pieces between start and end
     */
    private boolean opponentHaveStackedPiecesBetween(int pieceindex, int playerconst) {
        int movedistance = dice;
        int globalposition = userGridToLudoBoardGrid(playerconst, pieces.get(pieceindex));
        int indexofstackedpieces = findStackedPieces(playerconst);
        //There are no stacked pieces in the whole game
        if(indexofstackedpieces == -1){
            return false;
        }
        //search for the 'global' index of the stacked pieces
        int stackedpieceglobalindex = userGridToLudoBoardGrid(getPlayerFromPieceIndex(indexofstackedpieces),pieces.get(indexofstackedpieces).intValue());

        //The stacked pieces are in between the 'start' and the 'end' position
        return stackedpieceglobalindex >= globalposition && stackedpieceglobalindex <= (movedistance + globalposition);
    }

    /**
     * Finds stacked pieces and returns the index of them in the pieces array / -1 if not found
     * @return the index / -1
     * @param playerconst the player who owns the pieces in the current move
     */
    private int findStackedPieces(int playerconst) {
        //loop all the piecs of the players in the game
        for (int player = 0; player < numberofplayers ; player++) {
            int startpiece = (player*NUMBEROFPIECES);

            //loop all the pieces of the players to see if there are any ones with matching values
            for (int firstindex = startpiece; firstindex < startpiece+4; firstindex++) {
                for (int secondindex = firstindex; secondindex < startpiece+4; secondindex++) {

                    //Find the global position of the two pieces
                    int globalfirst = userGridToLudoBoardGrid(player, pieces.get(firstindex));
                    int globalsecond = userGridToLudoBoardGrid(player, pieces.get(secondindex));

                    // if the index is not the same and the player is different and the pieces have the same value and is outside the yard
                    if(firstindex != secondindex && player != playerconst &&  globalfirst == globalsecond && globalfirst >= 16){
                        //return the index of the stacked pieces
                        return firstindex;
                    }
                }
            }
        }//No stacked pieces found
        return -1;
    }

    /**
     * Check if start + dice <= 59
     * This is becuase if the player want to move to the finishline, he have to provide the exact amount of eyes
     * @param start start pos of piece
     * @return true if the move is allowed
     */
    private boolean checkGoalAllowed(int start) {
        return (start + dice) <= 59;
    }

    /**
     * Check if the difference between the start and end is different from the dice just thrown
     * @param start start pos of move
     * @param end end pos of move
     * @return
     */
    private boolean checkDiceAndToFromPosition(int start, int end) {
        int difference = end - start;
        //Custom action if the dice is '6' and the player tries to move from 0 to 1
        if(start == 0 && end == 1 && dice == 6){
            return true;
        }
        //return true or false based on the move distance and the dice
        return difference == dice;
    }

    /**
     * Check if the person who claims to own the piece at start really does.
     * Returns the index of the found piece or -1 if not found
     * @param start         the start pos of the piece
     * @param playerconst   the player who claim to own the piece
     * @return index of the piece | -1
     */
    private int findIndexToPieceOnPosition(int start, int playerconst) {
        //pieceowner is not found
        boolean pieceowner = false;
        //find the startindex of the player
        int playerpiecestartindex = ((playerconst) * NUMBEROFPIECES);
        //Default pieceindex is not found
        int pieceindex = -1;
        //loop through it's own pieces
        for (int index = playerpiecestartindex; index < playerpiecestartindex + 4; index++){
            //if a pieces which matches the start index is found
            if(pieces.get(index) == start || pieces.get(index) == 53){
                //the owner is compared
                pieceowner = (playerconst == getPlayerFromPieceIndex(index));
            }
            //if the owner is found, the index of the piece is returned
            if(pieceowner){
                return index;
            }
        }
        return pieceindex;
    }

    /**
     * @return the index of the winning player (eg. Ludo.RED) / -1, if no players have won yet
     * Seen from the user and not the ludoarray.
     */
    int getWinner() {
        //number of pieces in the finish is zeroed
        int numberofpiecesinfinish = 0;
        //loop all the pieces
        for (int index = 0; index < pieces.size(); index++) {
            //if a player zone is changed: reset the number of pieces in finish
            if(index % NUMBEROFPIECES == 0){
                numberofpiecesinfinish = 0;
            }
            //if the piece is in the finish. increase the counter
            if(pieces.get(index) == 59){
                numberofpiecesinfinish++;
            }
            //If a player have all the pieces in finish: we have a winner
            if(numberofpiecesinfinish == 4){
                //get the owner of the pieces
                return getPlayerFromPieceIndex(index);
            }
        }
        //no winner was found
        return -1;
    }

    /**
     * Takes the index of the piece in the pieces array and returns the owner of those
     * pieces
     * @param index the index of the piece in the pieces array
     * @return      the player who owns the pieces
     */
    private int getPlayerFromPieceIndex(int index){
        return index / NUMBEROFPIECES;
    }

    /**
     * Listener to se if / and who threw a dice
     * @param dicelistener the listener 'object'
     */
    public void addDiceListener(DiceListener dicelistener) {
        dicelisteners.add(dicelistener);
    }

    /**
     * Listener to see if /and who moved a piece
     * @param piecelistener the listener 'object'
     */
    public void addPieceListener(PieceListener piecelistener) {
        picelisteners.add(piecelistener);
    }

    /**
     * adds the playerlistener to what?
     * @param playerlistener the listener 'object'
     */
    public void addPlayerListener(PlayerListener playerlistener) {
        playerlisteners.add(playerlistener);
    }

    /**
     * Set the activeplayer to the next one | wraps around
     */
    private void nextPlayer() {
        //sets the activeplayer to the next active player or wraps around
        noLongerActiveEvent();

        //Get the next player
        activeplayer = activeplayer + 1;
        activeplayer = (activeplayer % numberofplayers);

        //Find the next player which is not inactive
        while(gameusers.get(activeplayer).contains("Inactive: ")){
            activeplayer = activeplayer + 1;
            activeplayer = (activeplayer % numberofplayers);
        }

        //Reset the throws
        numberofthrowsfortheactiveplayer = 0;

        //Change players turn - event
        nextPlayerEvent();
        allsix = true;
    }

    /**
     * Check if all of the pieces to 'activeplayer' are in the yard or not
     * If they are = return true
     * if at least one is outside of the yard: return false
     * @return a boolean based on if all pieces are home
     */
    private boolean allHome(){
        boolean allhome = true;
        //Loop all of the pieces that belongs to the current player
        int firstpiece = ((activeplayer) * NUMBEROFPIECES);
        for (int startindex = firstpiece;startindex < (firstpiece+4); startindex++) {
            //if one of them is outside tha yard: return false
            if(pieces.get(startindex) != 0 && pieces.get(startindex) != 59){
                allhome = false;
            }
        }
        //all of the pieces are inside the yard: return true
        return allhome;
    }
}
