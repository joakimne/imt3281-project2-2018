package no.ntnu.imt3281.ludo.logic.dice;

import no.ntnu.imt3281.ludo.logic.Ludo;

import java.util.EventObject;

/**
 * Custom event for DiceListener
 */
public class DiceEvent extends EventObject {
    int playerconst;
    int dice;
    /**
     * The custructor for a DiceEvent
     * @param ludo the ludo class
     * @param playerconst the playerconst (ludo.RED)
     * @param dice the number on the dice
     */
    public DiceEvent(Ludo ludo, int playerconst, int dice){
        super(ludo);
        setPlayer(playerconst);
        setDice(dice);
    }

    public int getPlayer() {
        return playerconst;
    }

    public void setPlayer(int player) {
        this.playerconst = player;
    }

    public int getDice() {
        return dice;
    }

    public void setDice(int dice) {
        this.dice = dice;
    }

    public Ludo getLudo() {
        return (Ludo) this.getSource();
    }

    @Override
    public boolean equals(Object obj) {
    	if(obj instanceof DiceEvent) {
    		DiceEvent eventparam = (DiceEvent) obj;
            return eventparam.getDice() == this.dice && eventparam.getPlayer() == this.playerconst;
    	}
        return false;
    }
    @Override
    public int hashCode() {
    	// TODO Auto-generated method stub
    	return super.hashCode();
    }

}
