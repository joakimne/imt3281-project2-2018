package no.ntnu.imt3281.ludo.logic;


/**
 * A Coordinate represents a x,y position on the board.
 * This is used in a lookup table where coordinates are looked up and a block # is returned
 * 
 * x and y is the top left position in integer. 
 * 
 * @author joakimellestad
 *
 */
public class Coordinate {
	// Hold top left
	// The size is known so we can calculate any offset
	private int x;
	private int y;

	/**
	 * Initalizes a new Coordinate based on integer block positions
	 * x: 0->14
	 * y: 0->14
	 * @param x x cordinate
	 * @param y y cordinate
	 */
	public Coordinate(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Initalizes new coordinate baed on raw coordinates from the ludo board
	 * x: 0->722(Given the image 722x722)
	 * y: 0->722(Given the image 722x722)
	 * When given a coordinate this will transform it into block x,y positions
	 * @param x x cordinate
	 * @param y y cordinate
	 */
	public Coordinate(Double x, Double y) {
		this.x = (int) Math.floor(x/48.13333333333333);
		this.y = (int) Math.floor(y/48.13333333333333);
	}
	
	/**
	 * Returns the coordinate's layout coordinates. 
	 * This value can be used directly yo position a token on the board
	 * @return min value for x
	 */
	public Double getMinX() {
		return this.x * 48.13333333333333;
	}
	/**
	 * Returns the coordinate's layout coordinates. 
	 * This value can be used directly yo position a token on the board
	 * @return min value for y
	 */
	public Double getMinY() {
		return this.y * 48.13333333333333;
	}
	/**
	 * Getter for the property x
	 * @return x value
	 */
	public int getX() {
		return this.x;
	}
	/**
	 * Getter for the property y
	 * @return y value
	 */
	public int getY() {
		return this.y;
	}
	
	/**
	 * Return the x center for this block
	 * This is calculated using the hardcoded half of a block width 24.0666666667
	 * This value can be used directly to position a token on the board
	 * @return center of the block x axis
	 */
	public Double centerX() {
		return (this.x* 48.13333333333333) + 24.0666666667;
	}
	/**
	 * Return the y center for this block
	 * This is calculated using the hardcoded half of a block width 24.0666666667
	 * This value can be used directly to position a token on the board
	 * @return center of the block y axis
	 */
	public Double centerY() {
		return (this.y * 48.13333333333333) + 24.0666666667;
	}
	
	/**
	 * Check if a given coordinate is within this objects coordinates
	 * 
	 * @param coordinate cordinate object describing cordinate
	 * @return true or false depending on it being within x and y
	 */
	public Boolean isWithin(Coordinate coordinate) {
		return (coordinate.getX() == this.x && coordinate.getY() == this.y);
	}
}
