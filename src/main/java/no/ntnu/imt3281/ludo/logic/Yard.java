package no.ntnu.imt3281.ludo.logic;

/**
 * A yard is the starting place for tokens when a game begins
 * This is used as the index 0 in the lookup table.
 * This holds the position of blocks within the yard. 
 * @author joakimellestad
 *
 */
public class Yard extends Coordinate {

	/**
	 * This will support checking if a click is withing yard bounds
	 */
	private Coordinate top;
	private Coordinate right;
	private Coordinate left;
	private Coordinate bottom;

	/**
	 * Constructor for a yard, where the pieces start
	 * @param yardBounds bounds for the yard
	 * @param top top pos
	 * @param left left pos
	 * @param right right pos
	 * @param bottom bottom pos
	 */
	public Yard(Coordinate yardBounds, Coordinate top, Coordinate left, Coordinate right, Coordinate bottom) {
		super(yardBounds.getMinX(), yardBounds.getMinY());
		this.top = top;
		this.right = right;
		this.left = left;
		this.bottom = bottom;
	}
	
	/**
	 * Returns the coordinate for a position (top,left,right,bottom) within this yard
	 * @param tokPos the piece to get pos of
	 * @return the pos of the piece
	 */
	public Coordinate getYardPosition(TokenPosition tokPos) {
		switch (tokPos) {
		case TOP:
			return this.top;
		case LEFT:
			return this.left;
		case RIGHT:
			return this.right;
		case BOTTOM:
			return this.bottom;
		default:
			return null; //This never happens...hopefully - can this be done better?
		}
	}
	
	/**
	 * Need to check tokens within the yard
	 * @param coordinate - A coordinate to check if this yard is within given coordinate
	 */
	@Override
	public Boolean isWithin(Coordinate coordinate) {
		return ((coordinate.getX() == this.top.getX() && coordinate.getY() == this.top.getY())
				|| (coordinate.getX() == this.left.getX() && coordinate.getY() == this.left.getY())
				|| (coordinate.getX() == this.right.getX() && coordinate.getY() == this.right.getY())
				|| (coordinate.getX() == this.bottom.getX() && coordinate.getY() == this.bottom.getY()));

	}

}