package no.ntnu.imt3281.ludo.logic;

/**
 * Exception to handle if there are not enough players in a game
 */
public class NotEnoughPlayersException extends RuntimeException {

    /**
     * Creates an exception
     * @param message an optional message to pass to the exception
     */
    public NotEnoughPlayersException(String message)
    {
        super(message);
    }

    /**
     * Default constructor for the exception, if no message is passed
     * It sets a default one
     */
    public NotEnoughPlayersException( )
    {
        super("Not enough players");
    }

    /**
     * Tostring method to return the message from the exception
     * @return the message
     */
    @Override
    public String toString() {
        String s = getClass().getName();
        String message = getLocalizedMessage();
        return (message != null) ? (s + ": " + message) : s;
    }
}
