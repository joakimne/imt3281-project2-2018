package no.ntnu.imt3281.ludo.global;

/**
 * Class to keep the constants of the different request ID's
 */
public class CommunicationID {
	/**
	 * Static String with ID #1 Registration
	 */
    public static final String REGISTRATION = "1";
    /**
	 * Static String with ID #2 Roll the Dice
	 */
    public static final String RTD = "2";
    /**
	 * Static String with ID #3 Move piece
	 */
    public static final String MOVE = "3";
    /**
	 * Static String with ID #4 chat message
	 */
    public static final String CHATMESSAGE = "4";
    /**
	 * Static String with ID #5 Start a new game
	 */
    public static final String STARTAGAME = "5";
    /**
	 * Static String with ID #6 Join a game
	 */
    public static final String JOINGAME = "6";
    /**
	 * Static String with ID #7 Log out
	 */
    public static final String LOGOUT = "7";
    /**
	 * Static String with ID #8 Log in
	 */
    public static final String LOGIN = "8";
    /**
	 * Static String with ID #9 Error
	 */
    public static final String ERROR = "9";
    /**
	 * Static String with ID #10 Player state changed
	 */
    public static final String PLAYERSTATE = "10";
    /**
	 * Static String with ID #11 Authorize
	 */
    public static final String AUTHORIZE = "11";
    /**
	 * Static String with ID #12 List available games
	 */
    public static final String LISTAVAILABLEGAMES = "12";
    /**
	 * Static String with ID #13 List chats
	 */
    public static final String LISTCHATS = "13";
    /**
	 * Static String with ID #14 join chat room
	 */
    public static final String JOINCHATROOM = "14";
    /**
	 * Static String with ID #15 all chat room users
	 */
    public static final String CHATROOMUSERS = "15";
    /**
	 * Static String with ID #16 Invite players
	 */
    public static final String INVITE = "16";
    /**
     * Static string with ID #17 Statistics
     */
    public static final String STATISTICS = "17";
    
    private CommunicationID() {}
}