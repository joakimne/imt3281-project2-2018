package no.ntnu.imt3281.ludo.global;

import org.json.JSONObject;

import java.util.ResourceBundle;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class to keep global functions
 */
public class Toolkit {

	/**
	 * Checks if an email is in valid format (RFC 5322)
	 * @param email the email to check
	 * @return either true or false
	 */
	public static boolean checkValidEmail(String email) {
		String regex = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$";
		return email.matches(regex);
	}
	
	private Toolkit() {}
}
