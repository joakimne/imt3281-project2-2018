package no.ntnu.imt3281.ludo.gui;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.FileNotFoundException;

import javafx.scene.image.Image;

/**
 * A static class which sets the image to use for diceThrown
 * @author Bendik
 *
 */

public final class diceimages {
	/**
	 * Returns a dice image based on the given eyes
	 * @param eyes amount of eyes on the dice.
	 * @throws FileNotFoundException if it doesnt find the file
	 * @return the image
	 */
	public static Image getImage(int eyes) throws FileNotFoundException {
		String fd = "images/dice" + eyes + ".png";
		InputStream imgst = new BufferedInputStream(diceimages.class.getClassLoader().getResourceAsStream(fd));
		return new Image(imgst);
	}

	private diceimages(){}
}
