package no.ntnu.imt3281.ludo.gui;

import java.io.BufferedInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import javafx.scene.image.Image;
import no.ntnu.imt3281.ludo.logic.TokenEnum;

/**
 * A static class to hold possible token images and heir filepath
 * Possible to merge with {@link TokenEnum}
 * @author joakimellestad
 *
 */
public final class AvailableTokens {
	
	private static final String BASKETBALL = "images/tokens/basketball.png";
	private static final String CANNONNFILLED = "images/tokens/cannon-filled.png";
	private static final String DOUBLECHOCOLATECAKE = "images/tokens/double-chocolate-cake.png";
	private static final String EQUESTRIANCHOCOLATE = "images/tokens/equestrian-statue-png";
	private static final String GAMECONTROLLER = "images/tokens/game-controller.png";
	private static final String MEGAPHONE = "images/tokens/megaphone.png";
	private static final String PAWN = "images/tokens/pawn";
	private static final String PIECEOFCAKE = "images/tokens/piece-of-cake.png";
	private static final String SUBMACHINEGUN = "images/tokens/submachine-gun.png";
	
	private AvailableTokens() {}
	
	/**
	 * Return a image based on the given tokenum
	 * @param tok Which token/piece to use
	 * @return on success returns the image/token
	 * @throws FileNotFoundException if it doesnt find the file this is thrown
	 */
	public static Image getImage(TokenEnum tok) throws FileNotFoundException {
		String fd;
		switch (tok) {
		case BASKETBALL:
			fd = AvailableTokens.BASKETBALL;
			break;
		case CANNONNFILLED:
			fd = AvailableTokens.CANNONNFILLED;
			break;
		case DOUBLECHOCOLATECAKE:
			fd = AvailableTokens.DOUBLECHOCOLATECAKE;
			break;
		case EQUESTRIANCHOCOLATE:
			fd = AvailableTokens.EQUESTRIANCHOCOLATE;
			break;
		case GAMECONTROLLER:
			fd = AvailableTokens.GAMECONTROLLER;
			break;
		case MEGAPHONE:
			fd = AvailableTokens.MEGAPHONE;
			break;
		case PAWN:
			fd = AvailableTokens.PAWN;
			break;
		case PIECEOFCAKE:
			fd = AvailableTokens.PIECEOFCAKE;
			break;
		case SUBMACHINEGUN:
			fd = AvailableTokens.SUBMACHINEGUN;
			break;
		default:
			throw new FileNotFoundException("TokenEnum not supported");
		}
		
		InputStream imgst = new BufferedInputStream(AvailableTokens.class.getClassLoader().getResourceAsStream(fd));
		return new Image(imgst);
	}
	
	

}
