package no.ntnu.imt3281.ludo.gui;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.application.Platform;
import org.json.JSONObject;

import javafx.event.ActionEvent;

/*
  Sample Skeleton for 'GameBoard.fxml' Controller Class
 */

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import no.ntnu.imt3281.chat.ChatEvent;
import no.ntnu.imt3281.chat.Message;
import no.ntnu.imt3281.ludo.client.Client;
import no.ntnu.imt3281.ludo.global.CommunicationID;
import no.ntnu.imt3281.ludo.logic.Coordinate;
import no.ntnu.imt3281.ludo.logic.Ludo;
import no.ntnu.imt3281.ludo.logic.LudoCoordinateToBlockLookup;
import no.ntnu.imt3281.ludo.logic.Player;
import no.ntnu.imt3281.ludo.logic.TokenEnum;
import no.ntnu.imt3281.ludo.logic.TokenPosition;
import no.ntnu.imt3281.ludo.logic.dice.DiceEvent;
import no.ntnu.imt3281.ludo.logic.dice.DiceListener;
import no.ntnu.imt3281.ludo.logic.piece.PieceEvent;
import no.ntnu.imt3281.ludo.logic.piece.PieceListener;
import no.ntnu.imt3281.ludo.logic.player.PlayerEvent;
import no.ntnu.imt3281.ludo.logic.player.PlayerListener;
import no.ntnu.imt3281.ludo.ontheline.ChatRoomEventListener;
import no.ntnu.imt3281.ludo.ontheline.OnTheLineEvent;


/**
 * Implements
 * The GameEvents Listener implements methods that are called by the Client when responses are received
 *
 * @author joakimellestad
 */
public class GameBoardController implements PlayerListener, PieceListener, DiceListener, ChatRoomEventListener {

	private static final Logger LOGGER = Logger.getLogger(GameBoardController.class.getName());

	@FXML
	private Label player1Name;

	@FXML
	private ImageView player1Active;

	@FXML
	private Label player2Name;

	@FXML
	private ImageView player2Active;

	@FXML
	private Label player3Name;

	@FXML
	private ImageView player3Active;

	@FXML
	private Label player4Name;

	@FXML
	private ImageView player4Active;

	@FXML
	private ImageView diceThrown;

	@FXML
	private Button throwTheDice;

	@FXML
	private TextArea chatArea;

	@FXML
	private TextField textToSay;

	@FXML
	private Button sendTextButton;

	@FXML
	private Pane ludoContainer;

	@FXML
	private ImageView ludoBoardImage;

	private ArrayList<Token> tokens = new ArrayList<>(16);

	private String gameid;
	private String chatRoomid;


	@FXML
	void initialize() {
		for (int i = 0; i < 16; i++) {
			tokens.add(i, null);
		}

		LOGGER.log(Level.INFO,"GameBoardController initialize");
	}

	/**
	 * Called when the user presses the throw dice button on the gui
	 *
	 */
	@FXML
	void throwTheDiceButton() {
		LOGGER.log(Level.INFO, "User pressed throw dice button");
		OnTheLineEvent sending = new OnTheLineEvent(null, null, CommunicationID.RTD, Arrays.asList("gameid", this.gameid));
		try {
			Client.getConnection().send(sending.toString());
			throwTheDice.setDisable(true);

		} catch (IOException e) {
			LOGGER.log(Level.WARNING, "Semd to be some problems with communication" + e.getMessage(), e);
		}
	}

	/**
	 * Called when the user wants to send his message in the chat
	 * But careful - what if the text is empty...
	 *
	 * @param event
	 */
	@FXML
	void sendMessageInChat(ActionEvent event) {
		LOGGER.log(Level.INFO, "User pressed send message button");
		String messageToSend = this.textToSay.getText();
		OnTheLineEvent sending;
		try {
			sending = new OnTheLineEvent(null, null, CommunicationID.CHATMESSAGE, Arrays.asList("chatroomid", this.chatRoomid, "message", messageToSend));
			Client.getConnection().send(sending.toString());
			this.textToSay.clear();
		} catch (IOException e) {
			LOGGER.log(Level.INFO, "Fetching token or sending request failed." + e.getMessage(), e);
		}
	}

	/**
	 * Closes the tab which has been pressed close on.
	 */
	void closeTab() {
		LOGGER.log(Level.INFO, "Closing tab");
		try {
			Ludo game = Client.getLudoGame(this.gameid);
			game.removePlayer(Client.getUsername());


		} catch (Exception e) {
			LOGGER.log(Level.WARNING, "Could not find game locally" + e.getMessage(), e);
		}


	}

	/**
	 * Call this to make the GameBoard controller register itself
	 * for events
	 * PlayerListener, PieceListener, DiceListener, ChatRoomEventListener
	 * @param body The vody of the response from server.
	 */
	void registerYourself(JSONObject body) {

		Client.addNewLudoGame(this.gameid, this.chatRoomid, this, this, this, this, body);

		Client.addChatRoomEventListener(this, this.chatRoomid);
	}

	/**
	 * Used when a new player is to be added to the game board
	 * <p>
	 * Creates 4 tokens for the player
	 * Adds token to runlater as they are created
	 * <p>
	 * Hardcoding the length isnt very good.
	 *
	 * @param player
	 * @param nickname
	 */
	private void initializeNewPlayerOnBoard(int player, String nickname) {
		TokenPosition position = TokenPosition.TOP;
		TokenEnum token;

		//Sets the players pieces and name hardcoded.
		int redCap = 4;
		int blueCap = 8;
		int greenCap = 16;
		int yellowCap = 12;
		switch (player) {
		case 0:
			token = TokenEnum.GAMECONTROLLER;
			player1Name.setText(nickname);
			for (int i = 4 * 0; i < redCap; i++) {
				Token tok = new Token(Player.RED, position, nickname, token);
				this.tokens.set(i, tok);
				// Only the actual player can press the buttons
				if (Client.getUsername().equals(nickname)) {
					tok.setOnAction(this::userClickedOnToken);
				}
				this.ludoContainer.getChildren().add(tok);

				position = position.next();
			}
			break;
		case 1:
			token = TokenEnum.DOUBLECHOCOLATECAKE;
			player2Name.setText(nickname);
			for (int i = 4 * 1; i < blueCap; i++) {
				Token tok = new Token(Player.BLUE, position, nickname, token);
				this.tokens.set(i, tok);
				if (Client.getUsername().equals(nickname)) {
					tok.setOnAction(this::userClickedOnToken);
				}
				this.ludoContainer.getChildren().add(tok);

				position = position.next();
			}
			break;
		case 2:
			token = TokenEnum.BASKETBALL;
			player3Name.setText(nickname);
			for (int i = 4 * 2; i < yellowCap; i++) {
				Token tok = new Token(Player.YELLOW, position, nickname, token);
				this.tokens.set(i, tok);
				if (Client.getUsername().equals(nickname)) {
					tok.setOnAction(this::userClickedOnToken);
				}
				this.ludoContainer.getChildren().add(tok);

				position = position.next();
			}
			break;
		case 3:
			token = TokenEnum.CANNONNFILLED;
			player4Name.setText(nickname);
			for (int i = 4 * 3; i < greenCap; i++) {
				Token tok = new Token(Player.GREEN, position, nickname, token);
				this.tokens.set(i, tok);
				if (Client.getUsername().equals(nickname)) {
					tok.setOnAction(this::userClickedOnToken);
				}
				this.ludoContainer.getChildren().add(tok);
				position = position.next();
			}
			break;
		default:
			break;
		}
	}

	/**
	 * Sets the game id for this game
	 * Also registers this game to receive
	 *
	 * @param id
	 */
	void setGameId(String id) {
		this.gameid = id;
	}

	private String getGameId() {
		return this.gameid;
	}

	/**
	 * Set the chatroomid for this game
	 *
	 * @param chatroomid
	 */
	void setChatRoomid(String chatroomid) {
		this.chatRoomid = chatroomid;
	}

	public String getChatRoomId() {
		return this.chatRoomid;
	}

	/**
	 * Should be triggeren when a user clicks on a token
	 */
	private void userClickedOnToken(ActionEvent event) {
		Token token = (Token) event.getSource();
		Player player = token.getPlayer();
		Coordinate tokenPositionCoordinate = new Coordinate(token.getLayoutX(), token.getLayoutY());

		try {
			int startposition = LudoCoordinateToBlockLookup.getBlockPositionCoordinate(tokenPositionCoordinate, player);
			Ludo game = Client.getLudoGame(this.gameid);
			int dice = game.getDice();
			int endposition = startposition + dice;

			OnTheLineEvent sending = new OnTheLineEvent(null,null,
					CommunicationID.MOVE,Arrays.asList("gameid",this.getGameId(),
							"startposition", startposition, "endposition", endposition, "player", player.getPlayerNumber()));
			LOGGER.log(Level.INFO, "Token pressed: " + player.getColorName());
			Client.getConnection().send(sending.toString());
		} catch (Exception e) {
			LOGGER.log(Level.INFO, "Could not look up the token coordinate: " + e.getMessage(), e);
		}
	}

	/**
	 * Sets the player who threw the dice's image to the dice thrown.
	 * Sets the imageView diceThrown to the correct image.
	 * This function is called from inside the ludo class
	 */
	@Override
	public void diceThrown(DiceEvent diceevent) {
		Ludo ludoGame = (Ludo) diceevent.getSource();
		//          you are the current active player                               and    you have all six   or number of throws for the current player is below 3  and you can not move
		if (ludoGame.activePlayer() == ludoGame.getPlayerConst(Client.getUsername()) && (ludoGame.getAllSix() || ludoGame.getNumberofthrowsfortheactiveplayer() < 3) && !ludoGame.canMove()) {
			// enable button
			throwTheDice.setDisable(false);
		}
		switch (diceevent.getPlayer()) {
		case 0:
			try {
				this.player1Active.setImage(diceimages.getImage(diceevent.getDice()));
			} catch (FileNotFoundException e1) {
				LOGGER.log(Level.WARNING, "Setting player1 dice image failed: " + e1.getMessage(), e1);
			}break;
		case 1:
			try {
				this.player2Active.setImage(diceimages.getImage(diceevent.getDice()));
			} catch (FileNotFoundException e1) {
				LOGGER.log(Level.WARNING, "Setting player2 dice image failed: " + e1.getMessage(), e1);
			}break;
		case 2:
			try {
				this.player3Active.setImage(diceimages.getImage(diceevent.getDice()));
			} catch (FileNotFoundException e1) {
				LOGGER.log(Level.WARNING, "Setting player3 dice image failed: " + e1.getMessage(), e1);
			}break;
		case 3:
			try {
				this.player4Active.setImage(diceimages.getImage(diceevent.getDice()));
			} catch (FileNotFoundException e1) {
				LOGGER.log(Level.WARNING, "Setting player4 dice image failed: " + e1.getMessage(), e1);
			}break;
		default:
			break;
		}
		if (Client.getUsername().equals(ludoGame.getPlayerName(diceevent.getPlayer()))) {
			try {
				// Needs an image in parameter, calls for the return of the file
				this.diceThrown.setImage(diceimages.getImage(diceevent.getDice()));
			} catch (FileNotFoundException e) {
				LOGGER.log(Level.WARNING, "could not set image: " + e.getMessage(), e);
			}
		}
	}


	/**
	 * Moves a piece from one position to another.
	 * This function is called from inside the ludo class
	 */
	@Override
	public void pieceMoved(PieceEvent event) {

		Ludo ludoGame = (Ludo) event.getSource();
		int piece = event.getPieceid();
		int playerconst = event.getPlayerconst();
		int endPos = event.getEnd();

		//If you are the active player and threw all sixes
		if (ludoGame.activePlayer() == ludoGame.getPlayerConst(Client.getUsername()) && ludoGame.getAllSix()) {
			throwTheDice.setDisable(true);
		}

		Token token = tokens.get(piece + 4 * playerconst);
		LOGGER.log(Level.INFO, String.format("Moving piece %d from %d to %d ", (piece + 4 * playerconst), event.getStart(), event.getEnd()));
		token.moveToken(playerconst, endPos);

		if (ludoGame.activePlayer() == ludoGame.getPlayerConst(Client.getUsername()) && ludoGame.getAllSix()) {
			throwTheDice.setDisable(false);
		}
	}


	/**
	 * Any change in the player state is sent here and handled.
	 * <p>
	 * JOINED player is added to the ludo object.
	 * LEFTGAME is removed from the ludo object.
	 * PLAYING has the throw dice button enabled.
	 * WAITING has the button disabled.
	 * WON displays a message in chat.
	 * <p>
	 * This function is called from the ludo class
	 */
	@Override
	public void playerStateChanged(PlayerEvent event) {
		Ludo ludoGame = (Ludo) event.getSource();

		// if not your turn, disable button
		if (ludoGame.getPlayerConst(Client.getUsername()) != ludoGame.activePlayer()) {
			throwTheDice.setDisable(true);
		} else { // if your turn, enable
			throwTheDice.setDisable(false);
		}
		int playerconst = event.getActiveplayer();
		String playerName = ludoGame.getPlayerName(playerconst);

		String color;

		if (playerconst == 0) {
			color = "RED";
		} else if (playerconst == 1) {
			color = "BLUE";
		} else if (playerconst == 2) {
			color = "YELLOW";
		} else {
			color = "GREEN";
		}

		switch (event.getState()) {
		case PlayerEvent.JOINED:
			//Set up the tokens for the current player
			Platform.runLater(() -> this.initializeNewPlayerOnBoard(playerconst, playerName));
			//Make sure everyone but red have it's throwdice button disabled
			ChatEvent chatjoinedevent = new ChatEvent(new Message(String.format(" %s have joined the game as %s pieces", playerName, color), "SYSTEM"), this.chatRoomid);
			Platform.runLater(() -> newChatMessageEvent(chatjoinedevent));
			break;
		case PlayerEvent.LEFTGAME:
			//Notify players in the chat that player x have left the game
			ChatEvent chat = new ChatEvent(new Message(String.format(" %s have left the game", playerName), "SYSTEM"), this.chatRoomid);
			Platform.runLater(() -> newChatMessageEvent(chat));
			break;
		case PlayerEvent.PLAYING:
			ChatEvent chatplayingevent = new ChatEvent(new Message(String.format(" %s's turn to throw the dice", playerName), "SYSTEM"), this.chatRoomid);
			Platform.runLater(() -> newChatMessageEvent(chatplayingevent));

			break;
		case PlayerEvent.WAITING:
			break;
		case PlayerEvent.WON:
			//Notify the other players that a player have won the game
			ChatEvent chatwonevent = new ChatEvent(new Message(String.format(" %s have won the game", playerName), "SYSTEM"), this.chatRoomid);
			Platform.runLater(() -> newChatMessageEvent(chatwonevent));
			break;
		default:
			break;
		}

	}


	@Override
	public void newChatMessageEvent(ChatEvent event) {
		Message message = new Message(event.getEvent());
		String post = message.getpublishedTime() + " - " + message.getPublisher() + ": " + message.getMessage() + "\n";
		javafx.application.Platform.runLater(() -> this.chatArea.appendText(post));

	}


	/**
	 * This function is not useful for a Game
	 * The Ludo already handles players.
	 */
	@Override
	public void subscribersInChatRoomChanged(ChatEvent event) {
		LOGGER.log(Level.INFO, "Unimplemented method");
	}

	@Override
	public void joinChatRoomResponse(OnTheLineEvent event) {
		LOGGER.log(Level.INFO, "Unimplemented in gameBoardControler..doesnt bother. Should never be called though");
	}


}