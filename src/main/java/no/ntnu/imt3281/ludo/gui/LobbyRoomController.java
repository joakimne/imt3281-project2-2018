package no.ntnu.imt3281.ludo.gui;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONArray;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextFlow;
import no.ntnu.imt3281.chat.ChatEvent;
import no.ntnu.imt3281.chat.Message;
import no.ntnu.imt3281.ludo.client.Client;
import no.ntnu.imt3281.ludo.global.CommunicationID;
import no.ntnu.imt3281.ludo.ontheline.ChatRoomEventListener;
import no.ntnu.imt3281.ludo.ontheline.OnTheLineEvent;


/**
 * Controller for all the chat tabs.
 * Implements ChatRoomEventListener so that relevant methods are called when responses are received from server.
 * @author Bendik
 */
public class LobbyRoomController implements ChatRoomEventListener {

	private static final Logger LOGGER = Logger.getLogger(LobbyRoomController.class.getName());
	
	private String chatRoomId;
	public void setChatRoomId(String chatroomid) {
		this.chatRoomId = chatroomid;
	}
	
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private ScrollPane textContainer;

    @FXML
    private TextFlow chatflowarea;

    @FXML
    private TextField chatinputfield;

    @FXML
    private Button sendchatbutton;

    @FXML
    private ListView<String> memberlistview;

    @FXML
    private Button invitebutton;
    
    
    private ObservableList<String> selectedMembers;
    
    
    
    /**
     * Fetches the players you selected from listView.
     * Sends an invite request to the server. (ID 16)
     */
    @FXML
    void invitePlayersToGame(ActionEvent event) {
    	JSONArray invites = new JSONArray();

    	this.selectedMembers = memberlistview.getSelectionModel().getSelectedItems();

    	for (String string : this.selectedMembers) {
			invites.put(string);
			LOGGER.log(Level.INFO, String.format("Selected item is: %s", string));
		}
    	OnTheLineEvent sending = new OnTheLineEvent(null, null, CommunicationID.INVITE, Arrays.asList("inviter", Client.getUsername(), "invitedplayers", invites));
    	try {
    		Client.getConnection().send(sending.toString());
		} catch (IOException e) {
			LOGGER.log(Level.WARNING, "Sending invite request failed" + e.getMessage(), e);
		}
    }

    /**
     * Fetches the message from the chatinputfield and sends a chat message request (ID 4) to the server.
     * @param event
     */
    @FXML
    void sendChatMessage(ActionEvent event) {
    	LOGGER.log(Level.INFO, "Sending message: ");
    	String messageToSend = this.chatinputfield.getText();
    	OnTheLineEvent sending;
		try {
			sending = new OnTheLineEvent(null, null, CommunicationID.CHATMESSAGE, Arrays.asList("chatroomid", this.chatRoomId, "token", Client.getToken(), "message", messageToSend));
			Client.getConnection().send(sending.toString());
			this.chatinputfield.clear();
		} catch (IOException e) {
			LOGGER.log(Level.INFO, "Fetching token or sending request failed: " + e.getMessage(), e);
		}
    }

    /**
     *
     * @param event describes the event
     */
    @FXML
    void selectedListClick(ActionEvent event) {
    	LOGGER.log(Level.INFO, "selected list click");
    	this.selectedMembers = this.memberlistview.getSelectionModel().getSelectedItems();
    }


    /**
     * Initializes the lobby
     */
    @FXML
    void initialize() {
    	LOGGER.log(Level.INFO, "LobbyRoomController started");
    	//Lobby is created

        //Allows for multiple selection in the list
        memberlistview.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    	this.memberlistview.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    }

    /**
     * Meant to unsubscribe the user from the room, currently not implemented.
     * @param event describes the event
     */
    public void closeTab(Event event) {
		LOGGER.log(Level.INFO, "Closing tab");
	}

    /**
     *
     * @param event describes the event
     */
	@Override
	public void newChatMessageEvent(ChatEvent event) {
		Message message = new Message(event.getEvent());
		//Scrolls to the last message in the chatarea
		chatflowarea.layout();
        textContainer.layout();
        textContainer.setVvalue(1.0f);
        
		Text post = new Text(message.getpublishedTime() + " - " + message.getPublisher() + ": " + message.getMessage() + "\n");
		post.setFont(new Font(14));
		post.setWrappingWidth(200);
		post.setTextAlignment(TextAlignment.JUSTIFY);
		
		this.chatflowarea.getChildren().add(post);
	}

    /**
     *
     * @param event describes the event
     */
	@Override
	public void subscribersInChatRoomChanged(ChatEvent event) {
		LOGGER.log(Level.INFO, "Updating new chatroom users");
		List<String> chatroomusers = event.getChatRoomUsers();
		
		this.selectedMembers = FXCollections.observableArrayList();

		
        memberlistview.getItems().removeAll();

        for(String user : chatroomusers){
            memberlistview.getItems().add(user);
        }
	}

    /**
     *
     * @param event describes the event
     */
	@Override
	public void joinChatRoomResponse(OnTheLineEvent event) {
		// TODO Auto-generated method stub
		LOGGER.log(Level.INFO, "Unimplemented in lobby..doesnt bother. Should never be called thoug");
	}

}

