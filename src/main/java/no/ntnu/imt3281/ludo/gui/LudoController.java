package no.ntnu.imt3281.ludo.gui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.JSONObject;

import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.application.Platform;
import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextFlow;
import no.ntnu.imt3281.chat.ChatEvent;
import no.ntnu.imt3281.ludo.client.Client;
import no.ntnu.imt3281.ludo.global.CommunicationID;
import no.ntnu.imt3281.ludo.ontheline.ChatRoomEventListener;
import no.ntnu.imt3281.ludo.ontheline.LudoResponseListener;
import no.ntnu.imt3281.ludo.ontheline.OnTheLineEvent;
import no.ntnu.imt3281.ludo.server.Config;

/**
 * Controller for the client, sets up popups, tabs, games, chats etc.
 * Implements LudoResponseListener to handle the function calls from dispatcher.
 * @author Bendik
 *
 */
public class LudoController implements LudoResponseListener {

	private static final Logger LOGGER = Logger.getLogger(LudoController.class.getName());
	//private Pop

	private Dialog<ButtonType> loginOptions;

	//Registation fields and login fields
	// For registration all fielda are used
	// For login only username and password is used
	TextField usernamefield;
	TextField emailfield;
	PasswordField passwordfield;

	//Used for the chatroom list when a Dialog opens
	Dialog<ButtonType> listchatDialog;
	//These are paired to get translation from chatroom name to chatroom id
	ObservableList<String> chatroomname;
	ArrayList<String> chatroomid;

	ListView<String> chatslistview;

	@FXML
	private TextFlow lobbyChat;

	@FXML
	private AnchorPane anchor;
	
    @FXML
    private MenuItem closeitem;

	@FXML
	private MenuItem random;

    @FXML
    private MenuItem about;

	@FXML
	private MenuItem top;

	@FXML
	private TabPane tabbedPane;
	
	/**
	 * Displays the about page in a dialog.
	 * @param event
	 */
    @FXML
    void aboutDisplay(ActionEvent event) {
    	Platform.runLater(()->{
			Dialog<ButtonType> aboutdialog = new Dialog<>();
			aboutdialog.setTitle(Config.bundle.getString("about.title"));

			ButtonType close = new ButtonType(Config.bundle.getString("about.close"),ButtonData.CANCEL_CLOSE);

			aboutdialog.setHeaderText(Config.bundle.getString("about.madeby"));
			aboutdialog.getDialogPane().setContentText("LITsec, all rights reserved™");
			aboutdialog.getDialogPane().getButtonTypes().add(close);

			final Button closeBtn = (Button) aboutdialog.getDialogPane().lookupButton(close);

			closeBtn.onActionProperty().set(actionevent -> {
				aboutdialog.close();
			});

			aboutdialog.show();
		});
    }

	/**
	 * Sends a request to the server for a random game.
	 * @param event event from javafx
	 */
	@FXML
	public void joinRandomGame(ActionEvent event) {  
		try {
			OnTheLineEvent header = new OnTheLineEvent(null,null,CommunicationID.JOINGAME,Arrays.asList("gameid",""));
			Client.getConnection().send(header.toString());
		} catch (Exception e) {
			LOGGER.log(Level.INFO,"Sending random game request failed: " + e.getMessage(), e);
		}
	}


	/**
	 * Sends a request to the server and asks if it has some available games information
	 * 
	 * @param event
	 */
	@FXML
	void listAvailableGames(ActionEvent event) {

		try {
			OnTheLineEvent sending = new OnTheLineEvent();
			sending.setid(CommunicationID.LISTAVAILABLEGAMES);
			Client.getConnection().send(sending.toString());
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.contentTextProperty().set("Failed getting list of available games");
			Platform.runLater(()->{
				alert.show();
			});
			LOGGER.log(Level.INFO,"Sending request for available games failes" + e.getMessage(), e);
		}
	}
	
    @FXML
    void closeApp(ActionEvent event) {
    	try {
			OnTheLineEvent sending = new OnTheLineEvent();
			sending.setid(CommunicationID.LOGOUT);
			Client.getConnection().send(sending.toString());
		} catch (Exception e) {
			LOGGER.log(Level.INFO, "Failed to send logout request " + e.getMessage(), e);
		}
    }
    
	/**
	 * Gonna send a request to list chats
	 * @param event
	 */
	@FXML
	void openChatRoomList(ActionEvent event) {
		OnTheLineEvent sending = new OnTheLineEvent();
		sending.setid(CommunicationID.LISTCHATS);	
		try {
			Client.getConnection().send(sending.toString());
		}catch(Exception e) {
			LOGGER.log(Level.WARNING, "Could not send  request for chatroomlist" + e.getMessage(), e);
		}

	}


	/**
	 * Sends a request to the server to create a new chatroom
	 * @param event
	 */

	@FXML
	void createNewChatroom(ActionEvent event) {
		LOGGER.log(Level.INFO, "Sending request for newchat");
		OnTheLineEvent sending = new OnTheLineEvent();
		JSONObject newchat = new JSONObject();
		newchat.put("chatroomid", ""); //An empty chatroomid will create a new one - magic right?
		sending.setid(CommunicationID.JOINCHATROOM);
		sending.setbody(newchat);
		try {
			Client.getConnection().send(sending.toString());
		}catch(Exception e) {
			LOGGER.log(Level.WARNING, "Could not send  request for chatroomlist" + e.getMessage(), e);
		}

	}

	/**
	 * Sends a request to the server and asks for top 10 statistics
	 * @param event
	 */
	@FXML
	void topDialog(ActionEvent event) {
		try {
			OnTheLineEvent sending = new OnTheLineEvent();
			sending.setid(CommunicationID.STATISTICS);
			Client.getConnection().send(sending.toString());
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.contentTextProperty().set("Failed getting Top 10 statistics");
			Platform.runLater(()->{
				alert.show();
			});
			LOGGER.log(Level.INFO,"Sending request for top 10 statistics failed" + e.getMessage(), e);
		}
	}


	/**
	 * Displays a new game in a new tab
	 * 
	 * @param id - the game id used for tab name
	 * @return the newly created game GameBoardController
	 */
	private GameBoardController displayGame(String id) {
		LOGGER.log(Level.INFO, "Opening game GUI");
		ResourceBundle bundle = ResourceBundle.getBundle("no.ntnu.imt3281.I18N.i18n");
		FXMLLoader loader = new FXMLLoader(getClass().getResource("GameBoard.fxml"));
		loader.setResources(bundle);

		try {
			AnchorPane gameBoard = loader.load();
			GameBoardController controller = (GameBoardController)loader.getController();
			Tab tab = new Tab(Config.bundle.getString("ludogameboard.game") + " " + id);
			tab.setContent(gameBoard);
			tab.setClosable(true);

			tab.setOnClosed(event->controller.closeTab());
			Platform.runLater(()->{
				this.tabbedPane.getTabs().add(tab);
			});

			return loader.getController();
		} catch (IOException e1) {
			LOGGER.log(Level.INFO,"loader.load() failed: " + e1.getMessage(), e1);
		}
		LOGGER.log(Level.INFO, "displayGame returning null");
		return null;
	}

	@FXML 
	void initialize() {
		LOGGER.log(Level.INFO, "LudoController started initialize");
	}

	/**
	 * For debugging purposes.
	 */
	public LudoController() {
		LOGGER.log(Level.INFO, "Ludocontroller started");
	}

	/**
	 * When closing a tab one should also leave the game or chat
	 * 
	 * @param event
	 */
	@SuppressWarnings("unused")
	private void closeTab(Event event) {
		LOGGER.log(Level.INFO, "Closing tab");
	}

	/**
	 * Sets up a dialog which lets you choose between login or registration.
	 * Passes on to either presentLoginDialog or presentRegistrationDialog.
	 */
	public void presentLoginOrRegistrationDialog() {
		LOGGER.log(Level.INFO, "Setting up login/register window");

		ButtonType login = new ButtonType(Config.bundle.getString("popup.login"),ButtonData.OK_DONE);
		ButtonType registration = new ButtonType(Config.bundle.getString("popup.register"),ButtonData.OK_DONE);
		ButtonType cancel = new ButtonType(Config.bundle.getString("popup.cancel"),ButtonData.CANCEL_CLOSE);
		
		if(this.loginOptions != null) {
			LOGGER.log(Level.INFO, "LoginOptions is already loaded. Closing it now");
			Platform.runLater(()->{
				this.loginOptions.close();
			});
		}
		Platform.runLater(()->{
			//For some weird reason Dialog init wants to run on fx thread
			this.loginOptions = new Dialog<>();

			this.loginOptions.setContentText(Config.bundle.getString("popup.title"));
			this.loginOptions.getDialogPane().getButtonTypes().add(login);
			this.loginOptions.getDialogPane().getButtonTypes().add(registration);
			this.loginOptions.getDialogPane().getButtonTypes().add(cancel);


			final Button loginBtn = (Button) this.loginOptions.getDialogPane().lookupButton(login);
			final Button registerBtn = (Button) this.loginOptions.getDialogPane().lookupButton(registration);
			final Button cancelBtn = (Button) this.loginOptions.getDialogPane().lookupButton(cancel);

			loginBtn.onActionProperty().set(actionevent -> this.presentLoginDialog(actionevent));

			registerBtn.onActionProperty().set(actionevent -> this.presentRegistrationDialog(actionevent));
			cancelBtn.onActionProperty().set(actionEvent->this.cancelDialog(actionEvent));
			cancelBtn.onActionProperty().set(actionevent-> this.cancelDialog(actionevent));
			this.loginOptions.show();

		});


	}

	private void presentLoginDialog(ActionEvent event) {
		LOGGER.log(Level.INFO, "Presenting login dialog");

		Dialog<ButtonType> loginDialog = new Dialog<>();
		VBox verticalbox = new VBox();
		verticalbox.setSpacing(20);

		this.usernamefield = new TextField();
		this.usernamefield.setPromptText(Config.bundle.getString("popup.username"));
		this.usernamefield.setPrefWidth(300);
		this.usernamefield.setMaxWidth(300);

		this.passwordfield = new PasswordField();
		this.passwordfield.setPromptText(Config.bundle.getString("popup.password"));
		this.passwordfield.setPrefWidth(300);
		this.passwordfield.setMaxWidth(300);

		ButtonType submit = new ButtonType(Config.bundle.getString("popup.submit"), ButtonData.APPLY);
		ButtonType resetPassword = new ButtonType(Config.bundle.getString("popup.reset"), ButtonData.HELP);
		ButtonType cancel = new ButtonType(Config.bundle.getString("popup.cancel"),ButtonData.CANCEL_CLOSE);

		loginDialog.getDialogPane().getButtonTypes().add(submit);
		loginDialog.getDialogPane().getButtonTypes().add(resetPassword);
		loginDialog.getDialogPane().getButtonTypes().add(cancel);
		verticalbox.getChildren().addAll(this.usernamefield, this.passwordfield);

		loginDialog.setContentText(Config.bundle.getString("popup.login"));
		loginDialog.getDialogPane().setExpandableContent(verticalbox);

		loginDialog.resultProperty().addListener((observer, oldvalue, newvalue)->this.loginSubmit(observer, oldvalue, newvalue));


		Platform.runLater(()->{
			loginDialog.show();
		});



	}
	private void presentRegistrationDialog(ActionEvent event) {
		LOGGER.log(Level.INFO, "Presenting registration dialog");

		Dialog<ButtonType> registerDialog = new Dialog<>();
		VBox verticalbox = new VBox();
		verticalbox.setSpacing(20);

		this.usernamefield = new TextField();
		this.usernamefield.setPromptText(Config.bundle.getString("popup.username"));
		this.usernamefield.setPrefWidth(300);
		this.usernamefield.setMaxWidth(300);

		this.emailfield = new TextField();
		this.emailfield.setPromptText(Config.bundle.getString("popup.email"));
		this.emailfield.setPrefWidth(300);
		this.emailfield.setMaxWidth(300);

		this.passwordfield = new PasswordField();
		this.passwordfield.setPromptText(Config.bundle.getString("popup.password"));
		this.passwordfield.setPrefWidth(300);
		this.passwordfield.setMaxWidth(300);

		ButtonType submit = new ButtonType(Config.bundle.getString("popup.register"), ButtonData.APPLY);
		ButtonType cancel = new ButtonType(Config.bundle.getString("popup.cancel"),ButtonData.CANCEL_CLOSE);

		registerDialog.getDialogPane().getButtonTypes().add(submit);
		registerDialog.getDialogPane().getButtonTypes().add(cancel);

		verticalbox.getChildren().addAll(this.usernamefield, this.passwordfield, this.emailfield);

		registerDialog.setContentText(Config.bundle.getString("popup.register"));

		registerDialog.getDialogPane().setExpandableContent(verticalbox);

		registerDialog.resultProperty().addListener((observer, oldvalue, newvalue) 
				-> this.registrationSubmit(observer, oldvalue, newvalue));

		registerDialog.show();
	}

	private void registrationSubmit(Observable changedValue, ButtonType oldvalue, ButtonType newvalue) {
		LOGGER.log(Level.INFO, "Registration submit is called");
		//How to get the input data
		// if buttontype is OK we know this is submit
		LOGGER.log(Level.INFO, "changedvalue is: " + changedValue.toString());

		if(newvalue.getButtonData() == ButtonData.APPLY) {
			//Submit is always apply
			String username = this.usernamefield.getText();
			String email = this.emailfield.getText();
			String password = this.passwordfield.getText();

			OnTheLineEvent sending = new OnTheLineEvent(null, null, CommunicationID.REGISTRATION, Arrays.asList("username",username,"email",email,"password",password));
			LOGGER.log(Level.INFO, "Sending request: " + sending.toString());
			try {
				Client.getConnection().send(sending.toString());
			} catch (IOException e) {
				LOGGER.log(Level.WARNING, "Could not send registration " + e.getMessage(), e);
			}
		}else if(newvalue.getButtonData() == ButtonData.CANCEL_CLOSE) {
			Platform.runLater(()->{
				//Close dialog
				Platform.exit();
			});	
		}
	}

	private void loginSubmit(Observable changedValue, ButtonType oldvalue, ButtonType newvalue) {
		LOGGER.log(Level.INFO, "Login submit is called");

		if(newvalue.getButtonData() == ButtonData.APPLY) {
			String username = this.usernamefield.getText();
			String password = this.passwordfield.getText();

			OnTheLineEvent sending = new OnTheLineEvent(null, null, CommunicationID.LOGIN, Arrays.asList("username",username,"password",password));
			try {
				Client.getConnection().send(sending.toString());
			} catch (IOException e) {
				LOGGER.log(Level.WARNING, "Could not send login: " + e.getMessage(), e);
			}
			LOGGER.log(Level.INFO, "Sending request: " + sending.toString());
		}else if(newvalue.getButtonData() == ButtonData.HELP) {
			//Reset password
			LOGGER.log(Level.WARNING, "Reset password is intentionally not implemented");
		}else if(newvalue.getButtonData() == ButtonData.CANCEL_CLOSE) {

			Platform.runLater(()->{
				Platform.exit();
			});

		}
	}

	private void cancelDialog(ActionEvent event) {
		//Dialog box canceled
		//Close this
		LOGGER.log(Level.INFO, "cancel dialog - closing application");
		Platform.exit();
	}	

	/**
	 * Used to handle the selection of a chat to join
	 * @param changedValue
	 * @param oldvalue
	 * @param newvalue
	 */
	private void joinchatbutton(Observable changedValue, ButtonType oldvalue, ButtonType newvalue) {

		if(newvalue.getButtonData() == ButtonData.OK_DONE) {
			LOGGER.log(Level.INFO, "OK button pressed");
			//Get the select list and join game
			//this.chatroomname.
			int selectedchatindex = this.chatslistview.getSelectionModel().getSelectedIndex();
			String selectedchatid = this.chatroomid.get(selectedchatindex);
			OnTheLineEvent sending = new OnTheLineEvent(null, null, CommunicationID.JOINCHATROOM, Arrays.asList("chatroomid",selectedchatid));
			try {
				Client.getConnection().send(sending.toString());
			} catch (IOException e) {
				LOGGER.log(Level.WARNING, "Could not send join chat request" + e.getMessage(), e);
			}
		}else if(newvalue.getButtonData() == ButtonData.CANCEL_CLOSE) {
			//close

			Platform.runLater(()->{
				this.listchatDialog.close();
			});
		}
	}

	/**
	 * Logger for LudoController.
	 * @param msg message to be displayed.
	 */
	public void log(String msg) {
		Platform.runLater(()-> {
			LOGGER.log(Level.INFO, "Started shutting down...");
		});
	}

	/**
	 * Attempts to connect with the token in Token.dat
	 * @return returns true or false based on the response from server.
	 * @throws IOException 
	 */
	public void attemptConnection() {

		try {
			String token = Client.getToken();
			OnTheLineEvent header = new OnTheLineEvent(null,null,CommunicationID.AUTHORIZE,Arrays.asList("token",token));
			Client.getConnection().send(header.toString());
		} catch (IOException e) {
			LOGGER.log(Level.INFO,"Fetching token failed: " + e.getMessage(), e);
			//No token results in login window
			this.presentLoginOrRegistrationDialog();
		} 
	}


	/**
	 * This will always be an error, intended use is for this to display an error message in the registration dialog. 
	 * OnTheLineEvent is needed for the function to work.
	 * OnTheLineEvent needs a particular setup which can be found in communication.md file on the wiki under id 1: Registration response.
	 */
	@Override
	public void registrationResponse(OnTheLineEvent event) {
		LOGGER.log(Level.INFO,"Registration failed - gonna set up a new login/registration window");
		this.presentLoginOrRegistrationDialog();
	}

	/**
	 * Reads the join game response from server and displays a new game in a new tab. 
	 * OnTheLineEvent is needed for the function to work.
	 * OnTheLineEvent needs a particular setup which can be found in communication.md file on the wiki under id 6: Join game response.
	 */
	@Override
	public void joinGameResponse(OnTheLineEvent event) {
		LOGGER.log(Level.INFO, "Joined game response");
		//Successfully joined a game..yeyyy
		if (event.getSuccess()) {
			try {
				String gameid = event.getGameId();
				String roomid = event.getChatRoomId();
				JSONObject body = event.getBody();

				GameBoardController gameBoardController = this.displayGame(gameid);
				if(gameBoardController != null) {
					gameBoardController.setGameId(gameid);
					gameBoardController.setChatRoomid(roomid);
					gameBoardController.registerYourself(body);
				}else {
					LOGGER.log(Level.WARNING, "Could not set gameid or chatroom id. gameboarcontroller is NULL");
				}
			} catch (Exception e) {
				LOGGER.log(Level.INFO,"Failed to join game." + e.getMessage(), e);
			}
		} else {
			LOGGER.log(Level.INFO,event.getErrorMessage());
		}

	}

	/**
	 * Logs the client out if response contains success: true. 
	 * OnTheLineEvent is needed for the function to work.
	 * OnTheLineEvent needs a particular setup which can be found in communication.md file on the wiki under id 7: Log out response.
	 */
	@Override
	public void logOutResponse(OnTheLineEvent event) {
		if (event.getSuccess()) {
			Platform.exit();
		} else {
			LOGGER.log(Level.INFO,event.getErrorMessage());
		}
	}

	/**
	 * This will always be an error, intended use is for this to display an error message in the login dialog. 
	 * OnTheLineEvent is needed for the function to work.
	 * OnTheLineEvent needs a particular setup which can be found in communication.md file on the wiki under id 8: Log in response.
	 */
	@Override
	public void logInResponse(OnTheLineEvent event) {
		LOGGER.log(Level.INFO,event.getErrorMessage());
	}

	/**
	 * Reads the error message from server and logs it. 
	 * OnTheLineEvent is needed for the function to work.
	 * OnTheLineEvent needs a particular setup which can be found in communication.md file on the wiki under id 9: error.
	 */
	@Override
	public void errResponse(OnTheLineEvent event) {
		LOGGER.log(Level.WARNING,event.getErrorMessage());
	}

	/**
	 * Reads the authorize response from the server and if successfully authorized sets a token which is stored on the client. 
	 * OnTheLineEvent is needed for the function to work.
	 * OnTheLineEvent needs a particular setup which can be found in communication.md file on the wiki under id 11: Authorize response.
	 */
	@Override
	public void authorizeResponse(OnTheLineEvent event) {
		LOGGER.log(Level.INFO, "Authorize response: " + event.toString());
		if (event.getSuccess()) {
			LOGGER.log(Level.INFO, "Registration,login or authentication successfull");
			try {
				Client.setToken(event.getToken());
			} catch (IOException e) {
				LOGGER.log(Level.INFO,"Failed to write token to file: " + e.getMessage(), e);
			} catch (Exception e) {
				LOGGER.log(Level.INFO,"Failed to fetch chat id." + e.getMessage(), e);
			}

			Client.setUsername(event.getUsername());
			try {
				Client.setLobbyChatRoomId(event.getChatRoomId());
			}catch(Exception e) {
				LOGGER.log(Level.WARNING, "No chatroomid: " + e.getMessage(), e);
			}
			LOGGER.log(Level.INFO, "I am: " + event.getUsername());

			
		} else {
			LudoController.LOGGER.log(Level.INFO, "Authentication failed. Need to setup login window");
			this.presentLoginOrRegistrationDialog();
		}
	}

	/**
	 * Sets up dialog with available games, OnTheLineEvent is needed for the function to work.
	 * OnTheLineEvent needs a particular setup which can be found in communication.md file on the wiki under id 12: List available games response.
	 * User can select a game to join and client will send a join game request to server.
	 */
	@Override
	public void availableGamesResponse(OnTheLineEvent event) {
		LOGGER.log(Level.INFO, "Setting up game list dialog");
		Platform.runLater(()->{
			Dialog<ButtonType> gamelist = new Dialog<>();
			ButtonType join = new ButtonType(Config.bundle.getString("list.join"), ButtonData.OK_DONE);
			ListView<String> availablegames = new ListView<>();

			ObservableList<String> list = FXCollections.observableArrayList(event.getGameRooms());

			availablegames.setItems(list);

			gamelist.setContentText(Config.bundle.getString("list.gamelist"));
			gamelist.getDialogPane().setExpandableContent(availablegames);
			gamelist.getDialogPane().getButtonTypes().add(join);

			final Button joinBtn = (Button) gamelist.getDialogPane().lookupButton(join);

			joinBtn.onActionProperty().set(actionevent -> {
				OnTheLineEvent header = new OnTheLineEvent(null,null,CommunicationID.JOINGAME,Arrays.asList("gameid",availablegames.getSelectionModel().getSelectedItem()));
				try {
					Client.getConnection().send(header.toString());
				} catch (IOException e) {
					LOGGER.log(Level.WARNING, "Failed to send packet in availableGamesResponse: " + e.getMessage(), e);
				}
			});

			gamelist.show();
		});
	}

	/**
	 * Sets up dialog with available chats, OnTheLineEvent is needed for the function to work.
	 * OnTheLineEvent needs a particular setup which can be found in communication.md file on the wiki under id 13: list all chats response.
	 * User can select a chat to join and client will send a join chat request to server.
	 */
	@Override
	public void availableChatResponse(OnTheLineEvent event) {
		LOGGER.log(Level.INFO, "Available chat reponse received" + event.toString());
		Platform.runLater(()->{
			this.listchatDialog = new Dialog<>();
			this.chatslistview = new ListView<>();
			this.chatroomname = FXCollections.observableArrayList();
			this.chatroomid = new ArrayList<>();

			ChatEvent chatevent = new ChatEvent(event.getBody());

			JSONArray chatrooms = chatevent.getEvent().getJSONArray("chatrooms");


			for (Object chatroom : chatrooms) {
				if(chatroom instanceof JSONObject) {
					String roomid = ((JSONObject) chatroom).getString("chatroomid");
					String name = ((JSONObject) chatroom).getString("chatroomname");
					chatroomname.add(name);
					this.chatroomid.add(roomid);
				}
			}

			this.chatslistview.setItems(chatroomname);

			ButtonType join = new ButtonType(Config.bundle.getString("chat.joingame"),ButtonData.OK_DONE);
			ButtonType cancel = new ButtonType(Config.bundle.getString("cancel"),ButtonData.CANCEL_CLOSE);

			this.listchatDialog.getDialogPane().setContent(this.chatslistview);
			this.listchatDialog.getDialogPane().getButtonTypes().addAll(join,cancel);

			this.listchatDialog.resultProperty().addListener((observer, oldvalue, newvalue)->this.joinchatbutton(observer, oldvalue, newvalue));

			this.listchatDialog.show();
		});
	}

	/**
	 * Sets up a new chat in a new tab, OnTheLineEvent is needed for the function to work.
	 * OnTheLineEvent needs a particular setup which can be found in communication.md file on the wiki under id 14: join chat response.
	 */
	@Override
	public void joinChatResponse(OnTheLineEvent event) {
		LOGGER.log(Level.INFO, "Join chat response");
		try {
			ResourceBundle bundle = ResourceBundle.getBundle("no.ntnu.imt3281.I18N.i18n");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("Lobby.fxml"));
			loader.setResources(bundle);

			AnchorPane chatroomNode = (AnchorPane) loader.load();
			LobbyRoomController newOffgameChat = loader.getController();
			String roomid = event.getChatRoomId();
			String roomname = event.getBody().getString("chatroomname");

			newOffgameChat.setChatRoomId(roomid);
			Tab tab = new Tab(roomname);
			tab.setContent(chatroomNode);
			tab.setClosable(true);

			tab.setOnCloseRequest(closerequest -> newOffgameChat.closeTab(closerequest));
			Platform.runLater(()->{
				this.tabbedPane.getTabs().add(tab);	
			});


			Client.addChatRoomEventListener((ChatRoomEventListener)newOffgameChat, roomid);

		} catch (Exception e) {
			LOGGER.log(Level.INFO,"Failed to fetch chat id or chatroomname.", e);
		}
	}

	/**
	 * Sets up a invite dialog with the name of the inviter, OnTheLineEvent is needed for the function to work.
	 * OnTheLineEvent needs a particular setup which can be found in communication.md file on the wiki under id 16: invite response.
	 */
	@Override
	public void inviteResponse(OnTheLineEvent event) {
		LOGGER.log(Level.INFO, "Setting up invite dialog");
		Platform.runLater(()->{
			Dialog<ButtonType> inviteDialog = new Dialog<>();

			ButtonType accept = new ButtonType(Config.bundle.getString("invite.accept"),ButtonData.OK_DONE);
			ButtonType decline = new ButtonType(Config.bundle.getString("invite.decline"), ButtonData.CANCEL_CLOSE);

			inviteDialog.setContentText(Config.bundle.getString("invite.invite") + event.getInviter());
			inviteDialog.getDialogPane().getButtonTypes().addAll(accept, decline);

			final Button acceptBtn = (Button) inviteDialog.getDialogPane().lookupButton(accept);
			final Button declineBtn = (Button) inviteDialog.getDialogPane().lookupButton(decline);

			acceptBtn.onActionProperty().set(actionevent -> {
				String gameid = null;
				try {
					gameid = event.getGameId();
				} catch (Exception e1) {
					LOGGER.log(Level.WARNING, "Fetching gameid failed in inviteResponse: " + e1.getMessage(), e1);
				}
				OnTheLineEvent header = new OnTheLineEvent(null,null,CommunicationID.JOINGAME,Arrays.asList("gameid", gameid));
				try {
					Client.getConnection().send(header.toString());
				} catch (IOException e) {
					LOGGER.log(Level.INFO,"Sending listGames requests failed: " + e.getMessage(), e);
				}
			});
			declineBtn.onActionProperty().set(actionevent -> {
				inviteDialog.close();
			});
			inviteDialog.show();
		});

	}

	/**
	 * Sets up a statistics dialog by using a OnTheLineEvent.
	 * OnTheLineEvent needs two JSONArrays in the body:
	 * numberofgames and numberofwins, the contents of these can be found in the communication.md file on the wiki.
	 */
	@Override
	public void statisticsResponse(OnTheLineEvent event) {
		LOGGER.log(Level.INFO, "Setting up invite dialog");
		Platform.runLater(()->{
			Dialog<ButtonType> statisticsDialog = new Dialog<>();
			statisticsDialog.setTitle(Config.bundle.getString("statistics.title"));

			ButtonType close = new ButtonType(Config.bundle.getString("statistics.close"),ButtonData.CANCEL_CLOSE);
			
			
			String toplist = "";
			toplist += "Top 10 games won:\n\n";
			toplist += event.getTopWins();
			toplist += "\n\nTop 10 games played:\n\n";
			toplist += event.getTopGames();

			statisticsDialog.getDialogPane().setContentText(toplist);
			statisticsDialog.getDialogPane().getButtonTypes().add(close);

			final Button closeBtn = (Button) statisticsDialog.getDialogPane().lookupButton(close);

			closeBtn.onActionProperty().set(actionevent -> {
				statisticsDialog.close();
			});

			statisticsDialog.show();
		});
	}


}
