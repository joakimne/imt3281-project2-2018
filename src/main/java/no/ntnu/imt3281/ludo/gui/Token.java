package no.ntnu.imt3281.ludo.gui;

import java.awt.Color;
import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.CornerRadii;
import no.ntnu.imt3281.ludo.logic.Coordinate;
import no.ntnu.imt3281.ludo.logic.LudoCoordinateToBlockLookup;
import no.ntnu.imt3281.ludo.logic.Player;
import no.ntnu.imt3281.ludo.logic.TokenEnum;
import no.ntnu.imt3281.ludo.logic.TokenPosition;
import no.ntnu.imt3281.ludo.logic.Yard;

/**
 * A subclass of the button to make a token for used in the game
 * The button has an image for background to make it look like a token
 * 
 * The button handles press events on its own
 * @author joakimellestad
 *
 *
 *
 *
 *
 *Exmaple usage:
 * 	 private Token greenTop;
 *   private Token greenLeft;
 *   private Token greenRight;
 *   private Token greenBottom;
 *
 *
 *try {
 *   		this.greenTop = new Token(Player.GREEN, TokenPosition.TOP, "Bubba Gump", TokenEnum.CANNONNFILLED);
 *   	}catch(Exception e) {
 *   		System.out.println("Creating token failed: " + e.getMessage());
 *   	}
 *   	this.ludoContainer.getChildren().add(this.greenTop);
 */

public class Token extends Button{
    private static final Logger LOGGER = Logger.getLogger(Token.class.getName());

	ImageView tokenImage;
	Player player;
	TokenPosition tokpos;
	Double tokensize = (double) 48;

	/**
	 * A token is bound to a player: green, red, blue, yellow
	 * It also is bound to a specific position. You can call that
	 * top, left, right, bottom.
	 * By knowing its position we can position it from this class
	 * 
	 * This throws because this can't properly handle an exception...what should it do?
	 * @param player the player
	 * @param tokPos the token position
	 * @param playerNick not in use
	 * @param tok which token type (image)
	 */
	public Token(Player player, TokenPosition tokPos, String playerNick, TokenEnum tok) {
		super();
		this.player = player;
		this.tokpos = tokPos;
		
		this.setMinSize(tokensize, tokensize);
		this.setPrefSize(tokensize, tokensize);
		this.setMaxSize(tokensize, tokensize);
		
		/// Making it look pretty
		try {
		BackgroundSize backgroundSize = new BackgroundSize(100, 100, true, true, true, true);

		BackgroundImage backimage = new BackgroundImage(AvailableTokens.getImage(tok),BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, backgroundSize);
		Background back = new Background(backimage);
		this.setBackground(back);
		}catch(FileNotFoundException e) {
			LOGGER.log(Level.INFO, e.getMessage(), e);
		}
		
		// Fetch correct position for player in yard
		try {
		Yard yard = (Yard) LudoCoordinateToBlockLookup.getCoordinatesFromBlock(player, 0);
		this.relocate(yard.getYardPosition(tokPos).centerX(), yard.getYardPosition(tokPos).centerY());
		}catch(Exception e) {
			LOGGER.log(Level.WARNING, e.getMessage(), e);
		}
	}

	/**
	 * getter for player
	 * @return the player
	 */
	public Player getPlayer() {
		return this.player;
	}

	/**
	 * Sets the player nickname, and token type, not in use
	 * @param playerNick the nickname
	 * @param tok the token type
	 */
	public void setPlayerAndImage(String playerNick, TokenEnum tok) {
		this.setText(playerNick);
		try {
			this.tokenImage = new ImageView(AvailableTokens.getImage(tok));
		}catch(FileNotFoundException fnfe) {
			LOGGER.log(Level.WARNING, "File not found", fnfe);
		}
		
		if(this.tokenImage != null) {
			this.tokenImage.resize(25, 25);
			this.setGraphic(this.tokenImage);
			this.setGraphicTextGap(2.0);
			
		}
	}
	
	/**
	 * Moves a piece in the GUI
	 * @param playerconst the player which owns the piece
	 * @param endPos the piece which should be moved (0-3)
	 */
	public void moveToken(int playerconst, int endPos) {
		LOGGER.log(Level.INFO, "Moving piece for player:" + player);
		
		Player moveplayer = Player.intToPlayer(playerconst);
		try {
			if(endPos == 0) {
				//hahaha. Going back to the yard
				Yard yard = (Yard) LudoCoordinateToBlockLookup.getCoordinatesFromBlock(moveplayer, 0);
				this.relocate(yard.getYardPosition(this.tokpos).centerX(), yard.getYardPosition(this.tokpos).centerY());
				
			}else {
				
				Coordinate coordinate = LudoCoordinateToBlockLookup.getCoordinatesFromBlock(moveplayer, endPos);
				this.relocate(coordinate.getMinX(), coordinate.getMinY());
			}
			
		} catch (Exception e) {
			LOGGER.log(Level.WARNING, "Could not get coordinates from block: " + e.getMessage(), e);
		}
	}
}