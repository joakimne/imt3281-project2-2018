package no.ntnu.imt3281.ludo.client;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import no.ntnu.imt3281.chat.ChatEvent;
import no.ntnu.imt3281.ludo.global.CommunicationID;
import no.ntnu.imt3281.ludo.gui.LudoController;
import no.ntnu.imt3281.ludo.logic.Ludo;
import no.ntnu.imt3281.ludo.logic.dice.DiceListener;
import no.ntnu.imt3281.ludo.logic.piece.PieceListener;
import no.ntnu.imt3281.ludo.logic.player.PlayerEvent;
import no.ntnu.imt3281.ludo.logic.player.PlayerListener;
import no.ntnu.imt3281.ludo.ontheline.ChatRoomEventListener;
import no.ntnu.imt3281.ludo.ontheline.LudoResponseListener;
import no.ntnu.imt3281.ludo.ontheline.OnTheLineEvent;

import java.io.*;
import java.util.Iterator;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;



/**
 * This is the main class for the client.
 * **Note, change this to extend other classes if desired.**
 *
 * @author
 */
public class Client extends Application {

	private static final Logger LOGGER = Logger.getLogger(Client.class.getName());

	private static boolean shutdown = false;
	
	/**
	 * Object to keep the connection to the server. All data is sent through connection.
	 */
	private static ServerConnection connection;
	
	public static ServerConnection getConnection() throws IOException{
		if(connection != null) {
			return Client.connection;
		}
		LOGGER.log(Level.INFO, "No server connection...");
		throw new IOException("Could not get serverconnection");
	}

	/**
	 * A LudoController object to be able to reference the LudoController.
	 */
	private static LudoController ludoController;
	public LudoController getLudoController() throws Exception{
		if(Client.ludoController != null) {
			return Client.ludoController;
		}
		LOGGER.log(Level.INFO, "No ludocontroller exists yet");
		throw new Exception("No ludocontroller exists yet");
	}
	
	/**
	 * Listener for Ludo responses. Contains interface functions for all the relevant communication IDs
	 */
	private static LudoResponseListener ludoResponseEventListener;

	private static LinkedBlockingQueue<OnTheLineEvent> incoming = new LinkedBlockingQueue<>();

	/**
	 * hashmap with the different individual Ludo games, they can be fetched for editing through getLudoGame(gameid).
	 * Key: gameid
	 */
	private static ConcurrentHashMap<String, Ludo> ludoGames = new ConcurrentHashMap<>();
	
	/**
	 * The hash map holds the ChatRoom event listeners
	 * Those listeners originate from
	 * GameBoardController
	 * LobbyRoomController
	 * Anyone can implements some chat - just register yourself, before you wreck yourself ;)
	 * The key is the chatroom id
	 * 
	 */
	private static ConcurrentHashMap<String, ChatRoomEventListener> chatroomeventlisteners = new ConcurrentHashMap<>();
	
	/**
	 * Thread which runs in the background.
	 */

	private static String token;
	/**
	 * After a authorize response the client will receive whatever the id of the lobby chatroom is.
	 * Set this here so LobbyRoomController.java can read it and subscribe to it.
	 */
	private static String chatRoomLobbyId;
	private static String username;

	/**
	 * Sets up the loader screen that might require the user to login
	 */
	@Override
	public void start(Stage primaryStage) {


		try {
			ResourceBundle bundle = ResourceBundle.getBundle("no.ntnu.imt3281.I18N.i18n");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("../gui/Ludo.fxml"));
			loader.setResources(bundle);
			AnchorPane root = loader.load();
			Scene scene = new Scene(root);
			primaryStage.getIcons().add(new Image("images/ludo-board.png"));
			primaryStage.setScene(scene);
			primaryStage.setTitle("Client view for Ludo application");
			primaryStage.show();

			Client.ludoController = loader.getController();
			Client.ludoController.attemptConnection();
			Client.ludoControllerRegisterForEvents(Client.ludoController);
			
		} catch (Exception e) {
			LOGGER.log(Level.WARNING, "Could not load Ludo game: " + e.getMessage(), e);
		}
	}

	/**
	 * Starts a new thread that reads all incoming requests to the client.
	 *
	 * @param args args to potentially start not used
	 */
	public static void main(String[] args) {
		try {
			Client.connection = new ServerConnection();
			
			new Thread(Client::socketReader).start();
			new Thread(Client::dispatcher).start();
			new Thread(Client::sendMessageThread).start();
			launch(args); // Does not return until finished.
	
		} catch (IOException e) {
			LOGGER.log(Level.WARNING, "Could not make connection to server" + e.getMessage(), e);
			LOGGER.log(Level.INFO, "Will not open client if server is not available.");
		}

		System.exit(0);    // Shut down application when GUI closes.
	}

	/**
	 * THREAD:
	 *
	 * Send message
	 */
	private static void sendMessageThread() {
		while (!shutdown){
			try {
				Client.connection.write();
			} catch (InterruptedException | IOException e) {
				LOGGER.log(Level.WARNING, "Could not send outgoing OnTheLine" + e.getMessage(), e);
			}
			//sleep zzzzzzzzzzz
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				LOGGER.log(Level.INFO, "Could not make the thread sleep: " + e .getMessage(), e);
			}
		}
	}

	/**
	 * Sets the shutdown variable to true, dispatcher and the connection is then stopped.
	 */
	public static void stopClient() {
		shutdown = true;
	}


	/**
	 * Attempts to read input from a BufferedReader coming in from the server connection
	 * If IoException failes it will attempt to reconnect. If the server i unavailable then we dont know
	 * what we should do. Maybe a reconnect in about 10 seconds or close the application.
	 * The read content is used to create a OnTheLine object and is plased in a
	 * LinkedBlockingQueu so the worker thread can start doing some work on in. 
	 */
	private static void socketReader() {
		while (!shutdown) {
			try {

				String received = Client.connection.read();
				LOGGER.log(Level.INFO, "Recieved data: {0}", received);

				OnTheLineEvent onthelinevent = new OnTheLineEvent(received);
					Client.incoming.put(onthelinevent);
			} catch (IOException e) {
				LOGGER.log(Level.INFO, "Socketreader ioerror - will attempt to reconnect" + e.getMessage(), e);
				try {
					Client.connection = new ServerConnection();
				} catch (IOException e1) {
					LOGGER.log(Level.INFO, "Socketreader ioerror - reconnect failed..." + e.getMessage(), e1);
				}
			}catch(InterruptedException | NullPointerException e) {
                LOGGER.log(Level.WARNING, e.getMessage(), e);
            }
        }
	}
		/**
		 * dispatcher
		 * noun
		 * > a person whose job is to receive messages and organize the movement of people or vehicles,
		 * > especially in the emergency services.
		 * <p>
		 * dispatcher should receive all socket communications and organize them out to the handlers of the messages.
		 */
		private static void dispatcher() {
			LOGGER.log(Level.INFO, "Dispatcher started");
			while(!shutdown) {

				OnTheLineEvent onthelinevent;
				try {
					onthelinevent = Client.incoming.take();
					String id = onthelinevent.getId();

					switch (id) {
					case CommunicationID.REGISTRATION:
						LOGGER.log(Level.INFO, "Request id 1: Registration");
						Client.ludoController.registrationResponse(onthelinevent);
						break;
					case CommunicationID.RTD:
						LOGGER.log(Level.INFO, "Request id 2: RTD");
						Client.rollTheDice(onthelinevent);
						break;
					case CommunicationID.MOVE:
						LOGGER.log(Level.INFO, "Request id 3: Move");
						Client.moveToken(onthelinevent);
						break;
					case CommunicationID.CHATMESSAGE:
						LOGGER.log(Level.INFO, "Request id 4: Chat message");
						Client.newChatMessage(onthelinevent);
						break;
					case CommunicationID.STARTAGAME:
						LOGGER.log(Level.INFO, "Request id 5: Start a game");
						Client.ludoResponseEventListener.joinGameResponse(onthelinevent);
						break;
					case CommunicationID.JOINGAME:
						LOGGER.log(Level.INFO, "Request id 6: Join game");
						Client.ludoResponseEventListener.joinGameResponse(onthelinevent);
						break;
					case CommunicationID.LOGOUT:
						LOGGER.log(Level.INFO, "Request id 7: Log out");
						Client.ludoResponseEventListener.logOutResponse(onthelinevent);
						break;
					case CommunicationID.LOGIN:
						LOGGER.log(Level.INFO, "Request id 8: Log in");
						Client.ludoResponseEventListener.logInResponse(onthelinevent);
						break;
					case CommunicationID.ERROR:
						LOGGER.log(Level.INFO, "Request id 9: Error");
						Client.ludoResponseEventListener.errResponse(onthelinevent);
						break;
					case CommunicationID.PLAYERSTATE:
						LOGGER.log(Level.INFO, "Request id 10: Player state changed");
						Client.playerStateChanged(onthelinevent);
						break;
					case CommunicationID.AUTHORIZE:
						LOGGER.log(Level.INFO, "Request id 11: Authorize");
						
						Client.ludoResponseEventListener.authorizeResponse(onthelinevent);

						
						break;
					case CommunicationID.LISTAVAILABLEGAMES:
						LOGGER.log(Level.INFO, "Request id 12: Available games");
						//Finne den ene loby controlleren
						Platform.runLater(()-> Client.ludoResponseEventListener.availableGamesResponse(onthelinevent));
						
						break;
					case CommunicationID.LISTCHATS:
						LOGGER.log(Level.INFO, "Request id 13: Available chats");
						//Finne den ene loby controlleren
						Platform.runLater(()-> Client.ludoResponseEventListener.availableChatResponse(onthelinevent));
						
						break;
					case CommunicationID.JOINCHATROOM:
						LOGGER.log(Level.INFO, "Request id 14: Join chat");
						Platform.runLater(()-> Client.ludoResponseEventListener.joinChatResponse(onthelinevent));
						
						break;
					case CommunicationID.CHATROOMUSERS:
						LOGGER.log(Level.INFO, "Request id 15: Users in chat changed/update");
						Client.listUsersInchatRoomResponse(onthelinevent);
						break;
					case CommunicationID.INVITE:
						LOGGER.log(Level.INFO, "Request id 16: Invite received.");
						Client.ludoResponseEventListener.inviteResponse(onthelinevent);
						break;
					case CommunicationID.STATISTICS:
						LOGGER.log(Level.INFO, "Request id 17: Statistics");
						Client.ludoResponseEventListener.statisticsResponse(onthelinevent);
						break;
					default:
						LOGGER.log(Level.WARNING, "Unknown id in request");
					}
				} catch (InterruptedException e) {
					LOGGER.log(Level.WARNING, "Handler failed reading incoming" + e.getMessage(), e);
				}
			}
		}


		private static void playerStateChanged(OnTheLineEvent onthelinevent) {
			JSONObject body = onthelinevent.getBody();
			int state = Integer.parseInt(body.getString("state"));

			try {
				Ludo ludogame = Client.getLudoGame(onthelinevent.getGameId());

				switch (state) {
				case PlayerEvent.JOINED:
					LOGGER.log(Level.INFO, "A player just joined");
					ludogame.addPlayer(body.getString("username"));
					break;
				case PlayerEvent.LEFTGAME:
					LOGGER.log(Level.FINEST, "A player just left the game");
					ludogame.removePlayer(body.getString("username"));
					break;
				default:
					break;
				}
			} catch (Exception e) {
				LOGGER.log(Level.WARNING, "Could not find ludogame here: " + e.getMessage(), e);
			}
		}
		
		/**
		 * Getter for username.
		 * @return
		 */
		public static String getUsername() {
			return Client.username;
		}
		
		/**
		 * Setter for username
		 * @param username String which username should be set to.
		 */
		public static void setUsername(String username) {
			Client.username = username;
		}
		
		/**
		 * Getter for chatRoomLobbyId
		 * @return
		 */
		public static String getLobbyChatRoomId() {
			return Client.chatRoomLobbyId;
		}
		
		/**
		 * Setter for chatRoomLobbyId.
		 * @param lobbychatroomid String with the lobby id.
		 */
		public static void setLobbyChatRoomId(String lobbychatroomid) {
			Client.chatRoomLobbyId = lobbychatroomid;
		}
		
		/**
		 * Fetches the Ludo object with a certain gameid. Used when you want to change variables etc. in the Ludo object.
		 * @param gameid The game which you want to make changes to.
		 * @return	The relevant Ludo game.
		 * @throws Exception If the game cannot be found.
		 */
		public static Ludo getLudoGame(String gameid) throws Exception{
			Ludo ludo = Client.ludoGames.get(gameid);
			if(ludo != null) {
				return ludo;
			}
			throw new Exception("Could not find ludogame");
		}

		/**
		 * The LudoController should already have made a GameController -
		 * A newGame response should have be received from the server
		 *  
		 * 1. Register the GameBoardController with: diceListener, pieceListener and PlayerStateChanged
		 * 
		 * Also make the Ludo game and put that in the ludogames(-hashmap) for safe storage
		 * 
		 * Since a new game also will have a chat the GameBoardController must implement ChatRoomEventListener
		 *
		 * 
		 * @param gameid - the gameid for the newly created game
		 * @param chatroomid the identifier for the chatroom
		 * @param body the body of the request containing existing users, colors, and usernames
		 * @param chatroomeventlistener - the controller to receive chatroomevents
		 * @param piecelistener - the controller to receive pieceevent
		 * @param dicelistener - the controller to receive dicevent
		 * @param player - the controller to receive playerevents
		 */
		public static void addNewLudoGame(String gameid, String chatroomid, ChatRoomEventListener chatroomeventlistener, PieceListener piecelistener, DiceListener dicelistener, PlayerListener player, JSONObject body) {

			Ludo newLudo = new Ludo();
			newLudo.addPlayerListener(player);
			newLudo.addDiceListener(dicelistener);
			newLudo.addPieceListener(piecelistener);

			//Check for existing users in response
			try{
				JSONArray existingusers = body.getJSONArray("existingusers");

                for (Object existinguser : existingusers) {
                    JSONObject user = (JSONObject) existinguser;

                    String username = user.getString("username");
                    int color = user.getInt("color");

                    newLudo.addPlayer(username, color);
                }
	
			}catch(JSONException e) {
				LOGGER.log(Level.INFO, "Game doesnt have existing players - that is okay" + e.getMessage(), e);
			}
			newLudo.addPlayer(Client.getUsername());
			
			Client.ludoGames.put(gameid, newLudo);
			Client.addChatRoomEventListener(chatroomeventlistener, chatroomid);
		}

		/**
		 * Adds chatroomlistener
		 * @param chatroomeventlistener - the controller that want chatevents
		 * @param chatroomid - the chatroomid for which this should belong to
		 */
		public static void addChatRoomEventListener(ChatRoomEventListener chatroomeventlistener, String chatroomid) {
			Client.chatroomeventlisteners.put(chatroomid, chatroomeventlistener);
		}

	/**
	 * rolls the dice locally
	 * @param event contains data about the event, gameid, and dice result
	 */
		private static void rollTheDice(OnTheLineEvent event) {

			try {
				String gameid = event.getGameId();
				Ludo ludogame = Client.getLudoGame(gameid);
				int dice = event.getBody().getInt("diceresult");
				Platform.runLater(()-> ludogame.throwDice(dice));
				

			}catch(Exception e) {
				//Could not get game id
				LOGGER.log(Level.WARNING, "Could not get diceresult" + e.getMessage(), e);
			}
		}

	/**
	 * moves a token in the game
	 * @param event event contains data about the moving, which player, startposition and endposition
	 */
	private static void moveToken(OnTheLineEvent event) {
			try {
				String gameid = event.getGameId();
				Ludo ludogame = Client.getLudoGame(gameid);

				int player = event.getBody().getInt("player");
				int start = event.getBody().getInt("startposition");
				int end = event.getBody().getInt("endposition");
				
				Platform.runLater(()-> ludogame.movePiece(player, start, end));
				

			}catch(Exception e) {
				//Could not get game id
				LOGGER.log(Level.WARNING, "Could not get move data" + e.getMessage(), e);
			}
		}

	/**
	 * Adds new message
	 * @param event containing data about the message, chatroomid, message, sender etc
	 */
		private static void newChatMessage(OnTheLineEvent event) {
			try {
				
				JSONObject body = event.getBody();
				ChatEvent chatevent = new ChatEvent(body);
				
				ChatRoomEventListener chatroom = Client.chatroomeventlisteners.get(chatevent.getChatroomId());
				if(chatroom != null){
					Platform.runLater(()-> chatroom.newChatMessageEvent(chatevent));
						
				}else {
					LOGGER.log(Level.WARNING, "Received a chatroom that I have no knowledge of");
				 } 		
			}catch(Exception e) {
				LOGGER.log(Level.WARNING, "chatroom could not be found: " + e.getMessage(), e);
			}
		}
		
		/**
		 * Called by the dispatcher when the response is
		 * 15 - List users in chatroom.
		 * Takes the chatroomid and adds a new listener, then updates GUI with member list.
		 * @param event event containing users and chatroomid
		 */
		private static void listUsersInchatRoomResponse(OnTheLineEvent event) {
			JSONObject body = event.getBody();
			ChatEvent chatevent = new ChatEvent(body);
			String  chatroomid = chatevent.getChatroomId();
			
			ChatRoomEventListener chatroomeventlistener = Client.chatroomeventlisteners.get(chatroomid);
			if(chatroomeventlistener != null) {
			Platform.runLater(()-> chatroomeventlistener.subscribersInChatRoomChanged(chatevent));
			}else {
				LOGGER.log(Level.WARNING, "(ListUsersInChatRoomResponse) --> That chatroomid has no power here: {0}" + chatroomid);
			}
		}


		/**
		 * The ludocontroller have registered for events
		 * This calles the event so the controller get fetch its own data
		 * @param listener sets the ludoresponselistener
		 */
		private static void ludoControllerRegisterForEvents(LudoResponseListener listener) {
			Client.ludoResponseEventListener = listener;
		}


		/**
		 * Attempts to write the token to file
		 * The token can then be used later when authorize to server
		 * If the token can not be written this throws a IOException
		 * @param token user token
		 * @throws IOException it it couldnt set token
		 */
		public static void setToken(String token) throws IOException {

			try(BufferedWriter writer = new BufferedWriter(new FileWriter("Token.dat"))){
				writer.write(token);
				writer.newLine();
				Client.token = token;
			}catch(IOException e) {
			    LOGGER.log(Level.WARNING, "Can not set token {0}" + e.getMessage(), e);
				throw e;
			}

		}

		/**
		 * Attempts to read a token from file
		 * if the file does not exists a IOException will be throws
		 * if the file does not contain a token this returns null
		 * @throws IOException if it couldnt get token
		 */
		public static String getToken() throws IOException{
			if(Client.token != null) {
				//Token is already loaded
				return Client.token;
			}
			try(BufferedReader reader = new BufferedReader(new FileReader("Token.dat"))){
				String token = reader.readLine();
				Client.token = token;
				return token;
			}catch(IOException e) {
                LOGGER.log(Level.WARNING, "Can not get token {0}" + e.getMessage(), e);
				throw e;
			}
		}



	}
