package no.ntnu.imt3281.ludo.client;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import no.ntnu.imt3281.ludo.server.Config;

/**
 * Class to take care of the socket object
 */
public class ServerConnection {
    private BufferedWriter output;
    private BufferedReader input;
    private Socket socket;

    private static final Logger LOGGER = Logger.getLogger(ServerConnection.class.getName());
    private LinkedBlockingQueue<String> messages = new LinkedBlockingQueue<>();

    /**
     * initialize the socket and the bufferedWriter
     * @throws IOException Could not open buffered writer or buffered reader.
     */
    public ServerConnection() throws IOException {
        socket = new Socket(Config.SERVERNAME, Config.SERVERPORT);
        output = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    }

    /**
     * Sends a message on the bufferedWriter
     * @param text message to be sent
     * @throws IOException Could not write to bufferedwriter.
     */
    public void send(String text) throws IOException {
    	LOGGER.log(Level.INFO, "Sending data: " + text);
        try {
            messages.put(text);

        } catch (InterruptedException e) {
            LOGGER.log(Level.WARNING, "Could not add message to queue: " + e.getMessage());
        }
    }
    
    /**
     * Reads from the connection.
     * @return Returns a String with JSON format.
     * @throws IOException Could not read from Buffered reader
     */
    public String read() throws IOException{
    	LOGGER.log(Level.INFO, "Reading from bufferedreader");
    	return this.input.readLine();
    }
    
    /**
     * Should be called periodically to make the client write messages on the line/to the srver
     * @throws IOException Could not write to file.
     * @throws InterruptedException Writing to file interrupted.
     */
    public void write() throws IOException, InterruptedException{
    	String message = this.getMessage();
    	this.output.write(message);
    	this.output.newLine();
    	this.output.flush();
    }
    
    /**
     * Gets a message from the getMessage queue. Used to send a message to server.
     * @return	String in JSON format.
     * @throws InterruptedException
     */
    public String getMessage() throws InterruptedException {
        return messages.take();
    }
}
