package no.ntnu.imt3281.ludo.server;

import org.json.JSONArray;
import org.json.JSONObject;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This is the main place to go for database queries.
 *
 * @author joakimellestad
 */
public final class Database {

	private static final Logger LOGGER = Logger.getLogger(Database.class.getName());
	private static Connection connection;

	/**
	 * Private initializer so others cant make another instance of this.
	 */
	private Database() {
	}



	/**
	 * Creates the database if not yet created
	 * This should be run when the server starts - multiple attempts at creating db doesnt matter
	 */
	public static void createDB() {
		try {
			Database.connection = DriverManager.getConnection("jdbc:derby:LudoDB;create=true");	
			LOGGER.info("Database created");
		}catch(SQLException sqlexception) {
			LOGGER.log(Level.WARNING,"Create database failed: " + sqlexception.getMessage(), sqlexception);
		}
	}

	/**
	 * Get the connection  for the database
	 * If not exists the connection will be made
	 * @return Connection - the connection to the database
	 */
	private static Connection getConnection(){
		try {
			if(!Database.connection.isValid(0)) {
				Database.connection = DriverManager.getConnection("jdbc:derby:LudoDB;");
			}
		}catch(SQLException e) {
			LOGGER.log(Level.WARNING, "Failed getting database connection",e);
		}
		return Database.connection;
	}
	/**
	 * When testing we want a fresh db
	 * This cannot be done unfortunately
	 */
	public static void deleteDB() {
		LOGGER.log(Level.INFO, "Sorry, you must manually delete LudoDB and Derby.log");
	}

	/**
	 * Creates the schema for the db if not created yet
	 * We want the shcema on file - beacuse having it in memory is not necessary. Though it would make this very easy...22:23PM
	 */
	public static void createSchema() {

		try (Statement stmnt = Database.getConnection().createStatement()) {

			/**
			 * Create user table
			 * Read from file -> make string -> execute
			 */
			InputStream usertable = Database.class.getResourceAsStream("/resources/UserTable.sql");
			BufferedReader userTableBufferedReader = new BufferedReader(new InputStreamReader(usertable));
			String userTableString = "";
			try {
				StringBuilder userTableSB = new StringBuilder();
				String usertableLine = userTableBufferedReader.readLine();

				while (usertableLine != null) {
					userTableSB.append(usertableLine);
					userTableSB.append(System.lineSeparator());
					usertableLine = userTableBufferedReader.readLine();
				}

				userTableString = userTableSB.toString();
			} catch (IOException ioexception) {
				LOGGER.log(Level.WARNING, "IOException occured for UserTable.sql file:", ioexception);
			} finally {
				userTableBufferedReader.close();
			}

			/**
			 * Create userstatistic table
			 * Read from file -> make string -> execute
			 */
			InputStream userStatisticTable = Database.class.getResourceAsStream("/resources/UserStatisticTable.sql");
			BufferedReader userStatisticTableBufferedReader = new BufferedReader(new InputStreamReader(userStatisticTable));
			String userStatisticTableString = "";
			try {
				StringBuilder userStatisticTableSB = new StringBuilder();
				String userStatisticTableLine = userStatisticTableBufferedReader.readLine();

				while (userStatisticTableLine != null) {
					userStatisticTableSB.append(userStatisticTableLine);
					userStatisticTableSB.append(System.lineSeparator());
					userStatisticTableLine = userStatisticTableBufferedReader.readLine();
				}

				userStatisticTableString = userStatisticTableSB.toString();

			} catch (IOException ioe) {
				LOGGER.log(Level.WARNING, "IOException occured for UserStatisticTable.sql file: " + ioe.getMessage(), ioe);
			} finally {
				userStatisticTableBufferedReader.close();
			}

			stmnt.execute(userTableString);
			stmnt.execute(userStatisticTableString);
			stmnt.close();
			LOGGER.info("Database schema created");
		} catch (SQLException sqlexception) {
			LOGGER.log(Level.WARNING,"Create table execution failed: " + sqlexception.getMessage(), sqlexception);
		} catch (Exception e) {
			LOGGER.log(Level.WARNING, "Creates chema failed: " + e.getMessage(), e);
		}
	}


	/**
	 * Adds a new user to the database.
	 * Assumes all values are correct and will not crash insertion
	 *
	 * @param username the username
	 * @param hashedPassword the hashed password
	 * @param email the email of the user
	 * @param salt the salt for the hashing
	 * @return int Returns the newly created id or 0 if no new user was made(Could make email unique).
	 * @throws throws Exception if insertion new user is not possible
	 */
	public static int addNewUser(String username, String hashedPassword, String email, String salt) throws Exception {
		PreparedStatement prep = null;
		int newuserid = 0;
		try {
			
			//Try the prepared statement
			 prep = Database.getConnection().prepareStatement("INSERT INTO LUDOUSER (username, password, email, salt) VALUES (?,?,?,?)");

				prep.setString(1, username);
				prep.setString(2, hashedPassword);
				prep.setString(3, email);
				prep.setString(4, salt);

				newuserid = prep.executeUpdate();

		}catch (SQLException sqlException) {
			LOGGER.log(Level.WARNING, "Encounter sqlexception while adding new user: " + sqlException.getMessage(), sqlException);
			throw new Exception("Could not create new user");
		}finally {
			try {
				prep.close();
			}catch(NullPointerException | SQLException e) {LOGGER.log(Level.WARNING, "Failed to close:" + e.getMessage(), e);}
		}
		if(newuserid == 0) {
			throw new Exception("Could not create new user");
		}
		return newuserid;
	}

	/**
	 * Sets a session token for a user
	 *
	 * @param username - the username for the user that has the new token
	 * @param token - token that is freshly generated
	 * @throws Exception - throws on an error
	 */
	public static void setTokenForUser(String username, String token) throws Exception {
		PreparedStatement prep = null;
		try{
			prep = Database.getConnection().prepareStatement("UPDATE LUDOUSER SET token = ? WHERE username = ?");

			prep.setString(1, token); //
			prep.setString(2, username);
			prep.executeUpdate();

		} catch (SQLException sqlException) {
			LOGGER.log(Level.WARNING, "Cannot update/set token: " + sqlException.getMessage(),sqlException);
			throw new Exception("Failed setting/updating token");
		}finally {
			try {
				prep.close();
			}catch(NullPointerException | SQLException e) {LOGGER.log(Level.WARNING, "Failed to close:" + e.getMessage(), e);}
		}
	}

	/**
	 * Gets userid, username, token, tokenexpire and email for user
	 * Returns a ResultSet - Lucky for me I'm not responsible for actually get the data. I dunno how ResultSet works..Good luck
	 *
	 * @param username - String the userid of the user
	 * @return JSONObject - the userdata from the user keys: username, userid, salt, token, password
	 * @throws Exception on error
	 */
	public static JSONObject getUserDataForUser(String username) throws Exception {
		JSONObject userdata = new JSONObject();
		PreparedStatement prep = null;
		ResultSet result = null;
		try {
			prep = Database.getConnection().prepareStatement("SELECT userid, username, password, salt, token, email FROM LUDOUSER WHERE username = ?");


			prep.setString(1, username);
			Boolean autocommit = connection.getAutoCommit();
			LOGGER.log(Level.INFO, "AutoCommit: " + ((autocommit) ? ("true"):("false")));
			result = prep.executeQuery();
			while(result.next()) {
				//We have a row to read
				// We should only have one row. Always
				userdata.put("username", result.getString("username"));
				userdata.put("userid", result.getInt("userid"));
				userdata.put("salt", result.getString("salt"));
				userdata.put("token", result.getString("token"));
				userdata.put("password", result.getString("password"));
			}

		}catch (SQLException sqlException) {
			LOGGER.log(Level.WARNING, "Cannot get userdata: " + sqlException.getMessage(), sqlException);
		}finally {
			//Cleaning up

			try {
				result.close();	
			}catch(NullPointerException | SQLException e) {LOGGER.log(Level.WARNING, "Failed to close:" + e.getMessage(),e);}

			try {
				prep.close();
			}catch(NullPointerException | SQLException e) {LOGGER.log(Level.WARNING, "Failed to close:" + e.getMessage(), e);}	
		}

		return userdata;
	}

	/**
	 * Gets the password and hash from the database for a specific user identified by its username
	 * Caller gets access to this data as a ResultSet
	 * Use this as res.getString("password/salt");
	 *
	 * @param username - String the username from the user
	 * @return ResultSet - the resultSet returned
	 * @throws Exception is thrown on error
	 */
	public static ResultSet getPasswordHashAndTokenForUser(String username) throws Exception {
		ResultSet result = null;
		PreparedStatement prep = null;

		try  {
			prep = Database.getConnection().prepareStatement("SELECT password, salt FROM LUDOUSER WHERE username = ?");

			prep.setString(1, username);
			result = prep.executeQuery();
			prep.close();    

			}catch(SQLException e) {
				LOGGER.log(Level.WARNING, "Failed fetting password and salt" + e.getMessage(),e);
				throw new Exception("Could not get password and salt form database");
			}finally {
				//Cleaning up

				try {
					result.close();	
				}catch(NullPointerException | SQLException e) {LOGGER.log(Level.WARNING, "Failed to close:" + e.getMessage(),e);}

				try {
					prep.close();
				}catch(NullPointerException | SQLException e) {LOGGER.log(Level.WARNING, "Failed to close:" + e.getMessage(),e);}	
			}
			return result;
		}

	/**
	 * Tries to retrieve the username for the given token
	 * If no username can be found we throw an exception with a error message
	 * @param token - String - token from the user. 
	 * @return String username attached to the token
	 * @throws Exception - if username can not be retrieved we throw
	 */
	public static String getUsernameForToken(String token) throws Exception {
		ResultSet result = null;
		PreparedStatement prep = null;
		String username = null;
		try {

			prep = Database.getConnection().prepareStatement("SELECT username FROM LUDOUSER WHERE token = ?");


			prep.setString(1, token);
			result = prep.executeQuery();


			if(result.next()) {
				username = result.getString("username");
			}else{
				result.close();
				connection.close();
				throw new Exception("Username could not be retrieved for the spcified token");
			}

		}catch(SQLException e) {
			LOGGER.log(Level.WARNING, "Setting data to preparedstatement failed" +  e.getMessage(),e);
			throw new Exception("Username could not be retrieved for the spcified token");
		}finally {
			try {
				result.close();	
			}catch(NullPointerException | SQLException e) {LOGGER.log(Level.WARNING, "Failed to close:" + e.getMessage(),e);}

			try {
				prep.close();
			}catch(NullPointerException | SQLException e) {LOGGER.log(Level.WARNING, "Failed to close:" + e.getMessage(),e);}
		}
		return username;
	}

	/**
	 * Checks if a given email is already used. 
	 * email is NOT marked as unique in the database so checking is necessary.
	 * it tries to summarize all results from matching the email
	 * If the summary is greated than 0, then someone has that email
	 * @param email - String - the email to evaluate
	 * @return Boolean - true if the email is taken
	 */
	public static Boolean isEmailTaken(String email) {
		PreparedStatement prep = null;
		ResultSet result = null;
		int count = 0;
		try {

			prep = Database.getConnection().prepareStatement("SELECT COUNT(1) FROM LUDOUSER WHERE email = ?");

			prep.setString(1, email);
			result = prep.executeQuery();
			if(result.next()) {
				count = result.getInt(1);
			}
			

			LOGGER.log(Level.INFO, "Found " + count + " matching email: " + email);

		}catch(SQLException e) {
			LOGGER.log(Level.WARNING, e.getMessage(),e);
		}finally {
			try {
				result.close();	
			}catch(NullPointerException | SQLException e) {LOGGER.log(Level.WARNING, "Failed to close:" + e.getMessage(),e);}

			try {
				prep.close();
			}catch( NullPointerException |SQLException e) {LOGGER.log(Level.WARNING, "Failed to close:" + e.getMessage(),e);}
		}
		
		return(count > 0);
	}

	/**
	 * Takes a username and evaluates if the username is already taken
	 * The database alrady define sthe suername field as unique so creating a user with an exising usrname should fail
	 * Use this to give user a quick error message back if the username needs to be changed
	 * @param username - String - username the username to evaluate
	 * @return Boolean - true if the username is taken
	 */
	public static Boolean isUsernameTaken(String username) {
		PreparedStatement prep = null;
		ResultSet result = null;
		int count  = 0;
		try {
			prep  = Database.getConnection().prepareStatement("SELECT count(1) FROM LUDOUSER WHERE username = ?");

			prep.setString(1, username);
			result = prep.executeQuery();
			if(result.next()) {
				count = result.getInt(1);
			}
			LOGGER.log(Level.INFO, "Found " + count + " matching username: " + username);

		}catch(SQLException e) {
			LOGGER.log(Level.WARNING, e.getMessage(),e);
		}finally {
			try {
				result.close();	
			}catch(NullPointerException | SQLException e) {LOGGER.log(Level.WARNING, "Failed to close:" + e.getMessage(),e);}

			try {
				prep.close();
			}catch(NullPointerException | SQLException e) {LOGGER.log(Level.WARNING, "Failed to close:" + e.getMessage(),e);}
		}
		return (count>0);

	}
	/**
	 * After a game is done we have a winner. This user will get some gamepoints
	 * 
	 * @param gamepoints - the point a user got from playing
	 * @param userid - the user that got the points the id not the username
	 */
	public static void setLudoResult(int gamepoints, int userid){
		PreparedStatement prep = null;
		
		try{
			prep  = Database.getConnection().prepareStatement("INSERT INTO USERSTATS (gametime, gamepoints, userid) VALUES (CURRENT_TIMESTAMP,?,?)");
			prep.setInt(1, gamepoints);
			prep.setInt(2, userid);			
			
			prep.executeUpdate();
			
		}catch(SQLException e) {
			LOGGER.log(Level.WARNING, "Failed saving ludo game result" + e.getMessage(),e);
		}finally {
			try {
				prep.close();
			}catch(NullPointerException | SQLException e) {LOGGER.log(Level.WARNING, "Failed to close:" + e.getMessage(),e);}
		}
	}
	
	/**
	 * Gets the top ten players which have the most points
	 * @return
	 */
	public static JSONArray getTopTenMostPoints(){
		PreparedStatement prep = null;
		ResultSet result = null;
		JSONArray numberofgames = new JSONArray();
		try{
			prep  = Database.getConnection().prepareStatement(""
					+ "SELECT userstats.userid AS id, SUM(gamepoints) AS points, ludouser.username AS topplayer FROM userstats "
					+ "INNER JOIN ludouser ON userstats.userid = ludouser.userid "
					+ "GROUP BY userstats.userid, ludouser.username "
					+ "ORDER BY points DESC FETCH FIRST 10 ROWS ONLY ");		

			result = prep.executeQuery();
			
			
			int ranking = 0;
			
			while(result.next()) {
				JSONObject row = new JSONObject();
				int score = result.getInt("points");
				String user = result.getString("topplayer");
				
				row.put("username", user);
				row.put("number", Integer.toString(score));
				row.put("rank", ranking);
				numberofgames.put(row);
				ranking++;
			}

		}catch(SQLException e) {
			LOGGER.log(Level.WARNING, "Failed getTopTenMostPoints" + e.getMessage(),e);
		}finally {
			try {
				prep.close();
			}catch(NullPointerException | SQLException e) {LOGGER.log(Level.WARNING, "Failed to close:" + e.getMessage(),e);}
			try{
				result.close();
			}catch(NullPointerException | SQLException e) {LOGGER.log(Level.WARNING, "Failed to close:" + e.getMessage(),e);}
		}
		return numberofgames;
	}
	/**
	 * gets the top ten users for which has played the most games
	 * @return
	 */
	public static JSONArray getTopTenMostActivePlayers(){
		PreparedStatement prep = null;
		ResultSet result = null;
		JSONArray numberofgames = new JSONArray();
		try{
			prep  = Database.getConnection().prepareStatement(""
					+ "SELECT ludouser.username AS topplayer, COUNT(1) AS gamecount FROM USERSTATS "
					+ "INNER JOIN ludouser ON userstats.userid = ludouser.userid "
					+ "GROUP BY ludouser.username, userstats.userid "
					+ "ORDER BY gamecount DESC FETCH FIRST 10 ROWS ONLY");		
			
			result = prep.executeQuery();
			
			
			int ranking = 0;
			
			while(result.next()) {
				
				JSONObject row = new JSONObject();
				int ngames = result.getInt("gamecount");
				String user = result.getString("topplayer");
				
				row.put("username", user);
				row.put("number", Integer.toString(ngames));
				row.put("rank", ranking);
				numberofgames.put(row);
				ranking++;
			}

		}catch(SQLException e) {
			LOGGER.log(Level.WARNING, "Failed getTopTenMostActivePlayers" + e.getMessage(),e);
		}finally {
			try {
				prep.close();
			}catch(NullPointerException | SQLException e) {LOGGER.log(Level.WARNING, "Failed to close:" + e.getMessage(),e);}
			try{
				result.close();
			}catch(NullPointerException | SQLException e) {LOGGER.log(Level.WARNING, "Failed to close:" + e.getMessage(),e);}
		}
		return numberofgames;
		
	}
	

}
