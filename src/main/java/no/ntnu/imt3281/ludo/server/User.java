package no.ntnu.imt3281.ludo.server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import no.ntnu.imt3281.ludo.logic.Ludo;
import no.ntnu.imt3281.ludo.ontheline.ChatRoomEventListener;
import no.ntnu.imt3281.ludo.ontheline.OnTheLineEvent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import no.ntnu.imt3281.chat.ChatEvent;
import no.ntnu.imt3281.ludo.global.CommunicationID;
import no.ntnu.imt3281.ludo.global.Toolkit;
import no.ntnu.imt3281.ludo.logic.NoRoomForMorePlayersException;
import no.ntnu.imt3281.ludo.logic.dice.DiceEvent;
import no.ntnu.imt3281.ludo.logic.dice.DiceListener;
import no.ntnu.imt3281.ludo.logic.piece.PieceEvent;
import no.ntnu.imt3281.ludo.logic.piece.PieceListener;
import no.ntnu.imt3281.ludo.logic.player.PlayerEvent;
import no.ntnu.imt3281.ludo.logic.player.PlayerListener;


/**
 * Class for keeping tabs of users with active sessions, to be placed in a concurrent hashmap
 * Users handle for the most part socket interactions
 * This is a server class - though it could be implemented on the client, but the ship has sailed
 */
public class User implements DiceListener, PieceListener, ChatRoomEventListener, PlayerListener {

    private Socket s;
    private BufferedWriter output;
    private BufferedReader input;
    private String temporaryIdentificator;
    private String username;

    private LinkedBlockingQueue<String> outgoingOnTheLine = new LinkedBlockingQueue<>();

    //All incoming OnTheLineEvents are added to this
    //The user is suppose to go over these eventually
    private LinkedBlockingQueue<String> incomingOnTheLine = new LinkedBlockingQueue<>();

    private static final Logger LOGGER = Logger.getLogger(User.class.getName());

    /**
     * Used when u need a user/socket and the actions is:
     * HELLO
     */
    User(Socket s, String temporaryId) throws IOException {
        this.temporaryIdentificator = temporaryId;
        this.setSocket(s);
    }

    /**
     * Gets temporary identificator for the user, which is used before the user is authorized
     *
     * @return the identificator
     */
    private String getTemporaryIdentificator() {
        return this.temporaryIdentificator;
    }

    /**
     * Gets the username for this user
     * <p>
     * Username is used for the identificator for a user across database lookups and hashmaps
     *
     * @return the username
     */
    public String getUsername() {
        return this.username;
    }

    /**
     * Sets the socket the user will be connecting with in a variable, and adds output stream and input stream
     *
     * @param s socket to set
     * @throws IOException if an error occurs when creating input/output stream
     */
    private void setSocket(Socket s) throws IOException {
        this.s = s;
        output = new BufferedWriter(new OutputStreamWriter(this.s.getOutputStream()));
        input = new BufferedReader(new InputStreamReader(this.s.getInputStream()));
    }

    /**
     * adds a message to the outgoing queue
     *
     * @param outputMessage the message to be addded
     */
    private void addToOutputQueue(String outputMessage) {
        this.outgoingOnTheLine.add(outputMessage);
    }

    /*
     * ######################
     * THIS IS A WEIRD SECTION
     * ###################################
     * PLEASE READ THIS CAREFULLY
     *###################################################
     * There is 4 important functions. Yeah only 4...
     *
     * -> read()
     * Used by the server->readMessageThread to make the user read the contents it has in the input BufferedReader
     * The incoming is added to the LinkedBlockingQueue and nothing more
     *
     * -> startWorking
     * This is called by the Server->diligence thread. Diligence means HARDWORKING.
     * This will start the process by interpreting the content and DO WORK ON IT
     *
     * -> getRidOfOutgoingOnTheLine
     * This is used by Server->sendMessageThread to get the content in the LinkedBlockingQueue outgoingOnTheLine
     * and writes this to output BufferedWriter. This should be sent to the client
     *
     *
     *
     * -> addToIncomingOnTheLine
     * This is often just used one when the TCP session is just established.
     * It is utilized by the Server->dispatcher to make this instance add the received data to
     * its LinkedBlockingQueue incomingOnTheLine
     *
     *
     */


    /**
     * Reads the contents in the bufferedReader
     * Caller of this must assume that this string can be converted to a properly formatted json
     * If not u are out of luck
     */
    void read() {
        try {
            if (this.input.ready()) {
                String readinput = this.input.readLine();
                this.incomingOnTheLine.add(readinput);
            }
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, "Read failed: " + e.getMessage(), e);
        }
    }

    /**
     * Takes the content in outgoing messages and send them out to the BufferdWriter
     * If writing the message to the bufferedWriter fails this users activity status changes to false
     * We then assume that the socket is no longer active and we have to wait for the user to reconnect.
     */
    void getRidOfOutgoingOnTheLine() {
        try {
            String pulled = this.outgoingOnTheLine.poll();
            if (pulled != null) {
                //safe to send
                this.output.write(pulled);
                this.output.newLine();
                this.output.flush();
            }

        } catch (IOException e) {
            LOGGER.log(Level.WARNING, "Pulled is null: " + e.getMessage(), e);
        }
    }

    /**
     * This begins the path for which the user is going to do some work
     */
    void startWorking() {
        String incoming = this.incomingOnTheLine.poll();
        if (incoming != null) {
            this.handler(incoming);
        }
    }

    /**
     * Updates users in the game whenever a player state changes, only if the states are either 3 (LEFTGAME) or 4 (JOINED)
     * Updates statistic records upon won game
     * @param event contains data about the event and game
     */
    @Override
    public void playerStateChanged(PlayerEvent event) {
        Ludo ludo = (Ludo) event.getSource();
        // Sends response to clients when another player joins or leaves
        if (event.getState() > 2) {
            String gameid = event.getLudo().getGameId();
            String response = new OnTheLineEvent(true, "New user joined game", CommunicationID.PLAYERSTATE, Arrays.asList("gameid", gameid, "state", Integer.toString(event.getState()),
                    "username", ludo.getPlayerName(event.getActiveplayer()), "usercolor", Integer.toString(event.getActiveplayer()), "chatroomid", gameid)).toString();
            this.addToOutputQueue(response);

        } else if (event.getState() == 2) {
            try {
                //retrieves userdata so that we can get a userid for updating games played
                JSONObject userdata = Database.getUserDataForUser(getUsername());
                int userid = userdata.getInt("userid");

                if (event.getActiveplayer() == ludo.getPlayerConst(getUsername())) { // if player won, adds game played and victory
                    Database.setLudoResult(1, userid);
                } else { // if player lost, adds game played
                    Database.setLudoResult(0, userid);
                }

            } catch (Exception e) {
                LOGGER.log(Level.WARNING, e.getMessage(), e);
                return;
            }
        }
        LOGGER.log(Level.INFO, "Playerevent happened: " + event.toString());
    }

    /***
     * Updates users in the game whenever a piece has been moved
     * @param event custom PieceEvent containing data about the game and event
     */
    @Override
    public void pieceMoved(PieceEvent event) {
        String response = new OnTheLineEvent(true, "A piece was moved", CommunicationID.MOVE, Arrays.asList("gameid", event.getLudo().getGameId(),
                "startposition", Integer.toString(event.getStart()), "endposition", Integer.toString(event.getEnd()), "player", Integer.toString(event.getPlayerconst()))).toString();
        this.addToOutputQueue(response);
        LOGGER.log(Level.INFO, "Pieveevent happend: " + event.toString());
    }


    /**
     * Updates users in the game whenever a dice is thrown
     *
     * @param diceevent cointains data about the event that happened (dice throw) and the game state
     */
    @Override
    public void diceThrown(DiceEvent diceevent) {
        String response = new OnTheLineEvent(true, "Successfully rolled the dice", CommunicationID.RTD, Arrays.asList("diceresult", Integer.toString(diceevent.getDice()),
                "gameid", diceevent.getLudo().getGameId())).toString();
        this.addToOutputQueue(response);
        LOGGER.log(Level.INFO, "Diceevent happened: " + diceevent.toString());
    }

    /**
     * Updates users in the chat whenever a message is sent
     *
     * @param event contains data about the chat and the message
     */
    @Override
    public void newChatMessageEvent(ChatEvent event) {
        String response = new OnTheLineEvent(true, "New message", CommunicationID.CHATMESSAGE, event.getEvent()).toString();
        this.addToOutputQueue(response);
    }


    /**
     * Updates users in the chat that the subscribers to that room changed, someone either left or joined
     *
     * @param event information about the event that occurred
     */
    @Override
    public void subscribersInChatRoomChanged(ChatEvent event) {
        String response = new OnTheLineEvent(true, "Chatroom subscribers updated", CommunicationID.CHATROOMUSERS, event.getEvent()).toString();
        this.addToOutputQueue(response);

    }

    /**
     *adds response when joining chat
     * @param event event object describing the event
     */
    @Override
    public void joinChatRoomResponse(OnTheLineEvent event) {
        this.addToOutputQueue(event.toString());
    }

    /**
     * Used when creating a new user that has no entry in the database
     * It takes the body part of a request
     * It verifies that provided credentials is not already used by other users
     * It send an error through the socket if it fails
     *
     * @param event contains the data about the event that happened, the request received
     */
    private void newUser(OnTheLineEvent event) {
        JSONObject req = event.getBody();
        String email = req.getString("email");
        String requestusername = req.getString("username");
        String password = req.getString("password");

        if (!Toolkit.checkValidEmail(email)) {
            // Email is not valid
            // This is also done on the client! SO secure :)
            String response = new OnTheLineEvent(false, "Email is not a valid email. Please check your email and try again", CommunicationID.REGISTRATION).toString();
            this.addToOutputQueue(response);
            return;
        }

        // There are 2 scenarios
        // username is taken
        // email already registered
        Boolean emailTaken = Database.isEmailTaken(email);
        Boolean usernameTaken = Database.isUsernameTaken(requestusername);

        String message = "";
        Boolean failed = false;
        if (emailTaken && usernameTaken) {
            message = "Email and username is already registered.";
            failed = true;
        } else if (emailTaken) {
            message = "Email is already registered.";
            failed = true;
        } else if (usernameTaken) {
            message = "Username is already registered";
            failed = true;
        }
        if (failed) {
            // Return error message
            LOGGER.log(Level.WARNING, "Email or username is taken: " + message);
            String response = new OnTheLineEvent(false, message, CommunicationID.REGISTRATION).toString();
            this.addToOutputQueue(response);
            return;
        }

        // User is ready to be added to the database and this instance to be updated
        String salt = Server.generateSalt();
        String passwordHash = Server.hashPass(password, salt);

        try {
            int newuserid = Database.addNewUser(requestusername, passwordHash, email, salt);
            if (newuserid > 0) {
                Server.log("New user added: " + requestusername);
                this.loginUser(requestusername, password);
            } else {
                //User could not be added to database
                //Since it didnt threw it probably means the user exists already
                String response = new OnTheLineEvent(false, "User already exists", CommunicationID.REGISTRATION).toString();
                this.addToOutputQueue(response);
            }

        } catch (Exception ex) {
            LOGGER.log(Level.WARNING, "Adding new user failed: " + ex.getMessage(), ex);
            String response = new OnTheLineEvent(false, "New user could not be added", CommunicationID.REGISTRATION).toString();
            this.addToOutputQueue(response);
        }

    }


    /**
     * Logs a user in from a request, meaning the data (username and password is in the request
     *
     * @param event contains the request
     */
    private void loginFromRequest(OnTheLineEvent event) {
        JSONObject body = event.getBody();

        String requestusername = body.getString("username");
        String requestpassword = body.getString("password");

        this.loginUser(requestusername, requestpassword);
    }

    /**
     * Log a user in using
     * username and password
     * A token is generated and is give to the user eventually
     *
     * @param username username in cleartext
     * @param password password in cleartext
     */
    private void loginUser(String username, String password) {
        if (!Server.validateUser(username, password)) {
            // Cant validate user
            // Incorrect credentials
            String response = new OnTheLineEvent(false, "Username or password is incorrect", CommunicationID.LOGIN).toString();
            this.addToOutputQueue(response);
        } else {
            String token = Server.newToken();
            this.username = username;
            this.login(token);
        }
    }

    /**
     * A user logs in using token
     * We check if a token in the database match this.
     * If we find a match we have a user.
     *
     * @param event contains the request and therefore the token
     */
    private void loginWithToken(OnTheLineEvent event) {
        // Do database lookup and see if token is valid for a user
        // and if valid get its userdata and update hashmap
        JSONObject body = event.getBody();

        String token = body.getString("token");
        try {
            this.username = Database.getUsernameForToken(token);
            this.login(token);
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "Login with token failed: " + e.getMessage(), e);
            String response = new OnTheLineEvent(false, "Authentication with token failed", CommunicationID.AUTHORIZE).toString();
            this.addToOutputQueue(response);
        }
    }

    /**
     * Updates token in database
     * Updates the key in the server users concurrent hashmap since that key was probably not correct
     * since that request originated without a user-session being created
     *
     * @param token the token
     */
    private void login(String token) {
        try {
            Database.setTokenForUser(username, token);

            Server.updateExistingUserKeyInMemory(this.getUsername(), this.getTemporaryIdentificator());
            Server.log("User logged in: " + username);
            Server.log("Added " + username + " to subscribers in lobby");
            Server.joinChatroom(Server.getGlobalchat(), this);
            String response = new OnTheLineEvent(true, "Login successful", CommunicationID.AUTHORIZE,
                    Arrays.asList("token", token, "username", this.getUsername(), "chatroomid", Server.getGlobalchat())).toString();
            this.addToOutputQueue(response);

        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "Failes login in because setting token failed", e);
        }

    }

    /**
     * The handler which dispatches requests depending on their request id for the user making
     * the requests. Converts the request from String format to an OnTheLineEvent which is sent as a parameter to the
     * functions that handles the request in need of the data.
     *
     * @param incoming The request in string format
     */
    private void handler(String incoming) {

        try {

            OnTheLineEvent onthelinevent = new OnTheLineEvent(incoming);

            String id = onthelinevent.getId();

            switch (id) {
                case CommunicationID.REGISTRATION:
                    // New User registration
                    LOGGER.log(Level.INFO, "Request id 1: Register");
                    this.newUser(onthelinevent);
                    break;
                case CommunicationID.RTD: // Roll the dice
                    LOGGER.log(Level.INFO, "Request id 2: RTD");
                    this.rollTheDice(onthelinevent);
                    break;
                case CommunicationID.MOVE:
                    LOGGER.log(Level.INFO, "Request id 3: Move");
                    this.movePlayerToken(onthelinevent);
                    break;
                case CommunicationID.CHATMESSAGE: // Chat message
                    LOGGER.log(Level.INFO, "Request id 4: Chat message");
                    Server.msgFromClient(onthelinevent, this.getUsername());
                    break;
                case CommunicationID.STARTAGAME: // Start game
                    LOGGER.log(Level.INFO, "Request id 5: Start a game");
                    this.newGame();
                    break;
                case CommunicationID.JOINGAME: // Join game
                    LOGGER.log(Level.INFO, "Request id 6: Join game");
                    this.joinGame(onthelinevent);
                    break;
                case CommunicationID.LOGOUT: // Log user out
                    LOGGER.log(Level.INFO, "Request id 7: Log out");
                    this.logOut();
                    break;
                case CommunicationID.LOGIN: // Login with username and password
                    LOGGER.log(Level.INFO, "Request id 8: Log in");
                    this.loginFromRequest(onthelinevent);
                    break;
                case CommunicationID.AUTHORIZE:
                    LOGGER.log(Level.INFO, "Request id 11: Authorize user");
                    this.loginWithToken(onthelinevent);
                    break;
                case CommunicationID.LISTAVAILABLEGAMES:
                    LOGGER.log(Level.INFO, "Request id 12: List available games");
                    this.listAvailableGames();
                    break;
                case CommunicationID.LISTCHATS:
                    LOGGER.log(Level.INFO, "Request id 13: List available chats");
                    this.listAvailableChats();
                    break;
                case CommunicationID.JOINCHATROOM:
                    LOGGER.log(Level.INFO, "Request id 14: Join chatroom");
                    this.joinChatRoom(onthelinevent);
                    break;
                case CommunicationID.CHATROOMUSERS:
                    //15 - List all chatroom users
                    LOGGER.log(Level.INFO, "Request id 15: List chatroomusers");
                    this.listUsersInChatroom(onthelinevent);
                    break;
                case CommunicationID.INVITE:
                    LOGGER.log(Level.INFO, "Request id 16: Invite");
                    this.invitePlayers(onthelinevent);
                    break;
                case CommunicationID.STATISTICS:
                    LOGGER.log(Level.INFO, "Request id 17: Statistic");
                    this.sendStatistics();
                    break;
                case CommunicationID.ERROR: // Client error
                    LOGGER.log(Level.WARNING, "Request id 9: Error");
                    break;
                default:
                    LOGGER.log(Level.WARNING, "Unknown id in request");
                    OnTheLineEvent response = new OnTheLineEvent(false, "That id has no power here", CommunicationID.ERROR);
                    this.addToOutputQueue(response.toString());
                    break;
            }
        } catch (JSONException e) {
            LOGGER.log(Level.WARNING, "Could not read id of request", e);
        }
    }

    /**
     * Rolls the dice on the server using the ludo object for the game, and responds to the user if game isnt found
     *
     * @param event the data about the request
     */
    private void rollTheDice(OnTheLineEvent event) {
        JSONObject body = event.getBody();

        String gameid = body.getString("gameid");

        if (!Server.rollDice(gameid)) {
            //Could not find game
            String response = new OnTheLineEvent(false, "Game not found", CommunicationID.RTD).toString();
            this.addToOutputQueue(response);
        }
    }

    /**
     * Receives the move token request, and handles it on the server using the ludo object, responds to the user if
     * it fails
     *
     * @param event the move token request
     */
    private void movePlayerToken(OnTheLineEvent event) {
        JSONObject body = event.getBody();

        String gameid = body.getString("gameid");
        int player = body.getInt("player");
        int startPosition = body.getInt("startposition");
        int endPosition = body.getInt("endposition");


        if (!Server.movePiece(gameid, player, startPosition, endPosition)) {
            String response = new OnTheLineEvent(false, "Could not move piece", CommunicationID.MOVE).toString();
            this.addToOutputQueue(response);
        }

    }

    /**
     * Called when the user wants a new Ludo Game
     * This can handle creation of game and answers the user about the newly created game
     */
    private void newGame() {
        String gameid = Server.newGame(this.getUsername(), this, this, this, this);

        //Create game success. Respond to user with gameid and chatroomid
        String response = new OnTheLineEvent(true, "Game creation success", CommunicationID.STARTAGAME, Arrays.asList("gameid", gameid, "chatroomid", gameid)).toString();
        this.addToOutputQueue(response);
    }

    /**
     * Joins this user to a random game if a random game could be found
     * If no random game is found then it attempts to create a new game
     * <p>
     * if provided with a gameid this attempts to join that game - this user will get a join game response and get information about the game
     * The other users of that game should be notified by their eventlisteners
     *
     * @param event contains data about the request to join
     */
    private void joinGame(OnTheLineEvent event) {
        JSONObject body = event.getBody();
        String response;

        String gameid = body.getString("gameid");

        // Finds a random game to join if no game id is set
        //Empty gameid is received by the Client when a random game is chosen
        if ("".equals(gameid)) {
            // Get the random game id if a random game can be found
            gameid = Server.getRandomAvailableGameId(this.getUsername());
        }

        // If there is no random game found then gameid should be null
        if (gameid == null) {
            LOGGER.log(Level.INFO, "Could not find random game to join - creating new game");
            this.newGame();
        } else {
            //join a specific game by gameID
            //The important part here is that this user needs information about the existing users in the game
            try {
                JSONArray existingusers = Server.getExistingUsers(gameid); //Need to get them before adding player since the returned value should return players already there

                if (!Server.joinGame(gameid, this.getUsername(), this, this, this, this)) {
                    response = new OnTheLineEvent(false, "The game could not be found", CommunicationID.JOINGAME).toString();
                    this.addToOutputQueue(response);
                } else {
                    response = new OnTheLineEvent(true, "New user joined game", CommunicationID.JOINGAME, Arrays.asList("existingusers", existingusers,
                            "chatroomid", gameid, "gameid", gameid)).toString();
                    this.addToOutputQueue(response);
                }
            } catch (NoRoomForMorePlayersException nrfmpe) {
                response = new OnTheLineEvent(false, nrfmpe.getMessage(), CommunicationID.JOINGAME).toString();
                this.addToOutputQueue(response);
                LOGGER.log(Level.WARNING, nrfmpe.getMessage(), nrfmpe);
            }
        }

    }

    /**
     * Receives logout requests from the user, and logs out, sends response upon success/failure
     */
    private void logOut() {
        if (!Server.logOut(this.getUsername())) {
            String response = new OnTheLineEvent(false, "User is either not logged in or not found", CommunicationID.LOGOUT).toString();
            this.addToOutputQueue(response);
            Server.logUserOut(this.getUsername());
            return;
        }
        String response = new OnTheLineEvent(true, "User is logged out", CommunicationID.LOGOUT).toString();
        this.addToOutputQueue(response);
    }


    /**
     * Calls a method in Server to get a list of available games
     * The client should periodically get this list so the user don't try to join a game that is full
     */
    private void listAvailableGames() {
        String response = new OnTheLineEvent(true, "Sending back active games", CommunicationID.LISTAVAILABLEGAMES, Server.getAvailableGames()).toString();
        this.addToOutputQueue(response);
    }

    /**
     * List the available chats the user can join
     * The user will most likely not send this request
     * Instead the Server updates the user about the changes in
     * available games
     */
    private void listAvailableChats() {
        JSONObject chatrooms = new JSONObject();
        chatrooms.put("chatrooms", Server.getAvailableChats());
        String response = new OnTheLineEvent(true, "All chatrooms", CommunicationID.LISTCHATS, chatrooms).toString();
        this.addToOutputQueue(response);
    }

    /**
     * When the user wants to list the users in a particular chatroom
     * this is called
     * The caller needs to implement ChatRoomEventListener
     *
     * @param event - The event that the request originated from
     */
    private void listUsersInChatroom(OnTheLineEvent event) {
        JSONObject body = event.getBody();
        String chatroomid = body.getString("chatroomid");
        Server.listUsersInChatRoom(chatroomid, this);
    }

    /**
     * Receives a join chatroom request, and adds the user to a chatroom
     *
     * @param event the event that the request comes from
     */
    private void joinChatRoom(OnTheLineEvent event) {
        JSONObject body = event.getBody();
        String chatid = "";
        try {
            chatid = body.getString("chatroomid");
        } catch (JSONException e) {
            LOGGER.log(Level.WARNING, "chatroomid or chatroomname missing(that is okay)" + e.getMessage(), e);
        }
        Server.joinChatroom(chatid, this);
    }

    /**
     * Receives a invite players request, creates a game, and responds to the user inviting with the game, sends
     * invites to the users
     *
     * @param event the event containing the request
     */
    private void invitePlayers(OnTheLineEvent event) {
        JSONObject body = event.getBody();
        String gameid = Server.newGame(this.getUsername(), this, this, this, this);
        String response = new OnTheLineEvent(true, "Game creation success", CommunicationID.STARTAGAME, Arrays.asList("gameid", gameid, "chatroomid", gameid)).toString();
        this.addToOutputQueue(response);

        Server.invitePlayers(body, gameid);
    }

    /**
     * Sends a response to client sending victory and number of played games data for the top ten of each category
     */
    private void sendStatistics() {
        JSONArray toptenmostactive;
        JSONArray toptenmostwon;
        String response;
        try {
            toptenmostactive = Database.getTopTenMostActivePlayers();
            toptenmostwon = Database.getTopTenMostPoints();
            response = new OnTheLineEvent(true, "Successfully retrieved statistics", CommunicationID.STATISTICS,
                    Arrays.asList("numberofgames", toptenmostactive, "numberofwins", toptenmostwon)).toString();
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, e.getMessage(), e);
            response = new OnTheLineEvent(false, "Could not retrieve statistics, might be empty", CommunicationID.STATISTICS).toString();
        }
        this.addToOutputQueue(response);
    }

    /**
     * sends the invite response to a user
     *
     * @param body the body of the response to be sent, contains gameid, inviteer and all the other invited players
     */
    void sendInvite(List body) {
        String response = new OnTheLineEvent(true, "Invited to game", CommunicationID.INVITE, body).toString();
        this.addToOutputQueue(response);
    }
}
