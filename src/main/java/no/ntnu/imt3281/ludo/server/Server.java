package no.ntnu.imt3281.ludo.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.SecureRandom;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.scene.image.Image;
import no.ntnu.imt3281.chat.Message;
import no.ntnu.imt3281.ludo.logic.NoRoomForMorePlayersException;
import no.ntnu.imt3281.ludo.logic.dice.DiceListener;
import no.ntnu.imt3281.ludo.logic.piece.PieceListener;
import no.ntnu.imt3281.ludo.logic.player.PlayerListener;
import no.ntnu.imt3281.ludo.ontheline.ChatRoomEventListener;
import no.ntnu.imt3281.ludo.ontheline.OnTheLineEvent;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import no.ntnu.imt3281.chat.ChatEvent;
import no.ntnu.imt3281.chat.ChatRoom;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import no.ntnu.imt3281.ludo.logic.Ludo;

// java.util.concurrent imports

// Local imports

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

/**
 * s is the main class for the server.
 * **Note, change this to extend other classes if desired.**
 *
 */

public class Server extends Application {

    /**
     * Tar vare på alle bruker sesjoner som er aktive.
     * Disse lever kun i minnet og dør ut om programmet avsluttes.
     * Det er trygt for flere tråder å aksessere et concurrent hashmap.
     */
    private static final ConcurrentHashMap<String, User> users = new ConcurrentHashMap<>();

    /**
     * Tar vare på alle ludo spill som er aktive.
     * Disse lever kun i minnet og dør ut om programmet avsluttes.
     * Det er trygt for flere tråder å aksessere et concurrent hashmap.
     */
    private static final ConcurrentHashMap<String, Ludo> ludoGames = new ConcurrentHashMap<>();

    private static final ConcurrentHashMap<String, ChatRoom> chatrooms = new ConcurrentHashMap<>();

    private static final Logger LOGGER = Logger.getLogger(Server.class.getName());

    private static boolean shutdown = false;

    private static ServerController serverController;

    /**
     * Getter for globalchat id
     * @return globalchatid
     */
    static String getGlobalchat() {
        return globalchat;
    }

    private static String globalchat;

    /**
     * Starts the server gui
     * @param primaryStage the stage to sset
     * @throws Exception loader might throw an exception if an error occurs during loading
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("Server.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.getIcons().add(new Image("images/servericon.png"));
        primaryStage.setTitle("Server view for Ludo application");
        primaryStage.show();

        Server.serverController = loader.getController();
    }

    /**
     * Starts a new thread that reads all incoming requests to the server.
     *
     * @param args possible arguments to start the program with, not used
     */
    public static void main(String[] args) {
       
        ChatRoom lobbychat = new ChatRoom();
        lobbychat.setChatroomName("Lobby");
        Server.globalchat = lobbychat.getChatRoomId();
        Server.chatrooms.put(globalchat, lobbychat);
        
        //Database stuff
        Database.createDB();
        Database.createSchema();

        // New thread to handle incoming socket communication
        new Thread(() -> dispatcher()).start();

        //The diligence is the thread that does the work
        new Thread(()->diligence()).start();
        
        // New thread to read messages from queue
        new Thread(() -> readMessagesThread()).start();
        // New thread to send messages from queue
        new Thread(() -> sendMessageThread()).start();

        launch(args); // Returnerer ikke før grensesnittet avsluttes
        System.exit(0);    // Shut down application when GUI closes. Makes sure runserer thread and others close too.
    }

    /**
     * Stops the server
     */
    static void stopServer() {
        serverController.log("Started shutting down...");
        
        Server.chatrooms.forEachValue(100, chatroom->{
        	chatroom.shutdown();
        });
        
        Server.shutdown = true;
        
        Platform.exit();
    }

    /**
     * dispatcher
     * noun
     * > a person whose job is to receive messages and organize the movement of people or vehicles,
     * > especially in the emergency services.
     * <p>
     * New TCP connections are received by this.
     * This doesn't accept anything that doesn't include a username
     */
    private static void dispatcher() {
        LOGGER.log(Level.INFO, "Server started");

        while (!shutdown) {

            try (ServerSocket server = new ServerSocket(Config.SERVERPORT)) {

                Socket sock = server.accept();

                String remoteAddr = sock.getInetAddress().getHostName();
                int remotePort = sock.getPort();
                
                String newConnection = "Connection from: " + remoteAddr + ":" + remotePort;
                LOGGER.log(Level.INFO, newConnection);
                serverController.log(newConnection);

                try {
                	//Let the User find out who this is
                    String unknownUserId = UUID.randomUUID().toString();
                    User unknownUser = new User(sock, unknownUserId);
                    Server.users.put(unknownUserId, unknownUser);

                } catch (NullPointerException e) {
                    LOGGER.log(Level.WARNING, "Catched nullpointer exception. Could not put user" + e.getMessage(), e);
                }


            } catch (SecurityException | IOException e) {
                LOGGER.log(Level.WARNING,"Socket accept or reading failed: " + e.getMessage(), e);
            }
        }
        serverController.log("Server stopped");
    }


    /**
     * This is a thread
     * This is the worker thread - this takes makes the User class work/read its incomingOnTheLine messages and
     * do some work on it
     */
    private static void diligence() {
    	while(!shutdown) {
    	Server.users.forEachValue(100, user->{
    		user.startWorking();
    	});
    	}
    }
    
    /**
     * THIS IS A THREAD
     * A thread that goes over all channels and checks for messages that need to be sent.
     */
    private static void sendMessageThread() {

        while (!shutdown) {
            //Goes over the chatrooms and empty their outgoing messages
        	//TODO: chatroom must add to user outgoing queue
        	Server.chatrooms.forEachValue(100, chatroom -> chatroom.sendMessage());
           
            Server.users.forEachValue(100, user -> {
            	user.getRidOfOutgoingOnTheLine();
            });

            //Make the thread sleep a few millisecons to avoid system exhaustion
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
            	LOGGER.log(Level.WARNING, "Could not make the thread sleep...I am tired now. Going to bed" + e.getMessage(), e);
            }
        }

    }

    /**
     * THIS IS A THREAD
     * <p>
     * This iterates over the users and does a read which is non blocking
     * users are responsible to maintain a socket and output contents to it.
     * 
     * Here we want to send a
     * 1. List of available games that should be displayed in the body
     * 
     */
    private static void readMessagesThread() {
        while (!shutdown) {
            Server.users.forEachValue(100, user -> {
            	
                user.read();
            });

            //Make the thread sleep a few millisecons to avoid system exhaustion
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
            	LOGGER.log(Level.WARNING, "Failed to make thread sleep: " + e.getMessage(), e);
            }
        }
    }

    /**
     * It is not always known the username of the user/socket requests
     * Therefor UUID is used in some instances for the key in users hashmap
     * But we wish to use username instead for further identification
     * <p>
     * You can use this function to update the key used in the users concurrentHashMap
     *
     * @param withUsername the username to put in memory
     * @param fromUUID identifier for the user in users
     */
    static void updateExistingUserKeyInMemory(String withUsername, String fromUUID) {
        User temp = Server.users.remove(fromUUID);
        if (temp == null) {
            return;
        }
        try {
            Server.users.put(withUsername, temp);
        } catch (NullPointerException npe) {
            Server.LOGGER.log(Level.WARNING, "Could not add user to users map: " + npe.getMessage(), npe);
        }
    }

    /**
     * Gets the the game by gameid from the hashmap, and uses the game object to throw the dice
     *
     * @param gameid identificator of the object in the hashmap
     * @return true/false depending on the funciton finding the game object with the gameid
     */
    static boolean rollDice(String gameid) {
        Ludo game = ludoGames.get(gameid);
        if (game == null) {
            return false;
        }
        game.throwDice();
        return true;
    }

    /**
     * Is called from user to move a piece, it retrieves the game from the hashmap and through it calls for the correct
     * function in the Ludo class.
     *
     * @param gameid identifying which game in the hashmap
     * @param player number identifying which player in the game is to be moved
     * @param start  start position of the piece to be moved
     * @param end    end position of the piece to be moved
     * @return either true or false depending on movePiece runs successfully
     */
    static boolean movePiece(String gameid, int player, int start, int end) {
        Ludo game = ludoGames.get(gameid);

        return game.movePiece(player, start, end);
    }


    /**
     * Receives message from user - the caller must know who got the request that is parameter two(2)
     * The caller must also have the whole OnTheLineEvent
     * gets the chatroom for the specified chatroomid
     * adds message to Chatroom by id
     * update users
     * @param event data about the event that occured,
     * @param username identifies the user
     */
    static void msgFromClient(OnTheLineEvent event, String username) {
    	LOGGER.log(Level.INFO, "Received message from client");
        JSONObject body = event.getBody();
        LOGGER.log(Level.INFO, body.toString(1));
        ChatEvent chatevent = new ChatEvent(body, username, "");
        
        ChatRoom chat = Server.chatrooms.get(chatevent.getChatroomId());
        if(chat != null) {
        	 try {
             	Message message = chatevent.getChatRoomNewMessage();
             	
             	chat.newMessage(message);
             	
             }catch(JSONException e) {
             	LOGGER.log(Level.WARNING, "Message from client failed: " + e.getMessage(), e);
             }catch(Exception e) {
             	 LOGGER.log(Level.WARNING, "Exception from newMessage" + e.getMessage(), e);
             }
        }else {
        	LOGGER.log(Level.WARNING, "chatrooms contains no mapping for: " + chatevent.getChatroomId());
        }
       
    }

    /**
     * creates a new game
     * Adds the new game to ludoGames hashmap
     * Respond to user with the newly created gameid
     *
     * @param username username so that the user creating the game can be added to it
     * @param dice listener for user
     * @param piece piecelistener for user
     * @param player playerlistener for user
     * @param chatUser chatroomeventlistener for user
     * @return String - returns the id for the new game or null if game could not be created
     */
    static String newGame(String username, DiceListener dice, PieceListener piece, PlayerListener player, ChatRoomEventListener chatUser) {
        Ludo newGame = new Ludo();
        //creates and returns a new game id for the Ludo
        //TODO: ComputeIfAbsent so that game doesnt overwrite previous
        String gameid = newGame.setGameId();
        ludoGames.put(gameid, newGame);
        try {
            newGame.addDiceListener(dice);
            newGame.addPieceListener(piece);
            newGame.addPlayer(username);
            newGame.addPlayerListener(player);

            // For a game the chatid and gameid is equal
            //We are living in a fantastic equal world
            ChatRoom gameChat = new ChatRoom(gameid);
            gameChat.setIsPartOfGame(true);
            Server.chatrooms.put(gameid, gameChat);
            Server.joinChatroom(gameid, chatUser);

        } catch (NoRoomForMorePlayersException e) {
            LOGGER.log(Level.WARNING, e.getMessage(), e);
        }
        return newGame.getGameId();
    }

    /**
     * User joins specific game by id
     * adds user to participants for the game
     * @param gameid identfying the game
     * @param username identifying the user
     * @param dice listener for user
     * @param piece piecelistener for user
     * @param player playerlistener for user
     * @param chatUser chatroomeventlistener for user
     * @return either true or false depending on success
     */
    static boolean joinGame(String gameid, String username, DiceListener dice, PieceListener piece, PlayerListener player, ChatRoomEventListener chatUser) {
        Ludo game = ludoGames.computeIfPresent(gameid,(ludogameid, oldvalue) -> {
            oldvalue.addDiceListener(dice);
            oldvalue.addPieceListener(piece);
            oldvalue.addPlayer(username);
            oldvalue.addPlayerListener(player);
            return oldvalue;
        });
        if(game != null) {
        	 Server.joinChatroom(gameid, chatUser);
        	 return true;
        }
        return false;
    }

    /**
     * Join the given user to random game
     * If no random game could be joined then this returns null
     * @param username identifying the user
     * @return String if game is made. or null if no game could be made
     */
    static String getRandomAvailableGameId(String username) {
        
        Optional<Entry<String, Ludo>> randomgame = Server.ludoGames.entrySet().stream().filter( e-> ((e.getValue().nrOfPlayers() < 4) && ("Initiated".equals(e.getValue().getStatus())) &&
                !e.getValue().getGameUsers().contains(username)) ).findFirst();
        if(randomgame.isPresent()) {
        	try {
        	Entry<String,Ludo> entry = randomgame.get();
        	Ludo game = entry.getValue();
        	LOGGER.log(Level.INFO, "User: " + username + " joined random game: " + game.getGameId());
        	return game.getGameId();
        	}catch(NoSuchElementException e) {
        		LOGGER.log(Level.WARNING, "Ludo game dissapeard" + e.getMessage(), e);
        	}
        }
        return null;
    }

    /**
     * Logs user out
     * removes user from hashmap and and token from db
     * @param username whom to logout
     * @return either true or false depending on if it was successful
     */
    static boolean logOut(String username) {
        return (users.get(username) == null);
    }

    /**
     * Breaks the connection with user
     * @param username user to break connection with.
     */
    static void logUserOut(String username) {
    	users.remove(username);
    }


    /**
     * Loops over available games that are not full
     * and craft a response so the user gets a nice list
     * If the number of players in agame is less than 4 and the game status is initiated we assume
     * that it can accept new players.
     * @return JSONObject - can be put directly in a body response
     */
    static JSONObject getAvailableGames() {
    	JSONObject games = new JSONObject();
        JSONArray lists = new JSONArray();
        Server.ludoGames.forEachValue(100, ludo -> {
        	JSONObject game = new JSONObject();
            if (ludo.nrOfPlayers() < 4 && "Initiated".equals(ludo.getStatus())) {
            	game.put("gameid", ludo.getGameId());
                JSONArray listusers = new JSONArray();
                listusers.put(ludo.getUsernames());
                game.put("users", listusers);
                lists.put(game);
            }
        });
        games.put("games", lists);
        return games;
    }

    
    /**
     * Returns a list containing all chats that are available to be joined
     * Does not hopefully return Lobby or Ludo game specific chatrooms
     * @return a jsonArray containing available chats
     */
    static JSONArray getAvailableChats() {
    	JSONArray chatrooms = new JSONArray();
    	
        Server.chatrooms.forEachValue(100, chatroom -> {
            if (!chatroom.getIsPartOfGame() && !chatroom.getChatRoomId().equals(Server.getGlobalchat())) {
            	JSONObject chatroominfo = new JSONObject();
            	chatroominfo.put("chatroomid", chatroom.getChatRoomId());
            	chatroominfo.put("usernames", chatroom.getSubscribersAsUsername());
            	chatroominfo.put("chatroomname", chatroom.getChatroomName());
            	chatrooms.put(chatroominfo);
            }
        });
        LOGGER.log(Level.INFO, "The available chats are: " + chatrooms.toString());
        return chatrooms;
    }

    /**
     * For the specified chatroomid gets the ChatRoom
     * The caller must implement the ChatRoomEventListener to receive the content
     * @param chatroomid identifying the chatroom
     * @param user, the event object describing the event that occured, so that it can be handled
     */
    static void listUsersInChatRoom(String chatroomid, ChatRoomEventListener user) {
    	
    	ChatRoom chatRoom = Server.chatrooms.get(chatroomid);
    	chatRoom.publishChatRoomUsers(user);
    }
    
    /**
     * Use this to create or join a ChatRoom
     * 
     * @param chatid - the chatid or an emtpy string "" to create a new chatroom
     * @param user - the user that wishes to receive messages. often "this"
     */
    static void joinChatroom(String chatid, ChatRoomEventListener user) {
    	if(user == null) {
    		LOGGER.log(Level.INFO, "User has dissapear??");
    		
    	}
        if ("".equals(chatid)) {
            ChatRoom chatroom = new ChatRoom();
            chatroom.addSubscriber(user);
            Server.chatrooms.put(chatroom.getChatRoomId(), chatroom);
        } else {
        	if(user != null) {
        		ChatRoom chatroom = Server.chatrooms.get(chatid);
                chatroom.addSubscriber(user);
        	}else {
        		LOGGER.log(Level.WARNING, "Tried to add a user to a chatroom. The user dissapeard");
        	}
            
        }
    }

    /**
     * Getting existing users for a ludo game
     * The returned value for this should be added to a JSONObject 
     * 
           [ {"username":"User1","color":"0"},
            {"username":"User2","color":"1"},
            {"username":"User3","color":"2"}
        ]
     * @param gameid identifies the game to check
     * @return JSONrray containing JSONObject with username and color as keys
     */
    static JSONArray getExistingUsers(String gameid) {
        Ludo game = Server.ludoGames.get(gameid);
        JSONArray exsistingusers = new JSONArray();

        int nr = game.nrOfPlayers();
        
        for(int i = 0; i < nr; i++) {
            JSONObject existinguser = new JSONObject();
        	//For each user
        	String username = game.getPlayerName(i);
        	
        	existinguser.put("username", username);
        	existinguser.put("color", i);
            exsistingusers.put(existinguser);
        }
        return exsistingusers;
    }

    /**
     * Receives invite users request from user, further calls the invite function in the user to send out invites to each specific user
     * @param body Body of the request containing which users to invite and who invited
     * @param gameid the gameid identifying the game
     */
    static void invitePlayers(JSONObject body, String gameid) {
        JSONArray invitedUsers = body.getJSONArray("invitedplayers");
        for (Object user : invitedUsers) {
            if (user instanceof String) {
                users.get(user).sendInvite(Arrays.asList("gameid", gameid, "inviter", body.getString("inviter"), "invitedplayers", invitedUsers));
            }

        }
    }

    /**
     * Generates a strong random session token for the user
     * @return returns a unique session token
     */
    static String newToken() {
        SecureRandom token = new SecureRandom();
        byte bytes[] = new byte[256];
        token.nextBytes(bytes);

        return byteToHexString(bytes);
    }

    /**
     * Validates the individual trying to login against credentials in the db
     * Takes username and password
     * From the database it fetches the password hash for the user and compares
     *
     * @param username the username to check against db
     * @param password the password to check against the db
     * @return either true or false depending on the credentials being correct
     */
    static boolean validateUser(String username, String password) {
    	LOGGER.log(Level.INFO, "Validating user with: " + username + " - " + password);
        try {
        	JSONObject userdata = Database.getUserDataForUser(username);
        	//Check if the userdata have content
        	if(userdata.has("password")){
                //Gets the password hash
                String passwordhash = userdata.getString("password");
                String salt = userdata.getString("salt");

                String hashedClearText = Server.hashPass(password, salt);
                if (passwordhash.equals(hashedClearText)) {
                    //There seems to be a match
                    return true;
                }
            }
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, e.getMessage(), e);
        }

        return false;
    }

    /**
     * Salts and hashes the password using PBKDF2
     *
     * @param password password to hash
     * @param salt     and the salt to use
     * @return the salted hash
     */
    static String hashPass(String password, String salt) {
        byte[] byteSalt = salt.getBytes();

        try {
            SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512");
            PBEKeySpec spec = new PBEKeySpec(password.toCharArray(), byteSalt, 10, 512);
            SecretKey key = factory.generateSecret(spec);
            byte[] hash = key.getEncoded();
            return byteToHexString(hash);

        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Converts a bytearray into a string
     * @param bytearray Bytearray to be converted
     * @return the converted bytearray in String format
     */
    private static String byteToHexString(byte[] bytearray) {
        StringBuilder sb = new StringBuilder();
        for (byte b : bytearray) {
            sb.append(String.format("%02X ", b));
        }
        return sb.toString().replaceAll("\\s", "");
    }

    /**
     * Randomly generates a salt (20 chars long) and returns it in string format
     * @return Salt returned in String format
     */
    static String generateSalt() {
        SecureRandom random = new SecureRandom();
        byte[] salt = new byte[20];
        random.nextBytes(salt);

        return byteToHexString(salt);
    }

    /**
     * For logging in server GUI output
     *
     * @param msg text to be output
     */
    public static void log(String msg) {
        serverController.log(msg);
    }



}
