package no.ntnu.imt3281.ludo.server;

import java.net.URL;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import org.json.JSONObject;

/**
 * Server FXML controller
 */
public class ServerController {

	@FXML
	private Label activeClients;

	@FXML
	private Label activeGames;

	@FXML
	private Label serverLoad;

	@FXML
	private Label ipPort;

	@FXML
	private TextArea console;

	/**
	 * stops the server
	 * @param event not used
	 */
	@FXML
	void stopServer(ActionEvent event) {
		Server.stopServer();
	}

	/**
	 * initializes the gui
	 */
	@FXML // This method is called by the FXMLLoader when initialization is complete
	void initialize() {
		this.activeClients.textProperty().set("0");
		this.activeGames.textProperty().set("0");
		this.serverLoad.textProperty().set("0.10%");
	}

	/**
	 * logs the output
	 * @param msg the message to log
	 */
	public void log(String msg) {
		Platform.runLater(()-> {
			this.console.setText(console.getText() + msg + "\n");
		});
	}
}
