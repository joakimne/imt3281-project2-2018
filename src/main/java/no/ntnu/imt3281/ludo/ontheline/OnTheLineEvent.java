package no.ntnu.imt3281.ludo.ontheline;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * Responsible to create and convert server response/requests
 * @author joakimellestad
 *
 */
public class OnTheLineEvent {
	
	private String id;
	private Boolean success;
	private JSONObject body;
	private String message;
	
	private static final Logger LOGGER = Logger.getLogger(OnTheLineEvent.class.getName());
	
	/**
	 * Whenever you receive something from a socket that originates either from the server or the client 
	 * you can initialize this class with it and get access to all its content
	 * @param receivedFromSocket - String the data received from socket
	 */
	public OnTheLineEvent(String receivedFromSocket) {		
		try {
			JSONObject incoming = new JSONObject(receivedFromSocket);
		
			try {
				this.id = incoming.getString("id");
			}catch(JSONException e) {
				LOGGER.log(Level.SEVERE, "Received no id: " + e.getMessage(), e);
			}
			//Can only get body if success is true
			try {
				this.body = incoming.getJSONObject("body");
			}catch(JSONException e) {
				LOGGER.log(Level.SEVERE, "Received no body: " + e.getMessage(),e);
			}
			try {
				this.success = incoming.getBoolean("success");
				this.message = incoming.getString("message");
			}catch(JSONException e) {
				LOGGER.log(Level.SEVERE, "Received no success or message: " + e.getMessage(),e);
			}
				
		}catch (JSONException e) {
			LOGGER.log(Level.WARNING, "Could not read id or body from incoming JSONObject:", e);
		}
	}
	
	/**
	 * Craft a request 
	 * call toString to get this in a on the line friendly way
	 * @param success - Boolean
	 * @param message -  String
	 * @param id - String
	 * @param body - List<Object>
	 */
	public OnTheLineEvent(Boolean success, String message, String id, List<Object> body) {
		
    	this.id = id;
    	
    	if(message != null) {
    		this.message = message;
    	}
    	if(success != null) {
    		this.success = success;
    	}
    	
    	if(body != null) {
    		JSONObject bodyRequest = new JSONObject();
        	// key,value,key,value
        	Iterator<Object> keys = body.iterator();
        	String key = "";
        	Object value = null;
        	
        	while(keys.hasNext()) {
        		key = (String) keys.next();
        		Object tmpObject = keys.next();
        		        		
        		if(tmpObject instanceof String) {
        			value = (String) tmpObject;
        		}else if(tmpObject instanceof Boolean) {
        			value = (Boolean) tmpObject;
        		}else if(tmpObject instanceof Integer) {
        			value = (Integer) tmpObject;
        		}else if (tmpObject instanceof JSONArray) {
					value = (JSONArray) tmpObject;
        		}else {
        			value = "";
        		}
        		bodyRequest.put(key, value);
        	}
        	LOGGER.log(Level.INFO, "Created OnTheLine object: " + bodyRequest.toString(1));
        	this.body = bodyRequest;
    	}
    	
	}
	/**
	 * Crafts a onthelinevent for when you already have the body content.
	 * The body is added to a body object, so only provide the content of a body
	 * @param success - the status for a response (Do not use for requess)
	 * @param message - message for a response (Do not use for requess)
	 * @param id - the id for the request
	 * @param body - the content inside the body as JSONObject
	 */
	public OnTheLineEvent(Boolean success, String message, String id, JSONObject body) {
    	this.id = id;
    	
    	if(message != null) {
    		this.message = message;
    	}
    	if(success != null) {
    		this.success = success;
    	}
    	if(body != null) {
    		this.body = body;
    	}
	}
	
	/**
	 * Creates a empty OnTheLineEvent
	 * Later you can set the values you want
	 * 
	 * setid
	 * setbody
	 * setsuccess
	 * setmessage
	 * 
	 * @param requestid
	 */
	public OnTheLineEvent() {
		
	}
	
	/**
	 * Craft a response for when you only want to send a success status and a message
	 * @param success - Boolean
	 * @param message - String
	 * @param id -String
	 */
	public OnTheLineEvent(Boolean success, String message, String id) {
		this.id = id;
    	
    	if(message != null) {
    		this.message = message;
    	}
    	if(success != null) {
    		this.success = success;
    	}
	}
	/**
	 * Sets the request/response id for this OnTheLineEvent
	 * @param id String
	 */
	public void setid(String id) {
		this.id = id;
	}
	/**
	 * Sets the response success status for this OnTheLineEvent
	 * @param success - Boolean
	 */
	public void setSuccess(Boolean success) {
		this.success = success;
	}
	/**
	 * Sets the request/response body for this OnTheLineEvent
	 * @param body - JSONObject
	 */
	public void setbody(JSONObject body) {
		this.body = body;
	}
	/**
	 * Sets the request/response message for this OnTheLineEvent
	 * @param message - String
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	
	
	
	@Override
	public String toString() {
		JSONObject sending = new JSONObject();
		
		if(this.id != null) {
			sending.put("id", this.id);
		}
		
		//If this is a response
		//We often have success and message too
		
		if(this.success != null) {
			sending.put("success", this.success);
		}
		if(this.message != null) {
			sending.put("message", this.message);
		}
		if(this.body != null) {
			sending.put("body", this.body);
		}
		
		return sending.toString();
	}
	/**
	 * get the id for this response
	 * The id should always exists
	 * @return
	 */
	public String getId() {
		return this.id;
	}
	/**
	 * Get the success status of this response
	 * @return
	 */
	public Boolean getSuccess() {
		return this.success;
	}
	

	/**
	 * If a success message is false
	 * there should exists a message with it
	 * This can get that message
	 * @return
	 */
	public String getErrorMessage() {
		if(this.message != null) {
			return this.message;
		}
		return "No error message";
	}
	
	/**
	 * Get the token that resides in the response
	 * The token is only available for responses from a authorize
	 * If you attempt to read a token that the body does not have we throw Exception
	 * @return String - The token for the user
	 * @throws Exception on failure
	 */
	public String getToken() throws Exception{
		
		try {
			String token = this.body.getString("token");
			return token;
		}catch (JSONException e) {
			throw new Exception("Could not get token. Are you sure this is a authorize response?");
		}
		
	}
	/**
	 * Gets the body of the request or response
	 * @return JSOBObject - the body of the request or response
	 */
	public JSONObject getBody() {
		return this.body;
	}
	
	
	/**
	 * ###################
	 * Methods below are specific for OnTheLineEvents that contains a gameid
	 * This is for the methods targeted towards a Ludogame
	 * ###################
	 */
	
	/**
	 * Tries  to retrieve  game
	 * @return
	 */
	public String getGameId() throws Exception{
		
		try {
			String gameid = this.body.getString("gameid");
			return gameid;
		}catch(JSONException e) {
			throw new Exception("Could not get gameid. This object might not bew game specific");
		}
		
	}
	
	
	
	
	
	/**
	 * ################### 
	 * Functions below are specifically meant to be used by ChatRoomListener 
	 * if you use these methods and the body does not contain chat room stuff you are out of luck.
	 * ###################
	 */
	/**
	 * If you are sure this objects body has a chatroom id you can use this function
	 * if you are not correct it will throw that in your face
	 * @return
	 */
	public String getChatRoomId() throws Exception{
		try{
			String chatroomid = this.body.getString("chatroomid");
			return chatroomid;
		}catch(JSONException e) {
			throw new Exception("Could not get chatroomid. The body is not chatroom specific ");
		}
		
	}
	/**
	 * Only used this if this event's body contains a Message
	 * Get the message from the chatroom event
	 * @return String - the message from the event
	 * @throws Exception
	 */
	public String getChatRoomMessage() throws Exception{
		try{
			String message = this.body.getString("message");
			return message;
		}catch(JSONException e) {
			throw new Exception("Could not get message. The body is not chatroom specific ");
		}
		
	}
	/**
	 * Only used this if this event's body contains a Message
	 * Get the publisher of this message
	 * @return String
	 * @throws Exception
	 */
	public String getChatRoomPublisher() throws Exception {
		try{
			String publisher = this.body.getString("publisher");
			return publisher;
		}catch(JSONException e) {
			throw new Exception("Could not get publisher. The body is not chatroom specific ");
		}
	}
	/**
	 * Only used this if this event's body contains a Message
	 * Get the published time of the message. 
	 * @return String
	 * @throws Exception
	 */
	public String getPublishedTime() throws Exception {
		try{
			String publishetime = this.body.getString("publishedtime");
			return publishetime;
		}catch(JSONException e) {
			throw new Exception("Could not get publishedtime. The body is not chatroom specific ");
		}
	}
	
	/**
	 * Response #12
	 * Can only be called if the body has game list
	 * @return
	 */
	public List<String> getGameRooms(){
		JSONArray gamerooms = this.body.getJSONArray("games");
		
		ArrayList<String> gameroomidList = new ArrayList<>();
		
		Iterator<Object> gameroomsList =  gamerooms.iterator();
		
		while(gameroomsList.hasNext()) {
			JSONObject gameroom = (JSONObject) gameroomsList.next();
			String gameid = gameroom.getString("gameid");
			gameroomidList.add(gameid);
		}
		return gameroomidList;
		
		
	}
	
	/**
	 * Response #13 
	 * Can only be called if the body has chatrooms list
	 * @return List<string> the chatrooms in the body
	 */
	public List<String> getChatRooms(){
		JSONArray chatrooms = this.body.getJSONArray("chatrooms");
		
		ArrayList<String> chatroomidList = new ArrayList<>();
		
		Iterator<Object> chatroomsList =  chatrooms.iterator();
		
		while(chatroomsList.hasNext()) {
			JSONObject chatroom = (JSONObject) chatroomsList.next();
			String chatroomid = chatroom.getString("chatroomid");
			chatroomidList.add(chatroomid);
		}
		return chatroomidList;
		
		
	}
	
	/**
	 * Can only be used by a type 11 response
	 * @return String - returns the username
	 */
	public String getUsername() {
		String username = this.body.getString("username");
		return username;
	}
	
	
	/**
	 * # 6 response
	 * @return JSONArray containing [{"username":"user1","color":"0"},{...}]
	 */
	public JSONArray getExistingUsersInLudo(){		
		return this.body.getJSONArray("existingusers");
	}
	
	/**
	 * #16 response
	 * Gets the inviter from an invite response.
	 * @return String 
	 */
	public String getInviter() {
		String inviter = this.body.getString("inviter");
		return inviter;
	}
	
	/**
	 * #17 response part 1
	 * Gets the numberofgames JSONArray from the statistics response.
	 * @return String
	 */
	public String getTopGames() {
		JSONArray players = this.body.getJSONArray("numberofgames");
		
		String playerGames = "";
		
		Iterator<Object> playerIterator =  players.iterator();
		
		String playerResult = String.format("%-5s%-30s%8s\n", "Rank", "Name", "Games won:");
		while(playerIterator.hasNext()) {
			JSONObject player = (JSONObject) playerIterator.next();
			Integer playerRank = player.getInt("rank");
			String playerName = player.getString("username");
			String playerNumber = player.getString("number");
			String playerInfo = String.format("%-5d%-30s%8s\n", playerRank, playerName, playerNumber);
			
			playerGames += playerInfo;
		}
		playerResult += playerGames;
		
		return playerResult;
	}
	
	/**
	 * #17 response part 2
	 * Gets the numberofwins JSONArray from the statistics response.
	 * @return String
	 */
	public String getTopWins() {
		JSONArray players = this.body.getJSONArray("numberofgames");
		
		String playerWins = "";
		
		Iterator<Object> playerIterator =  players.iterator();
		
		String playerResult = String.format("%-5s%-30s%8s\n", "Rank", "Name", "Games won:");
		while(playerIterator.hasNext()) {
			JSONObject player = (JSONObject) playerIterator.next();
			Integer playerRank = player.getInt("rank");
			String playerName = player.getString("username");
			String playerNumber = player.getString("number");
			String playerInfo = String.format("%-5d%-30s%8s\n", playerRank, playerName, playerNumber);
			
			playerWins += playerInfo;
		}
		playerResult += playerWins;
		
		return playerResult;
	}
}
