package no.ntnu.imt3281.ludo.ontheline;

import no.ntnu.imt3281.chat.ChatEvent;

/**
 * This is to be implemented by the ChatRoomController in order to receive responses form the server
 * @author joakimellestad
 *
 */
public interface ChatRoomEventListener {
	/**
	 * Whenever a new message is sent
	 * @param event describes the event
	 */
	public void newChatMessageEvent(ChatEvent event);

	/**
	 * Whenever a new user joins the chat, or one leaves
	 * @param event describes the event
	 */
	public void subscribersInChatRoomChanged(ChatEvent event);

	/**
	 * adds a message to the chat informing users that a new user joined
	 * @param event describes the event
	 */
	public void joinChatRoomResponse(OnTheLineEvent event);//On the client this is handled by the LudResponseListener- its a mess
}
