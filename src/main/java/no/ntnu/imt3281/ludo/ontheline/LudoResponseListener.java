package no.ntnu.imt3281.ludo.ontheline;

/**
 * This interface is to be inplemented by the LudoController class in order to receive 
 * responses made form the server
 * 1, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14
 * @author joakimellestad
 */
public interface LudoResponseListener {

    /**
     * 1 Register a new user
     * @param event the event that occurred
     */
    public void registrationResponse(OnTheLineEvent event);

    /**
     * 6 Join a game
     * @param event the event that occurred
     */
    public void joinGameResponse(OnTheLineEvent event);

    /**
     * 7 Log out
     * @param event the event that occurred
     */
    public void logOutResponse(OnTheLineEvent event);

    /**
     * 8 Log in
     * @param event the event that occurred
     */
    public void logInResponse(OnTheLineEvent event);

    /**
     * 9 Error
     * @param event the event that occurred
     */
    public void errResponse(OnTheLineEvent event);

    /**
     * 11 Authorize
     * @param event the event that occurred
     */
    public void authorizeResponse(OnTheLineEvent event);
    
    /**
     * 12 List available games
     * @param event the event that occurred
     */
    public void availableGamesResponse(OnTheLineEvent event);
    
    /**
     * 13 List chats
     * @param event the event that occurred
     */
    public void availableChatResponse(OnTheLineEvent event);

    /**
     * 14 Join chat
     * @param event the event that occurred
     */
    public void joinChatResponse(OnTheLineEvent event);

    /**
     * 16 invite response
     * @param event the event that occurred
     */
    public void inviteResponse(OnTheLineEvent event);
    
    /**
     * 17 Statistics response
     * @param event the event that occurred
     */
	public void statisticsResponse(OnTheLineEvent event);

	

	

}
