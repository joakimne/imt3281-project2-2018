package no.ntnu.imt3281.chat;

import no.ntnu.imt3281.ludo.server.Server;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A ChatEvent holds information about what is needed when passing chat data to the user
 * It super EvenObject is a Message object and contains the actual message that should be passwed to the user
 * A local variable chatroomid contains the id of the chatroom so this message can be dispatched to the correct
 * chatroom on the client side.
 * 
 * 
 * @author joakimellestad
 *
 */
public class ChatEvent{

	private String chatroomId;
	private JSONObject event;
	private String username;
	private Logger applogger = Logger.getLogger(Server.class.getName());

	/**
	 * When you want to craft a ChatEvent that holds a message
	 * @param message message added to object variable
	 * @param chatRoomId chatroomid to be added to object variable
	 */
	public ChatEvent(Message message, String chatRoomId) {
		this.event = message.toJSON();
		this.chatroomId = chatRoomId;
	}
	/**
	 * A unspecific initializer that takes a JSONObject
	 * The json object should be the body of a request
	 * @param event event - is the body with the contents
	 * @param chatRoomId -This is often used by the server side, but the client side probably don't want to use this
	 * It should be safe to pass in an empty string
	 */
	public ChatEvent(JSONObject event, String chatRoomId) {
		this.event = event;
		this.chatroomId = chatRoomId;
	}
	
	/**
	 * Use this when you receive a OnTheLineEvent and you know the body of it is
	 * a ChatEvent.
	 * Later the contents can be retrieved by ChatEvent speicifc functions for what you want.
	 * Remember to use only the function that is relevant for this event.
	 * The functions should be marked with number that relates to CommunicationId
	 * @param event - The body of the OnTheLineEvent received
	 * @param fromuser specifies the user that made the request.
	 * @param donotuse - The two initializer here was equal but not equal so created another parameter to have difference, but no difference
	 */
	public ChatEvent(JSONObject event, String fromuser, String donotuse) {
		this.chatroomId = (String) event.remove("chatroomid");
		//After removing the chatroomid we know the rest should
		//be whatever content this was crafter from
		this.event = event;
		this.username = fromuser;
	}
	
	/**
	 * When client receive a ChatEvent 
	 * When you receive a OnTheLineRequest on the client and just need a quick and dirty way ti get a ChatEvent object
	 * The initializer will try to extract the chatroomid, though this might not be present in all responses.
	 * But hopefully it is.
	 * @param body - the body of the OnTheLineEvent
	 */
	public ChatEvent(JSONObject body) {
		try {
			this.chatroomId = body.getString("chatroomid");
		}catch(JSONException e) {
			applogger.log(Level.WARNING, e.getMessage());
		}
		this.event = body;
	}
	
	public String getChatroomId() {
		return this.chatroomId;
	}

	
	/**
	 * Provides you with the event and also contains the chatroomid
	 * @return
	 */
	public JSONObject getEvent() {
		this.event.put("chatroomid", this.chatroomId);
		return this.event;
	}

	/**
	 * ################### 
	 * If you want to use functions below you should be sure that
	 * the event you are using it from has the content
	 * if not I am gonna kick your ass with a Exception
	 * ####################
	 */
	
	/**
	 * if you know the event is of a response from
	 * 15 then you can safely use this function
	 * For getting the users in a chatroom
	 * @return - returns the users/usernames in a list
	 */
	public List<String> getChatRoomUsers(){
		JSONArray activeusers = this.event.getJSONArray("activeusers");
		Iterator<Object> usernames = activeusers.iterator();
		List<String> result = new ArrayList<>();
		while(usernames.hasNext()) {
			Object next = usernames.next();
			if(next instanceof String) {
				result.add((String)next);
			}
		}
		return result;
	}
 
	
	
	/**
	 * if you know the event is of type response from
	 * 4 - New chat message you can use this function
	 * Use this to get this event message
	 * @return Message object containing the message
	 */
	public Message getChatRoomNewMessage() {
		String message = this.event.getString("message");
		return new Message(message, this.username);
	}
	
}
