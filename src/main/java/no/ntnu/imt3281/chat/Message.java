package no.ntnu.imt3281.chat;

import java.time.LocalTime;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONException;
import org.json.JSONObject;



/**
 * This is one instance of a message
 * It contains information about time, message, user etc.
 * 
 * @author joakimellestad
 *
 */
public class Message{
	private String message;
	private String publishedTime;
	private String publishername;
	
	private static final Logger LOGGER = Logger.getLogger(Message.class.getName());
	/**
	 * The one and only initializer
	 * A message doesn't change. Once it's initalized theres no way back.
	 * 
	 * @param message message to be sent
	 * @param publishername whom sent the message
	 */
	public Message(String message, String publishername) {
		LocalTime now = LocalTime.now();
		this.message = message;
		this.publishedTime = now.toString();
		this.publishername = publishername;
	}
	
	/**
	 * When you get a new chat message you can use this and 
	 * pass in the json data that is in the body
	 * @param message message data to be sent (message, publisher and publishedtime)
	 */
	public Message(JSONObject message) {
		try {
		this.message = message.getString("message");
		this.publishername = message.getString("publisher");
		this.publishedTime = message.getString("publishedtime");
		}catch(JSONException e) {
			LOGGER.log(Level.WARNING, "You probably did not receive a chat message" + e.getMessage(), e);
		}
	}
	
	/**
	 * Gets the message content for this message
	 * @return message which is sent
	 */
	public String getMessage() {
		return this.message;
	}

	/**
	 * Gets the published time for this message
	 * Remember this time is the time published by the user, not the server
	 * If the user is in another timezone the time from users might differ from that of the server
	 * So don't rely time, we should probably set the time according to the server clock....
	 * @return the published time
	 */
	public String getpublishedTime() {
		return this.publishedTime;
	}

	/**
	 * Gets the creator behind this fantastic message
	 * We don't have a direct reference to the user, 
	 * but the username can be used to dig up more about the user using some hashmap on the server
	 * @return the publisher name
	 */
	public String getPublisher() {
		return this.publishername;
	}

	/**
	 * Takes message, publishedtime and publisher from this object and returns an appropriate
	 * format ready to be sent to the user in a body object.
	 * Returns a result object with message info
	 * @return JSONObject - add this to a body in a OnTheLineEvent
	 */
	public JSONObject toJSON() {
		JSONObject messageData = new JSONObject();
		messageData.put("message", this.getMessage());
		messageData.put("publisher", this.getPublisher());
		messageData.put("publishedtime", this.getpublishedTime());
		return messageData;
	}
}
