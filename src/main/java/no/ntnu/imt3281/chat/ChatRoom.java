package no.ntnu.imt3281.chat;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Formatter;
import java.util.FormatterClosedException;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.JSONObject;

import no.ntnu.imt3281.ludo.global.CommunicationID;
import no.ntnu.imt3281.ludo.ontheline.ChatRoomEventListener;
import no.ntnu.imt3281.ludo.ontheline.OnTheLineEvent;
import no.ntnu.imt3281.ludo.server.Config;
import no.ntnu.imt3281.ludo.server.User;

/**
 * A chatroom is one instance of a chat
 * Users subscribe to a chat in order to get messages
 * Users send messages to a chat in order to rach out to subscirbers of a chat
 * 
 * 
 * 
 * @author joakimellestad
 *
 */
public class ChatRoom {

	private String chatroomid;
	private String chatroomname;
	/**
	 * If the chatroom is part of the game it should not be listed as available chat to be joined into
	 * It should in theory also be limited to 4 participants. 
	 * But since only the participants in a game will have the chatroom id I believe it is 
	 * safe enough to not add checking when adding users.
	 */
	private Boolean isPartOfGame = false;
	/**
	 * Messages are added as they come in, but when you first take one out it is gone
	 * FIFO 
	 */
	private final LinkedBlockingQueue<Message> messages = new LinkedBlockingQueue<>();
	
	/**
	 * Holds a reference to the users that wish to receive updates
	 * The index is the username for the user. The other part is the User class
	 */
	private ArrayList<ChatRoomEventListener> usersThatWantMessages = new ArrayList<ChatRoomEventListener>();
	
	private static final Logger LOGGER = Logger.getLogger(ChatRoom.class.getName());
	
	
	
	Formatter logFile;
	
	/**
	 * Create a new chatroom when you already have the unique key needed
	 * for this chatroom
	 * Mostly used for games because we can reuse its gameid for chatroomid
	 * @param uid identifies user
	 */
	public ChatRoom(String uid) {		
		// Creates a text file used for logging
		this.chatroomid = uid;
		this.createLogDirectory();
		this.setFunnyChatroomName();
	}
	
	/**
	 * Initialize a new chatroom - This is the preferred way
	 * Uses UUID to generate a random id for this instance.
	 * It's id is used to identify different chatrooms
	 * You might users might need to post to different chatrooms and the users have to know an id to post to.
	 * A nickname is the fun name of this chatroom. This does not have to be unique, that is not something that is enforced by me at least....
	 * 
	 * Creates a file used for logging
	 *
	 */
	public ChatRoom() {
		this.chatroomid = UUID.randomUUID().toString();
		this.createLogDirectory();
		this.setFunnyChatroomName();
		LOGGER.log(Level.INFO, "Created chatroom "  + ", uid: " + this.chatroomid);
	}
	
	/**
	 * Creates a directory used for loggin
	 * Will also create the log file 
	 * The logfile name will be the chatroomid of this. (UUID random)
	 */
	private void createLogDirectory() {
		try{
			//The root folder for project
			String projectfolder = new File(".").getCanonicalPath();
			File logdirectory = new File(projectfolder +  Config.CHATROOMLOGGINGDIRECTORY);
			if(logdirectory.exists()) {
				//Directory already exists -> good to go
				LOGGER.log(Level.INFO, "Chatroom log directory already exists");
				this.createLogFile(logdirectory.toString());
			}else if(logdirectory.mkdir()) {
				LOGGER.log(Level.INFO, "Created chatroom log directoryt");
				this.createLogFile(logdirectory.toString());
			}else {
				LOGGER.log(Level.WARNING, "Failecd creating log directory for chatroom");
			}			
			
		}catch(SecurityException | FormatterClosedException | IOException e) {
			LOGGER.log(Level.WARNING, "Chatroom logfile could not be created: " + e.getMessage(), e);
		}
	}
	
	private void createLogFile(String directory) {
		String logfilename = directory + "/" + this.chatroomid + ".txt";
		try {
		Boolean isnewfile = new File(logfilename).createNewFile();
		if(isnewfile) {
			this.logFile = new Formatter(logfilename);
		}
		}catch(SecurityException | FormatterClosedException | IOException e) {
			LOGGER.log(Level.INFO, "Failed creating logfile: " + e.getMessage(), e);
		}
	}
	
	/**
	 * gets the id of the chatroom
	 * @return the chatroomid
	 */
	public String getChatRoomId() {
		return this.chatroomid;
	}
	/**
	 * Gets the name of the chatroom
	 * @return the chatroomname
	 */
	public String getChatroomName() {
		return this.chatroomname;
	}

	/**
	 * sets the chatroomname
	 * @param name name to set
	 */
	public void setChatroomName(String name) {
		this.chatroomname = name;
	}
	/**
	 * Set the variable isPartOfGame to the parameter passed in
	 * When the chat is part of a game it will not be listed on the client side as something that can be joined
	 * into.
	 * @param isPartOf sets if it can be joined, boolean
	 */
	public void setIsPartOfGame(Boolean isPartOf) {
		this.isPartOfGame = isPartOf;
	}
	/**
	 * Gets the true or false if this chat is part of a game
	 * @return ispartofgame variable
	 */
	public Boolean getIsPartOfGame() {
		return this.isPartOfGame;
	}
	/**
	 * Add a user as a subscriber to this chatroom
	 * The user will receive new messages as long as the socket persists
	 * Also send out a new subscirbers changed event
	 * @param subscriber the chatroom listener
	 */
	public void addSubscriber(ChatRoomEventListener subscriber) {
		//New user added. Add a message to let subscirbers know
		if(this.usersThatWantMessages.contains(subscriber)) {
			LOGGER.log(Level.INFO, "Naughty user...tried to subscribe to a chatroom already subscirbed to");
			OnTheLineEvent failure = new OnTheLineEvent(false, "User is already apart of: " + this.getChatroomName(), CommunicationID.JOINCHATROOM);
			subscriber.joinChatRoomResponse(failure);
			return;
		}
		
		String username = ((User) subscriber).getUsername();
		Message newSubscriberAlert = new Message("New user joined chat: ", username);
		try {
			this.newMessage(newSubscriberAlert);
		}catch (Exception e) {
			this.LOGGER.log(Level.WARNING, "Could not add subscriber " + e.getMessage(), e);
		}
		//Subscriber is not suppose to get his own user added message..or?
		this.usersThatWantMessages.add(subscriber);
		this.chatroomUsersChangedSend();
		
		//The user doesnt need a chatroom response when this is part of game
		if(!this.isPartOfGame) {
	        OnTheLineEvent response = new OnTheLineEvent(true, "Joined chat successfully", CommunicationID.JOINCHATROOM, Arrays.asList("chatroomid", this.getChatRoomId(),"chatroomname",this.getChatroomName()));
	        subscriber.joinChatRoomResponse(response);
		}
        
	
	
	}
	/**
	 * Unsubscribes a user from the chatroom
	 * @param unsubscriber the chatroomlistener
	 */
	public void unsubscribe(ChatRoomEventListener unsubscriber) {
		
		String username = ((User) unsubscriber).getUsername();
		Message unsubscribeAlert = new Message("User left the game: ", username);
		try {
			this.newMessage(unsubscribeAlert);
		}catch (Exception e) {
			this.LOGGER.log(Level.WARNING, "Could not unsubscribe user: " + e.getMessage(), e);
		}
		this.chatroomUsersChangedSend();
		this.usersThatWantMessages.remove(unsubscriber);
	}
	
	/**
	 * used to add a new message to the chatroom
	 * A message is first added to the LinkedBlockingQueue
	 * It is logged to file
	 * Then it is distributed to subscribers
	 * 
	 * if logging the message to file fails, the message is NOT added to the queue for distribution
	 * 
	 * @param msg the message
	 * @throws when it cant log to file
	 */
	public void newMessage(Message msg) throws Exception{
		
		if(this.log(msg)) {
			try {
				this.messages.add(msg);
			}catch(IllegalStateException e) {
				LOGGER.log(Level.INFO, "Failed to add new message: " + e.getMessage(), e);
				throw new Exception("Could not log message to file:" + e.getMessage());
				
			}
		}else {
			throw new Exception("Logging message failed. Message is not added to queue");
		}
		
	}
	
	/**
	 * Takes the messages in the queue using poll which doesn't block.
	 * adds them to a subscriber/user output queue
	 * 
	 */
	public void sendMessage() {
		final Message msg = this.messages.poll();
		if(msg != null) {		
			ChatEvent newChatEvent = new ChatEvent(msg, this.chatroomid);
			this.usersThatWantMessages.forEach(subscriber->subscriber.newChatMessageEvent(newChatEvent));
		}	
	}
	
	/**
	 * gets the subscribers to this chatroom 
	 * @return the username
	 */
	public JSONArray getSubscribersAsUsername() {
		JSONArray usernames = new JSONArray();
		for(ChatRoomEventListener user :  this.usersThatWantMessages) {
			usernames.put(((User) user).getUsername());
		}
		return usernames;
	}
	
	/**
	 * Takes a message and logs it to file for persistent storage
	 * Messages are currently logged as time, user, message
	 * If we can't log message we should not distribute the message
	 * @param log message to log
	 * @return either true or false depending on success
	 */
	private Boolean log(Message log) {
		LOGGER.log(Level.INFO, "Writing message to permanent storage");
		try {
			this.logFile.format("%s %s %s", log.getpublishedTime(), log.getPublisher(), log.getMessage());
			this.logFile.flush();
			return true;
		}catch(NoSuchElementException elementException) {
			LOGGER.log(Level.WARNING, "Could not write messag to disk: " + elementException.getMessage(), elementException);
		}
		return false;
	}

	/**
	 * shuts down the chatroom
	 * @return either true or false depending on success
	 */
	public Boolean shutdown() {
		LOGGER.log(Level.INFO, "Shutting down chatroom: " + this.chatroomid);
		try {
			this.logFile.flush();
			this.logFile.close();
			return true;
		}catch(NoSuchElementException | FormatterClosedException e) {
			LOGGER.log(Level.INFO, "Attempted to flush and close file but failed: " + e.getMessage(), e);
		}
		return false;
	}

	/**
	 *  updates the chatroom subscriber list
	 * @param user the chatroomlistener for the user
	 */
	public void publishChatRoomUsers(ChatRoomEventListener user) {
		JSONArray subscribers = this.getSubscribersAsUsername();
		JSONObject body = new JSONObject();
		body.put("activeusers", subscribers);
		ChatEvent users = new ChatEvent(body, this.chatroomid);
		user.subscribersInChatRoomChanged(users);
		
	}

	/**
	 * Sends the user list to all 
	 * subscribers in this chatroom
	 */
	public void chatroomUsersChangedSend() {
		JSONArray subscribers = this.getSubscribersAsUsername();
		JSONObject body = new JSONObject();
		body.put("activeusers", subscribers);
		ChatEvent users = new ChatEvent(body, this.chatroomid);
		for(ChatRoomEventListener user :  this.usersThatWantMessages) {
			user.subscribersInChatRoomChanged(users);
		}
	}
	
	
	
	
	
	/*
	 * NOTHING TO SEE HERE 
	 * 
	 */
	
	/**
	 * A super secret function to set a funny chatroom name
	 * 
	 */
	private void setFunnyChatroomName() {
		Random n = new Random();
		int r = n.nextInt(5) +1 ;
		int r2 = n.nextInt(5)+1;
		StringBuilder funny = new StringBuilder();
		if(r < first.length) {
			funny.append(first[r]);
		}
		if(r2 < second.length) {
			funny.append(" " + second[r2]);
		}
		this.chatroomname = funny.toString();
	}
	
	String [] first = {
			"cold","fart","ass", "yellow","awesome","crap"
	};
	String [] second = {
		"Ludo","horse","donkey","monkey","Java","Winston"	
	};
	
	
}
