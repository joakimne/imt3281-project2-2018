CREATE TABLE USERSTATS (
  id int PRIMARY KEY NOT NULL GENERATED ALWAYS AS IDENTITY,
  gametime TIMESTAMP NOT NULL,
  gamepoints int NOT NULL,
  userid int NOT NULL,
  FOREIGN KEY (userid) REFERENCES LUDOUSER(userid) ON DELETE CASCADE)
