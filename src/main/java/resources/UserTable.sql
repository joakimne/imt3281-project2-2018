CREATE TABLE LUDOUSER (
  userid int PRIMARY KEY NOT NULL GENERATED ALWAYS AS IDENTITY,
  username varchar(100) UNIQUE NOT NULL,
  password varchar(300) NOT NULL,
  salt varchar(600) NOT NULL,
  token varchar(600),
  email varchar(100) UNIQUE NOT NULL)
