//package server;
//
//import static org.junit.Assert.assertEquals;
//
//import java.sql.ResultSet;
//import java.util.Random;
//
//import org.json.JSONArray;
//import org.json.JSONObject;
//import org.junit.Before;
//import org.junit.Test;
//
//import no.ntnu.imt3281.ludo.server.Database;
//
///**
// * This is a test class for the Database class
// * 
// * Testing of the database involves 
// * 1. Create the db - no assert
// * 2. Create schema - no assert
// * 3. Insert known data - no assert
// * 4. Select known data - assert equal
// * 
// * Don't know how we should test 1 -> 3
// * @author joakimellestad
// *
// */
//public class DatabaseTest {
//
//	private static Boolean setupdone = false;
//
//	public DatabaseTest() {
//		// TODO Auto-generated constructor stub		
//	}
//	
//	@Before
//	public void setup() {
//		if(this.setupdone) {
//			return;
//		}
//		Database.createDB();
//		Database.createSchema();
//		this.setupdone = true;
//	}
//	
//	/*
//	@Test
//	public void createDBTest() {
//		Database.createDB();
//	}
//	
//	@Test
//	public void createSchemaTest() {
//		Database.createSchema();
//	}*/
//	
//	/**
//	 * Adds new user then tries to get it and verify values
//	 */
//	@Test
//	public void addNewuserTest() {
//		String username = "Steve_Wozniak";
//		String passHash = "aHR0cHM6Ly93d3cueW91dHViZS5jb20vd2F0Y2g/dj1kUXc0dzlXZ1hjUQo=";
//		String email = "stiv_wozniak@vivaldi.net";
//		String salt = "aff43a43ge";
//		int newUserId;
//		try {
//			newUserId = Database.addNewUser(username, passHash, email, salt);
//			
//			try {
//				JSONObject userDataForUser = Database.getUserDataForUser(username);
//				
//				assertEquals(userDataForUser.getString("username"), username);
//				assertEquals(userDataForUser.getString("email"), email);
//				assertEquals(userDataForUser.getString("password"), passHash);
//				
//			}catch(Exception e) {
//				new AssertionError();
//			}
//			
//		}catch(Exception e) {
//			new AssertionError();
//		}	
//	}
//	
//	@Test
//	public void isEmailTakenTest() {
//		String email = "stiv_wozniak@vivaldi.net";
//		
//		try {
//			
//			Boolean isEmailTaken = Database.isEmailTaken(email);
//			assertEquals(isEmailTaken, false);
//			
//			if(this.addUser("Wozniak", "hashashash", email, "1234")) {
//				Boolean isEmailTakenAgain = Database.isEmailTaken(email);
//				assertEquals(isEmailTakenAgain, true);
//			}
//			
//		}catch(Exception e) {
//			new AssertionError();
//			System.out.println(e.getMessage());
//		}
//	}
//	
//	@Test
//	public void isUsernameTaken() {
//		String username = "Star Lord";
//		try {
//			Boolean isUsernameTaken = Database.isUsernameTaken(username);
//			assertEquals(isUsernameTaken, false);
//			
//			if(this.addUser(username, "somehash", "Star@Lord.Nova", "1234")) {
//				Boolean isusernameTakenAgain = Database.isUsernameTaken(username);
//				assertEquals(isusernameTakenAgain, true);
//			}
//			
//		}catch(Exception e) {
//			new AssertionError();
//		}
//	}
//	
//	
//	@Test
//	public void adduserStatsTest() {
//		
//		this.addUser("Nikolai", "hashash", "niko@email.com", "random");
//		this.addUser("Joakim", "hash", "joakim@email.com", "rand");
//		this.addUser("Bendik", "hashha", "bendik@email.com", "rand");
//		this.addUser("magnus", "hahshah", "magnull@stud.ntnu.no", "salt");
//		
//		
//		for(int i = 0; i < 50; i++) {
//			//Add userstats
//			Random n = new Random();
//			int r = n.nextInt(4)+1;
//			try {
//				Database.setLudoResult(10, r);
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//	}
//	
//	
//	@Test
//	public void getTopTenMostPoints() {
//		JSONArray res;
//		try {
//			res = Database.getTopTenMostPoints();
//			System.out.println(res.toString());;
//		} catch (NullPointerException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//		
//		
//	}
//	
//	@Test
//	public void getTopTenNGames() {
//		JSONArray res;
//		try {
//			res = Database.getTopTenMostActivePlayers();
//			System.out.println(res.toString());
//		} catch (NullPointerException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//	}
//	
//	private Boolean addUser(String username, String passwordHash, String email, String salt) {
//		try {
//			int newuser = Database.addNewUser(username, passwordHash, email, salt);
//			if(newuser > 0) {
//				System.out.println("New user added: " + newuser + " : " + username);
//				return true;
//			}
//			return false;
//		}catch(Exception e) {
//			new AssertionError();
//			System.out.println(e.getMessage());
//		}
//		return false;
//	}
//	
//	
//	
//	
//	
//}
