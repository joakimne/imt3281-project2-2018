//package server;
//
//import static org.junit.Assert.*;
//
//import java.io.BufferedReader;
//import java.io.BufferedWriter;
//import java.io.DataInputStream;
//import java.io.DataOutputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.InputStreamReader;
//import java.io.OutputStream;
//import java.io.OutputStreamWriter;
//import java.io.Writer;
//import java.net.Socket;
//import java.net.UnknownHostException;
//import java.util.Arrays;
//import java.util.List;
//
//import no.ntnu.imt3281.ludo.server.Server;
//
//import org.json.JSONArray;
//import org.json.JSONObject;
//import org.junit.Before;
//import org.junit.Test;
//
//
//import no.ntnu.imt3281.ludo.server.Config;
//
//public class ServerTest {
//
//	String userToken;
//	String gameid;
//	
//	String username = "hawkeye69";
//	String password = "TonyStark111";
//	String email = "hawkeye@marvelmail.co";
//	
//	
//	
//	
//	String rollTheDice = "{"
//			+ "id:2,"
//			+ "body:{"
//			+ "token:,"
//			+ "gameid:,"
//			+ "}"
//			+ "}";
//	
//	Socket server;
//	BufferedWriter serverWriter;
//	BufferedReader serverReader;
//	
//	JSONObject generateJSONDefault() {
//
//		JSONObject req = new JSONObject();
//		req.put("id", "1");
//		JSONObject body = new JSONObject();
//		body.put("email", "Magnus@serIkkePåSerier.com");
//		body.put("username", "ikkeBraMagnus");
//		body.put("password", "Magnus123");
//		req.put("body", body);
//		return req;
//	}
//	
//	@Before
//	public void setUpConnection() {
//		try {
//			server = new Socket(Config.SERVERNAME, Config.SERVERPORT);
//			serverWriter = new BufferedWriter(new OutputStreamWriter(server.getOutputStream()));
//			serverReader = new BufferedReader(new InputStreamReader(server.getInputStream()));
//			
//		} catch (UnknownHostException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
//	
//	@Before
//	public void createUserTest(){
//		String newUser = "{"
//				+ "id:1,"
//				+ "body:{"
//				+ "email:" + this.email + ","
//				+ "username:" + this.username + ","
//				+ "password:" + this.password + "}"
//				+ "}"; 
//		try {
//		serverWriter.write(newUser);
//		serverWriter.newLine();
//		serverWriter.flush();
//		
//		String answer = serverReader.readLine();
//		
//		JSONObject req = new JSONObject(answer);
//
//		boolean success = req.getBoolean("success");
//		String message = req.getString("message");
//		System.out.println(message);
//		assertEquals(success, true);
//			
//		JSONObject result = req.getJSONObject("result");
//		userToken = result.getString("token");
//		System.out.println("Received token: " + userToken);
//		
//		
//		}catch(IOException ioe) {
//			ioe.printStackTrace();
//		}		
//			
//	}
//	
//	/*Creating a new game*/
//	@Test
//	public void newGameTest() {
//		String newGame = "{"
//				+ "id:5,"
//				+ "body:{"
//				+ "token:" + userToken
//				+ "}";
//		
//		try {
//			serverWriter.write(newGame);
//			serverWriter.newLine();
//			serverWriter.flush();
//			
//			String answer = serverReader.readLine();
//			
//			JSONObject req = new JSONObject(answer);
//
//			boolean success = req.getBoolean("success");
//			String message = req.getString("message");
//			System.out.println(message);
//			assertEquals(success, true);
//			
//			JSONObject result = req.getJSONObject("result");
//			this.gameid = result.getString("gameid");
//			
//			
//		}catch(IOException e) {
//			e.printStackTrace();
//		}
//	}
//	
//	@Test
//	public void joinGameTest() {
//		
//	}
//	
//	@Test
//	public void logout() {
//		String logout = "{"
//				+ "id:7,"
//				+ "body:{"
//				+ "token:" + userToken + "}"
//				+ "}";
//		
//		try {
//			serverWriter.write(logout);
//			serverWriter.newLine();
//			serverWriter.flush();
//			
//			String answer = serverReader.readLine();
//			
//			JSONObject req = new JSONObject(answer);
//
//			boolean success = req.getBoolean("success");
//			String message = req.getString("message");
//			System.out.println(message);
//			assertEquals(success, true);
//			
//			JSONObject result = req.getJSONObject("result");
//			this.gameid = result.getString("gameid");
//			
//			
//		}catch(IOException e) {
//			e.printStackTrace();
//		}
//	}
//	
//	@Test
//	public void loginWithUsernameAndPassword() {
//		String login = "{"
//				+ "id:8,"
//				+ "body:{"
//				+ "username:" + this.username + ","
//				+ "password:" + this.password + ","
//				+ "}";
//		
//		try {
//			serverWriter.write(login);
//			serverWriter.newLine();
//			serverWriter.flush();
//			
//			String answer = serverReader.readLine();
//			
//			JSONObject req = new JSONObject(answer);
//
//			boolean success = req.getBoolean("success");
//			String id = req.getString("id");
//			String message = req.getString("message");
//			System.out.println(message);
//			
//			assertEquals(success, true);
//			assertEquals(id, "11");
//			
//			JSONObject result = req.getJSONObject("result");
//			String newToken = result.getString("token");
//			assertNotEquals(newToken, this.userToken);
//			this.userToken = newToken;
//			
//			this.logout();
//		}catch(IOException e) {
//			e.printStackTrace();
//		}
//	}
//	@Test 
//	public void loginWithToken() {
//		String login = "{"
//				+ "id:11,"
//				+ "body:{"
//				+ "token:" + this.userToken + "}"
//				+ "}";
//		try {
//			serverWriter.write(login);
//			serverWriter.newLine();
//			serverWriter.flush();
//			
//			String answer = serverReader.readLine();
//			
//			JSONObject req = new JSONObject(answer);
//
//			boolean success = req.getBoolean("success");
//			String id = req.getString("id");
//			String message = req.getString("message");
//			System.out.println(message);
//			
//			assertEquals(success, true);
//			assertEquals(id, "11");
//			
//			JSONObject result = req.getJSONObject("result");
//			String newToken = result.getString("token");
//			assertNotEquals(newToken, this.userToken);
//			this.userToken = newToken;
//			this.logout();
//		}catch(IOException e) {
//			e.printStackTrace();
//		}
//	}
//	
//	@Test
//	public void listAvailableGames() {
//		String listGames = "{"
//				+ "id:12,"
//				+ "body:{"
//				+ "token:" + this.userToken + "}"
//				+ "}";
//		
//		try {
//			serverWriter.write(listGames);
//			serverWriter.newLine();
//			serverWriter.flush();
//			
//			String answer = serverReader.readLine();
//			
//			JSONObject req = new JSONObject(answer);
//
//			boolean success = req.getBoolean("success");
//			String id = req.getString("id");
//			String message = req.getString("message");
//			System.out.println(message);
//			
//			assertEquals(success, true);
//			assertEquals(id, "12");
//			
//			JSONObject result = req.getJSONObject("result");
//			JSONArray games = result.getJSONArray("games");
//			JSONObject gameOne = games.getJSONObject(0);
//			String resultgameid = gameOne.getString("gameid");
//			JSONArray participants = gameOne.getJSONArray("users");
//			String firstUser = participants.getString(0);
//			
//			/*The only gameid that should exsts are the one we created earlier*/
//			assertEquals(resultgameid, this.gameid);
//			/*The only user should be us*/
//			assertEquals(firstUser, this.username);
//			
//			this.logout();
//		}catch(IOException e) {
//			e.printStackTrace();
//		}
//	}
//	
//
//	@Test
//	public void checkJSONTest() {
//		List<String> expectedKeys = Arrays.asList("id", "email", "username", "password");
//
//		JSONObject req = new JSONObject();
//		assertFalse(Server.checkJSON(req, expectedKeys));
//
//		req = generateJSONDefault();
//		assertTrue(Server.checkJSON(req, expectedKeys));
//	}
//
//	@Test
//	public void newUserTest() {
//
//	}
//}
