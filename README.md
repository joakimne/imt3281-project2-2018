# Velkommen til prosjekt 2 (Ludo prosjektet) i IMT3281 Applikasjonsutvikling

## Authors
* Joakim N. Ellestad - (141245), 16HBITSEC
* Bendik B. Flobak - (997818), 16HBITSEC
* Magnus L. Lilja - (473150), 16HBITSEC
* Nikolai Fauskerud - (473137), 16HBITSEC

[Beskrivelse av prosjektet](https://bitbucket.org/okolloen/imt3281-project2-2018/wiki/Home)

[Trelloboard fra utviklingen av løsningsforslaget](https://trello.com/b/rRweyGja/ludo-2016)

All testkode skal passere for å få bestått prosjektet.